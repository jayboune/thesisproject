<?php 
include'../config/db.php';
include'../config/functions.php';
include'../config/myfunction.php';
if(empty($_SESSION['login_applicant'])){ 
//This function is to check weather the account has been login or not
  header("Location: ../index.php");
  exit;
}
$user = getSingleRow("*","UserID","applicants",filter($_SESSION['UserID']));
$accounts = getSingleRow("*","UserID","accounts",filter($_SESSION['UserID']));
//Getting the Applicant personal information
if(isset($_POST['update_button'])){
  
  $FirstName = filter($_POST['FirstName']);
  $LastName = filter($_POST['LastName']);
  $email_address = filter($_POST['email_address']);
  $ContactNumber = filter($_POST['ContactNumber']);
  $birthdate = $_POST['birthdate'];
  $user_address = filter($_POST['user_address']);
  
  $allowedExts = array("png", "gif", "jpeg", "jpg");
  $temp = explode(".", $_FILES["photo"]["name"]);
  $photo =$_FILES['photo'] ["name"];
  $extension = end($temp);

  if(!is_numeric($ContactNumber)){
    $msg = 'Please enter valid number.';
  }elseif(is_numeric($FirstName)){
    $msg = 'Please enter letters only';
  }elseif(is_numeric($LastName)){
    $msg = 'Please enter letters only';
  }else{
    $arr_where = array("UserID"=>$_SESSION['UserID']);//update where
    $arr_set = array("ContactNumber"=>$ContactNumber,"BirthDate"=>$birthdate, "ApplicantEmail"=>$email_address,"Address"=>$user_address, "ApplicantPhoto"=>$photo);//set update
    $tbl_name = "applicants";
    $update = UpdateQuery($dbcon,$tbl_name,$arr_set,$arr_where);
    move_uploaded_file($_FILES["photo"]["tmp_name"],"../img/". $_FILES["photo"]["name"]);   
    header("location: index.php");
  }
  
}
?>
<?php include'../dist/assets/applicant_header.php';?>
<main role="main" style="background:#f4f4f5;">
      <div class="container marketing">
       <hr class="featurette-divider">
        <div class="row featurette" style="margin-top:100px;">

          <!-- /.col -->
          <div class="col-md-12">

            <div class="card" style="padding:15px;">
              <h4><i class="fa fa-pencil"></i> Update Information</h4><hr>
             
              <?php if(isset($msg)):?>
  <div class="alert alert-danger"><?php echo $msg;?></div>
<?php endif;?>
<form method="post" enctype="multipart/form-data">
<div class="row">
  <div class="col-md-2">
    First Name:
  </div>
  <div class="col-md-10">
    <input type="text" name="FirstName" value="<?php echo $accounts['FirstName']?>" placeholder="First Name" class="form-control" readonly>
  </div>
  <br><br>
  <div class="col-md-2">
    Last Name:
  </div>
  <div class="col-md-10">
    <input type="text" name="LastName" value="<?php echo $accounts['LastName']?>" placeholder="Last Name" class="form-control" readonly>
  </div>
  <br><br>
  <div class="col-md-2">
    Birthdate:
  </div>
  <div class="col-md-10">
    <input type="date" name="birthdate" value="<?php echo $user['Birthdate']?>" class="form-control">
  </div>
  <br><br>
  <div class="col-md-2">
    Contact Number:
  </div>
  <div class="col-md-10">
    <input type="text" name="ContactNumber" value="<?php echo $user['ContactNumber']?>" class="form-control" required placeholder="Contact Number">
  </div>
  <br><br>
  <div class="col-md-2">
    Address:
  </div>
  <div class="col-md-10">
    <input type="text" name="user_address" value="<?php echo $user['Address']?>" class="form-control" required placeholder="Address">
  </div>
  <br><br>
   <div class="col-md-2">
    Email Address:
  </div>
  <div class="col-md-10">
    <input type="text" name="email_address" value="<?php echo $user['ApplicantEmail']?>" class="form-control" required placeholder="Email Address">
  </div>
  <br><br>
  <div class="col-md-2">
    Photo:
  </div>
  <div class="col-md-10">
    <input type="file" name="photo"  class="form-control">
  </div>
  <br><br>

</div>
<center>
  <button class="btn btn-primary" name="update_button"><i class="fa fa-save"></i> Save</button>
  <a href="index.php" class="btn btn-danger"><i class="fa fa-arrow-left"></i> Return</a>
</center>
</form>
            </div>
            <!-- /.nav-tabs-custom -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
        </div>

</main>
      </div>
    
     <footer  style="background:#3f51b5; color:white; padding:20px;">
        <div class="container">
        <p>© JAD+GTC Manpower Supply & Services Inc. 2018 </p>
      </div>
      </footer>
<?php include'../dist/assets/pagemodals.php';?>
<?php include'../dist/assets/applicant_footer.php';?>
</body></html>