<?php 
include'../config/db.php';
include'../config/functions.php';
include'../config/myfunction.php';
if(empty($_SESSION['login_applicant'])){ 
//This function is to check weather the account has been login or not
  header("Location: ../index.php");
  exit;
}
$job = appliedJob();

?>
<?php include'../dist/assets/applicant_header.php';?>
<main role="main" style="background:#f4f4f5;">
      <div class="container marketing">
       <hr class="featurette-divider">
        <div class="row featurette" style="margin-top:100px;">
          <div class="col-md-3">
            <?php //include'../dist/assets/applicant_sidebar.php';?>
            <!-- /.card -->
          </div>
        </div>
          <!-- /.col -->

          <div class="col-md-12">
            <div class="card">
              <div class="card-body">
              <div class="row">
              <h5><i class="fa fa-th"></i> Applied Job</h5>
              </div>
              <hr>
<?php if(!empty($job)):?>
  <table id="example1" class="table table-bordered table-striped" style="font-size:12px;">
    <thead>
      <tr>
        <th>Job Title</th>
        <th>Company Name</th>
        <th>Status</th>
        <th>Interview Schedule</th>
        <th>Interviewer</th>
        <th>Remarks</th>
        <th>Option</th>
      </tr>
    </thead>
  <tbody>
    <?php foreach ($job as $key => $value):?>
      <tr>
        <th><?php echo $value->JobTitle?></th>
        <th>
          <?php echo $value->CompanyName;?>
        </th>
        <th><?php echo $value->ApplicationStatus?></th>
        <th><?php echo $value->InterviewSchedule?></th>
        <th><?php echo $value->Interviewer?></th>
        <th><?php echo $value->Remarks?></th>
        <th></th>
      </tr>
    <?php endforeach;?>
  </table>
<?php else:?>
  <div class="alert alert-danger">There are no records on the database.</div>
<?php endif;?>

              

                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            <!-- /.nav-tabs-custom -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
        </div>

</main>
      </div>
    
     <footer  style="background:#3f51b5; color:white; padding:20px;">
        <div class="container">
        <p>© JAD+GTC Manpower Supply & Services Inc. 2018 </p>
      </div>
      </footer>
<?php include'../dist/assets/pagemodals.php';?>
<?php include'../dist/assets/applicant_footer.php';?>
</body></html>