<?php 
include'../config/db.php';
include'../config/functions.php';
include'../config/myfunction.php';
if(empty($_SESSION['login_applicant'])){ 
//This function is to check weather the account has been login or not
  header("Location: ../index.php");
  exit;
}
$user = getSingleRow("*","UserID","applicants",filter($_SESSION['UserID']));
$accounts = getSingleRow("*","UserID","accounts",filter($_SESSION['UserID']));
//Getting the Applicant personal information
$education_list = fetchWhere("*","UserID","educations",$_SESSION['UserID']);
$skill_list = fetchWhere("*","UserID","skills",$_SESSION['UserID']);
$language_list = fetchWhere("*","UserID","languages",$_SESSION['UserID']);
$info_list = fetchWhere("*","UserID","additional_infos",$_SESSION['UserID']);
$experience_list = fetchWhere("*","UserID","experiences",$_SESSION['UserID']);

if(isset($_POST['upload_resume'])){
  $allowedExts = array("doc", "docx", "pdf", "xlsx");
  $temp = explode(".", $_FILES["photo"]["name"]);
  $photo =$_FILES['photo'] ["name"];
  $extension = end($temp);

  $arr_where = array("UserID"=>filter($_SESSION['UserID']));//update where
  $arr_set = array("ApplicantCV"=>$photo);//set update
  $tbl_name = "applicants";

  $update = UpdateQuery($dbcon,$tbl_name,$arr_set,$arr_where); 

  move_uploaded_file($_FILES["photo"]["tmp_name"],"../img/". $_FILES["photo"]["name"]);
  header("location: myprofile.php");
}
?>
<?php include'../dist/assets/applicant_header.php';?>
<main role="main" style="background:#f4f4f5;">
      <div class="container marketing">
       <hr class="featurette-divider">
        <div class="row featurette" style="margin-top:100px;">
          <div class="col-md-3">
            <?php include'../dist/assets/applicant_sidebar.php';?>
            <!-- /.card -->
          </div>
        </div>
          <!-- /.col -->

          <div class="col-md-9">
            <div class="card">
              <div class="card-body">
              <div class="row">
                <div class="col-md-2">
                  <img class="profile-user-img img-fluid"
                       src="../img/<?php echo $user['ApplicantPhoto']?>"
                       alt="User profile picture">
                </div>
                <div class="col-md-7">
                  <h3 class="profile-username">
                  <?php echo $accounts['FirstName']?> <?php echo $accounts['LastName']?>
                </h3>
                <p class="text-muted">
                <i class="fa fa-calendar"></i> <?php echo $user['BirthDate']?> /  <i class="fa fa-mobile"></i> <?php echo $user['ContactNumber']?> / <i class="fa fa-envelope"></i> <?php echo $user['ApplicantEmail']?> / <i class="fa fa-home"></i> <?php echo $user['Address']?></p>
                <center>

                </div>
              </div>
              <hr>                  
              <h5><i class="fa fa-graduation-cap"></i> Educational Background</h5><hr>
              <?php if(!empty($education_list)):?>
                <?php foreach ($education_list as $key => $value):?>
                  <div class="callout callout-success">
                  <h5 class="text-muted"><strong><?php echo $value->University?> - <?php echo $value->GradDate?></strong></h5>
                  <hr>

                  <p class="text-muted"><?php echo $value->Qualification?> - <?php echo $value->Course?></p>
                  <strong class="text-muted">Additional Information:</strong>
                  <p class="text-muted"><?php echo $value->AddInfo?></p>
                  
                </div>
                <?php endforeach;?>
                <?php else:?>
                  <div class="alert alert-danger">There are no records on the database.</div>
                <?php endif;?>
            <h5><i class="fa fa-briefcase"></i> Work Experience</h5><hr>
              <?php if(!empty($experience_list)):?>
                <?php foreach ($experience_list as $key => $value):?>
                  <div class="callout callout-success">
                  <h5 class="text-muted"><?php echo $value->EmployerName?> - <?php echo $value->PositionTitle?></h5>
                  <hr>

                  <p class="text-muted">From: <?php echo $value->Date_From?> - Until: <?php echo $value->Date_Until?></p>
                  <strong class="text-muted">Position Level: </strong> <?php echo $value->Position?><br>
                  <strong class="text-muted">Salary: </strong> <?php echo $value->Salary?><br>
                  <strong class="text-muted">Additional Information:</strong>
                  <p class="text-muted"><?php echo $value->OtherDesc?></p>
                  
                </div>
                <?php endforeach;?>
                <?php else:?>
                  <div class="alert alert-danger">There are no records on the database.</div>
                <?php endif;?>
              <h5><i class="fa fa-book"></i> Skills</h5><hr>
              <?php if(!empty($skill_list)):?>
                <?php foreach ($skill_list as $key => $value):?>

                  <div class="callout callout-success">
                  <div class="row">
                    <div class="col-md-9 text-muted"><strong>Skill:</strong> <?php echo $value->Skill?></div>
                    <div class="col-md-3 text-muted"><strong>Profeciency:</strong> <?php echo $value->Proficiency?></div>
                  </div>
                </div>
                <?php endforeach;?>
                <?php else:?>
                  <div class="alert alert-danger">There are no records on the database.</div>
                <?php endif;?>
              <h5><i class="fa fa-comment"></i> Language</h5><hr>
              <?php if(!empty($language_list)):?>
                <?php foreach ($language_list as $key => $value):?>

                <div class="callout callout-success">
                  <div class="row">
                    <div class="col-md-6 text-muted"><strong>Language:</strong> <?php echo $value->Language?></div>
                    <div class="col-md-3 text-muted"><strong>Spoken:</strong> <?php echo $value->Spoken?></div>
                    <div class="col-md-3 text-muted"><strong>Written:</strong> <?php echo $value->Written?></div>
                  </div>
                </div>

                <?php endforeach;?>
                <?php else:?>
                  <div class="alert alert-danger">There are no records on the database.</div>
                <?php endif;?>
              <h5><i class="fa fa-plus"></i> Additional Information</h5><hr>
              <?php if(!empty($info_list)):?>
                <?php foreach ($info_list as $key => $value):?>
                  <div class="callout callout-success">
                  
                  <strong class="text-muted">Expected Salary:</strong> <?php echo $value->ExpectedSalary?><br>
                  <strong class="text-muted">Preferred Work Location:</strong> <?php echo $value->WorkLocation?><br>
                  <strong class="text-muted">Other Information</strong><br>
                  <p><?php echo $value->OtherInfo?></p>
                 
                </div>
                <?php endforeach;?>
                <?php else:?>
                  <div class="alert alert-danger">There are no records on the database.</div>
                <?php endif;?>
              <center>
                
                <a href="update-profile.php" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i> Update Profile</a>

                  <a href="change.php" class="btn btn-danger btn-sm"><i class="fa fa-wrench"></i> Change Password</a>
              </center>

                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            <!-- /.nav-tabs-custom -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
        </div>

</main>
      </div>
    
     <footer  style="background:#3f51b5; color:white; padding:20px;">
        <div class="container">
        <p>© JAD+GTC Manpower Supply & Services Inc. 2018 </p>
      </div>
      </footer>
<?php include'../dist/assets/pagemodals.php';?>
<?php include'../dist/assets/applicant_footer.php';?>
</body></html>