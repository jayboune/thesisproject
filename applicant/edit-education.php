<?php 
include'../config/db.php';
include'../config/functions.php';
include'../config/myfunction.php';
if(empty($_SESSION['login_applicant'])){ 
//This function is to check weather the account has been login or not
  header("Location: ../index.php");
  exit;
}
if(isset($_GET['educID'])){
  $educ = fetchWhere("*","EduID","educations",$_GET['educID']);
  if(!empty($educ)){
    foreach ($educ as $key => $row) {
      $university = $row->University;
      $grad_date = $row->GradDate;
      $qualification = $row->Qualification;
      $course = $row->Course;
      $add_info = $row->AddInfo;
    }
  }else{
    header("location: error.php");
  }
}
if(isset($_POST['save_button'])){
  $university = filter($_POST['university']);
  $grad_date = filter($_POST['grad_date']);
  $qualification = filter($_POST['qualification']);
  $course = filter($_POST['course']);
  $add_info = filter($_POST['add_info']);

  $arr_where = array("EduID"=>filter($_GET['educID']));//update where
  $arr_set = array("University" => $university, "GradDate" => $grad_date, "Qualification" => $qualification, "Course" => $course, "AddInfo" => $add_info);
  $tbl_name = "educations";
  $update = UpdateQuery($dbcon,$tbl_name,$arr_set,$arr_where);// UPDATE SQL
  header("location: education.php");
}
?>
<?php include'../dist/assets/applicant_header.php';?>
<main role="main" style="background:#f4f4f5;">
      <div class="container marketing">
       <hr class="featurette-divider">
        <div class="row featurette" style="margin-top:100px;">
          <div class="col-md-3">
            <?php include'../dist/assets/applicant_sidebar.php';?>
            <!-- /.card -->
          </div>
        </div>
          <!-- /.col -->
          <div class="col-md-9">
            <div class="card">
              <div class="card-body">
                <div class="row" >
                  <div class="col-md-11">
                    <h5><i class="fa fa-pencil"></i> Update Information</h5>
                  </div>
                  <div class="col-md-1">
                    
                  </div>
                </div>
                <hr>
                <form method="post" enctype="multipart/form-data">
              <div class="row">
                <div class="col-md-12">
                  University:<br>
                  <input type="text" name="university" class="form-control" placeholder="University" required value="<?php echo $university;?>">
                </div>
                <br> 
                <div class="col-md-12">
                  Graduation Date:<br>
                  <input type="date" name="grad_date" class="form-control" placeholder="University" required value="<?php echo $grad_date;?>">
                </div>
                <div class="col-md-12">
                  Qualification:<br>
                  <select name="qualification" class="form-control">
                    <option value="HighSchool Diploma" <?php if(isset($_GET['educID'])){
                      if($qualification == "HighSchool Diploma"){echo 'selected';}}
                        elseif(isset($_POST['save_button'])){echo $_POST['qualification'];}?>>HighSchool Diploma
                    </option>
                    <option value="Vocational / Short Course Certificate" <?php if(isset($_GET['educID'])){
                      if($qualification == "Vocational / Short Course Certificate"){echo 'selected';}}
                        elseif(isset($_POST['save_button'])){echo $_POST['qualification'];}?>>Vocational / Short Course Certificate
                    </option>
                    <option value="Bachelors Degree /College Degree" <?php if(isset($_GET['educID'])){
                      if($qualification == "Bachelors Degree /College Degree"){echo 'selected';}}
                        elseif(isset($_POST['save_button'])){echo $_POST['qualification'];}?>>Bachelors Degree /College Degree
                    </option>
                    <option value="Post Graduate Diploma / Masters Degree" <?php if(isset($_GET['educID'])){
                      if($qualification == "Post Graduate Diploma / Masters Degree"){echo 'selected';}}
                        elseif(isset($_POST['save_button'])){echo $_POST['qualification'];}?>>Post Graduate Diploma / Masters Degree
                    </option>
                    <option value="Professional License (Pass Board/ Bar/ Professional License Exam)" <?php if(isset($_GET['educID'])){
                      if($qualification == "Professional License (Pass Board/ Bar/ Professional License Exam)"){echo 'selected';}}
                        elseif(isset($_POST['save_button'])){echo $_POST['qualification'];}?>>Professional License (Pass Board/ Bar/ Professional License Exam)
                    </option>
                    

                  </select>
                </div>
                <div class="col-md-12">
                  Course:<br>
                  <input type="text" name="course" class="form-control" placeholder="Course" required value="<?php echo $course;?>">
                </div>
                <div class="col-md-12">
                  Additional Information:<br>
                  <textarea class="form-control" name="add_info" placeholder="Enter Additinoal info(Optional)"><?php echo $add_info;?></textarea>
                </div>
                <br><br>
                <div class="col-md-12">
                  <br>
                  <button name="save_button" class="btn btn-primary">
                    <i class="fa fa-save"></i> Save Data
                  </button> 
                </div>

              </div>
            </form>            
              </div><!-- /.card-body -->
            </div>
            <!-- /.nav-tabs-custom -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
        </div>

</main>
      </div>
    
     <footer  style="background:#3f51b5; color:white; padding:20px;">
        <div class="container">
        <p>© JAD+GTC Manpower Supply & Services Inc. 2018 </p>
      </div>
      </footer>
<?php include'../dist/assets/pagemodals.php';?>
<?php include'../dist/assets/applicant_footer.php';?>
</body></html>