<?php 
include'../config/db.php';
include'../config/functions.php';
include'../config/myfunction.php';
if(empty($_SESSION['login_applicant'])){ 
//This function is to check weather the account has been login or not
  header("Location: ../index.php");
  exit;
}
if(isset($_GET['skillID'])){
  $skills = fetchWhere("*","SkillID","skills",$_GET['skillID']);
  if(!empty($skills)){
    foreach ($skills as $key => $row) {
      $skill_name = $row->Skill;
      $profeciency = $row->Proficiency;
    }
  }else{
    header("location: error.php");
  }
}
if(isset($_POST['save_button'])){
  $skill_name = filter($_POST['skill_name']);
  $profeciency = filter($_POST['profeciency']);

  $arr_where = array("SkillID"=>filter($_GET['skillID']));//update where
  $arr_set = array("Skill" => $skill_name, "Proficiency" => $profeciency);
  $tbl_name = "skills";
  $update = UpdateQuery($dbcon,$tbl_name,$arr_set,$arr_where);// UPDATE SQL
  header("location: skills.php");
}
?>
<?php include'../dist/assets/applicant_header.php';?>
<main role="main" style="background:#f4f4f5;">
      <div class="container marketing">
       <hr class="featurette-divider">
        <div class="row featurette" style="margin-top:100px;">
          <div class="col-md-3">
            <?php include'../dist/assets/applicant_sidebar.php';?>
            <!-- /.card -->
          </div>
        </div>
          <!-- /.col -->
          <div class="col-md-9">
            <div class="card">
              <div class="card-body">
                <div class="row" >
                  <div class="col-md-11">
                    <h5><i class="fa fa-pencil"></i> Update Information</h5>
                  </div>
                  <div class="col-md-1">
                    
                  </div>
                </div>
                <hr>
                <form method="post" enctype="multipart/form-data">
  <div class="row">
  <div class="col-md-12">
    Skills:<br>
    <input type="text" name="skill_name" class="form-control" placeholder="Skill" required value="<?php echo $skill_name?>">
  </div>
    <br><br>
    <div class="col-md-12">
      Qualification:<br>
      <select name="profeciency" class="form-control">
        <option value="Beginner" <?php if(isset($_GET['skillID'])){
          if($profeciency == "Beginner"){echo 'selected';}}
          elseif(isset($_POST['save_button'])){echo $_POST['profeciency'];}?>>Beginner</option>
        <option value="Advanced" <?php if(isset($_GET['skillID'])){
          if($profeciency == "Advanced"){echo 'selected';}}
          elseif(isset($_POST['save_button'])){echo $_POST['profeciency'];}?>>Advanced</option>
        <option value="Intermediate" <?php if(isset($_GET['skillID'])){
          if($profeciency == "Intermediate"){echo 'selected';}}
          elseif(isset($_POST['save_button'])){echo $_POST['profeciency'];}?>>Intermediate</option>
      </select>
    </div>
  <div class="col-md-12">
    <br>
    <button name="save_button" class="btn btn-primary"><i class="fa fa-save"></i> Save Data</button> 
  </div>
              </div>
            </form>         
              </div><!-- /.card-body -->
            </div>
            <!-- /.nav-tabs-custom -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
        </div>

</main>
      </div>
    
     <footer  style="background:#3f51b5; color:white; padding:20px;">
        <div class="container">
        <p>© JAD+GTC Manpower Supply & Services Inc. 2018 </p>
      </div>
      </footer>
<?php include'../dist/assets/pagemodals.php';?>
<?php include'../dist/assets/applicant_footer.php';?>
</body></html>