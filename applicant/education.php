<?php 
include'../config/db.php';
include'../config/functions.php';
include'../config/myfunction.php';
if(empty($_SESSION['login_applicant'])){ 
//This function is to check weather the account has been login or not
  header("Location: ../index.php");
  exit;
}
$education_list = fetchWhere("*","UserID","educations",$_SESSION['UserID']);
if(isset($_POST['save_button'])){
  $university = filter($_POST['university']);
  $grad_date = filter($_POST['grad_date']);
  $qualification = filter($_POST['qualification']);
  $course = filter($_POST['course']);
  $add_info = filter($_POST['add_info']);

  $insertArray = array("University" => $university, "GradDate" => $grad_date, "Qualification" => $qualification, "Course" => $course, "AddInfo" => $add_info, "UserID" => $_SESSION['UserID']);
  SaveData("educations",$insertArray);
  header("location: education.php");
}
if(isset($_GET['delete'])){ // Deleting records on the database.
  $delete = filter($_GET['delete']);
    $ar = array("EduID"=>$delete); //WHERE statement
  $tbl_name = "educations"; 
  $del = Delete($dbcon,$tbl_name,$ar);
  if($del){
    header("location: education.php");
  }
}
?>
<?php include'../dist/assets/applicant_header.php';?>
<main role="main" style="background:#f4f4f5;">
      <div class="container marketing">
       <hr class="featurette-divider">
        <div class="row featurette" style="margin-top:100px;">
          <div class="col-md-3">
            <?php include'../dist/assets/applicant_sidebar.php';?>
            <!-- /.card -->
          </div>
        </div>
          <!-- /.col -->
          <div class="col-md-9">
            <div class="card">
              <div class="card-body">
                <div class="row" >
                  <div class="col-md-11">
                    <h5><i class="fa fa-graduation-cap"></i> Educational Background</h5>
                  </div>
                  <div class="col-md-1">
                    <a href="" data-toggle="modal" data-target="#add-educ"><i class="fa fa-plus"></i> Add</a>
                  </div>
                </div>
                <hr>
                <p>Please add your two highest education qualifications</p>
                <?php if(!empty($education_list)):?>
                <?php foreach ($education_list as $key => $value):?>
                  <div class="callout callout-success">
                  <h5><?php echo $value->University?> - <?php echo $value->GradDate?></h5>
                  <hr>

                  <p><?php echo $value->Qualification?> - <?php echo $value->Course?></p>
                  <strong>Additional Information:</strong>
                  <p><?php echo $value->AddInfo?></p>
                  <a href="edit-education.php?educID=<?php echo $value->EduID?>" style="color:black;"><i class="fa fa-pencil"></i> Update</a> 
                  <a href="#" style="color:black;" <?php echo 'onclick=" confirm(\'Are you sure you want to delete?\')?window.location = \'education.php?delete='.$value->EduID.'\' : \'\';"'; ?>><i class="fa fa-remove"></i> Remove</a>
                </div>
                <?php endforeach;?>
                <?php else:?>
                  <div class="alert alert-danger">There are no records on the database.</div>
                <?php endif;?>
              </div><!-- /.card-body -->
            </div>
            <!-- /.nav-tabs-custom -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
        </div>

</main>
      </div>
    
     <footer  style="background:#3f51b5; color:white; padding:20px;">
        <div class="container">
        <p>© JAD+GTC Manpower Supply & Services Inc. 2018 </p>
      </div>
      </footer>
<?php include'../dist/assets/pagemodals.php';?>
<?php include'../dist/assets/applicant_footer.php';?>
</body></html>