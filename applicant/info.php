<?php 
include'../config/db.php';
include'../config/functions.php';
include'../config/myfunction.php';
if(empty($_SESSION['login_applicant'])){ 
//This function is to check weather the account has been login or not
  header("Location: ../index.php");
  exit;
}
$info_list = fetchWhere("*","UserID","additional_infos",$_SESSION['UserID']);
if(isset($_POST['save_button'])){
  $expected_salary = filter($_POST['expected_salary']);
  $work_location = filter($_POST['work_location']);
  $other_info = filter($_POST['other_info']);
 
  $insertArray = array("ExpectedSalary" => $expected_salary, "WorkLocation" => $work_location, "UserID" => $_SESSION['UserID'], "OtherInfo" => $other_info);
  SaveData("additional_infos",$insertArray);
  header("location: info.php");
}
if(isset($_GET['delete'])){ // Deleting records on the database.
  $delete = filter($_GET['delete']);
    $ar = array("InfoID"=>$delete); //WHERE statement
  $tbl_name = "additional_infos"; 
  $del = Delete($dbcon,$tbl_name,$ar);
  if($del){
    header("location: info.php");
  }
}
?>
<?php include'../dist/assets/applicant_header.php';?>
<main role="main" style="background:#f4f4f5;">
      <div class="container marketing">
       <hr class="featurette-divider">
        <div class="row featurette" style="margin-top:100px;">
          <div class="col-md-3">
            <?php include'../dist/assets/applicant_sidebar.php';?>
            <!-- /.card -->
          </div>
        </div>
          <!-- /.col -->
          <div class="col-md-9">
            <div class="card">
              <div class="card-body">
                <div class="row" >
                  <div class="col-md-11">
                    <h5><i class="fa fa-plus"></i> Other Information</h5>
                  </div>
                  <div class="col-md-1">
                    <a href="" data-toggle="modal" data-target="#add-info"><i class="fa fa-plus"></i> Add</a>
                  </div>
                </div>
                <hr>
                <?php if(!empty($info_list)):?>
                <?php foreach ($info_list as $key => $value):?>
                  <div class="callout callout-success">
                  
                  <strong>Expected Salary:</strong> <?php echo $value->ExpectedSalary?><br>
                  <strong>Preferred Work Location:</strong> <?php echo $value->WorkLocation?><br>
                  <strong>Other Information</strong><br>
                  <p><?php echo $value->OtherInfo?></p>
                  <a href="edit-info.php?info_id=<?php echo $value->InfoID?>" style="color:black;"><i class="fa fa-pencil"></i> Update</a> 
                  <a href="#" style="color:black;" <?php echo 'onclick=" confirm(\'Are you sure you want to delete?\')?window.location = \'info.php?delete='.$value->InfoID.'\' : \'\';"'; ?>><i class="fa fa-remove"></i> Remove</a>
                </div>
                <?php endforeach;?>
                <?php else:?>
                  <div class="alert alert-danger">There are no records on the database.</div>
                <?php endif;?>
              </div><!-- /.card-body -->
            </div>
            <!-- /.nav-tabs-custom -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
        </div>

</main>
      </div>
    
     <footer  style="background:#3f51b5; color:white; padding:20px;">
        <div class="container">
        <p>© JAD+GTC Manpower Supply & Services Inc. 2018 </p>
      </div>
      </footer>
<?php include'../dist/assets/pagemodals.php';?>
<?php include'../dist/assets/applicant_footer.php';?>
</body></html>