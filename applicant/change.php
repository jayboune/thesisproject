<?php 
include'../config/db.php';
include'../config/functions.php';
include'../config/myfunction.php';
if(empty($_SESSION['login_applicant'])){ 
//This function is to check weather the account has been login or not
  header("Location: ../index.php");
  exit;
}

if(isset($_POST['change_btn'])){
    $old_pass = md5($_POST['old_pass']);
    $new_pass = filter($_POST['new_pass']);
    $confirm_pass = filter($_POST['confirm_pass']);

    $g = getSingleRow("*","EmailAddress","accounts",filter($_SESSION['EmailAddress']));
    // select the data where email address is equal to the session so that you can check if the old password is correct or not
    if($new_pass != $confirm_pass){
      $msg = 'Password do not matched.';
    }
    elseif($g['Password'] != $old_pass){
      $msg = 'Old password do no matched';
    }
    else{
      $arr_where = array("EmailAddress"=>$_SESSION['EmailAddress']);//update where
      $arr_set = array("Password"=>md5($new_pass));//set update
      $tbl_name = "accounts";
      $update = UpdateQuery($dbcon,$tbl_name,$arr_set,$arr_where);   
      $success = 'Password has been successfully updated.';
    }
  }
?>
<?php include'../dist/assets/applicant_header.php';?>
<main role="main" style="background:#f4f4f5;">
      <div class="container marketing">
       <hr class="featurette-divider">
        <div class="row featurette" style="margin-top:100px;">

          <!-- /.col -->
          <div class="col-md-12">

            <div class="card" style="padding:15px;">
              <h4><i class="fa fa-wrench"></i> Change Password</h4>
              <?php if(isset($msg)):?>
              <div class="alert alert-danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $msg;?>
              <br />
            </div>
            <?php endif;?>
            <?php if(isset($success)):?>
              <div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $success;?>
              <br />
            </div>
            <?php endif;?>
             <form method="post">
             <table class="table table-bordered">
                <tr>
                  <td>Old Password:</td>
                  <td><input type="password" name="old_pass" class="form-control" placeholder="Old Password" required></td>
                </tr>
                 <tr>
                  <td>New Password:</td>
                  <td><input type="password" name="new_pass" class="form-control" placeholder="New Password" required></td>
                </tr>
                 <tr>
                  <td>Confirm Password:</td>
                  <td><input type="password" name="confirm_pass" class="form-control" placeholder="Confirm Password" required></td>
                </tr>
                <tr>
                  <td></td>
                  <td>
                     <button class="btn btn-primary btn-large" name="change_btn"><i class="fa fa-save"></i> Change Password</button>
                <a href="index.php" class="btn btn-danger btn-large"><i class="fa fa-arrow-left"></i> Return</a>
                  </td>
                </tr>
              </table>
          </form>
              
            </div>
            <!-- /.nav-tabs-custom -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
        </div>

</main>
      </div>
    
     <footer  style="background:#3f51b5; color:white; padding:20px;">
        <div class="container">
        <p>© JAD+GTC Manpower Supply & Services Inc. 2018 </p>
      </div>
      </footer>
<?php include'../dist/assets/pagemodals.php';?>
<?php include'../dist/assets/applicant_footer.php';?>
</body></html>