<?php 
include'../config/db.php';
include'../config/functions.php';
include'../config/myfunction.php';
if(empty($_SESSION['login_applicant'])){ 
//This function is to check weather the account has been login or not
  header("Location: ../index.php");
  exit;
}
$experience_list = fetchWhere("*","UserID","experiences",$_SESSION['UserID']);
if(isset($_POST['save_button'])){
  $position_title = filter($_POST['position_title']);
  $company_name = filter($_POST['company_name']);
  $date_from = filter($_POST['date_from']);
  $date_until = filter($_POST['date_until']);
  $position_level = filter($_POST['position_level']);
  $salary = filter($_POST['salary']);
  $other_desc = filter($_POST['other_desc']);

  $insertArray = array("PositionTitle" => $position_title, "EmployerName" => $company_name, "Date_From" => $date_from, "Date_Until" => $date_until, "Position" => $position_level, "UserID" => $_SESSION['UserID'], "Salary" => $salary, "OtherDesc" => $other_desc);
  SaveData("experiences",$insertArray);
  header("location: experience.php");
}
if(isset($_GET['delete'])){ // Deleting records on the database.
  $delete = filter($_GET['delete']);
    $ar = array("ExpID"=>$delete); //WHERE statement
  $tbl_name = "experiences"; 
  $del = Delete($dbcon,$tbl_name,$ar);
  if($del){
    header("location: experience.php");
  }
}
?>
<?php include'../dist/assets/applicant_header.php';?>
<main role="main" style="background:#f4f4f5;">
      <div class="container marketing">
       <hr class="featurette-divider">
        <div class="row featurette" style="margin-top:100px;">
          <div class="col-md-3">
            <?php include'../dist/assets/applicant_sidebar.php';?>
            <!-- /.card -->
          </div>
        </div>
          <!-- /.col -->
          <div class="col-md-9">
            <div class="card">
              <div class="card-body">
                <div class="row" >
                  <div class="col-md-11">
                    <h5><i class="fa fa-file"></i> Work Experience</h5>
                  </div>
                  <div class="col-md-1">
                    <a href="" data-toggle="modal" data-target="#add-experience"><i class="fa fa-plus"></i> Add</a>
                  </div>
                </div>
                <hr>
              
                <?php if(!empty($experience_list)):?>
                <?php foreach ($experience_list as $key => $value):?>
                  <div class="callout callout-success">
                  <h5><?php echo $value->EmployerName?> - <?php echo $value->PositionTitle?></h5>
                  <hr>

                  <p>From: <?php echo $value->Date_From?> - Until: <?php echo $value->Date_Until?></p>
                  <strong>Position Level: </strong> <?php echo $value->Position?><br>
                  <strong>Salary: </strong> <?php echo $value->Salary?><br>
                  <strong>Additional Information:</strong>
                  <p><?php echo $value->OtherDesc?></p>
                  <a href="edit-experience.php?experience_id=<?php echo $value->ExpID?>" style="color:black;"><i class="fa fa-pencil"></i> Update</a> 
                  <a href="#" style="color:black;" <?php echo 'onclick=" confirm(\'Are you sure you want to delete?\')?window.location = \'experience.php?delete='.$value->ExpID.'\' : \'\';"'; ?>><i class="fa fa-remove"></i> Remove</a>
                </div>
                <?php endforeach;?>
                <?php else:?>
                  <div class="alert alert-danger">There are no records on the database.</div>
                <?php endif;?>
              </div><!-- /.card-body -->
            </div>
            <!-- /.nav-tabs-custom -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
        </div>

</main>
      </div>
    
     <footer  style="background:#3f51b5; color:white; padding:20px;">
        <div class="container">
        <p>© JAD+GTC Manpower Supply & Services Inc. 2018 </p>
      </div>
      </footer>
<?php include'../dist/assets/pagemodals.php';?>
<?php include'../dist/assets/applicant_footer.php';?>
</body></html>