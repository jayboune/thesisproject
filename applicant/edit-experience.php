<?php 
include'../config/db.php';
include'../config/functions.php';
include'../config/myfunction.php';
if(empty($_SESSION['login_applicant'])){ 
//This function is to check weather the account has been login or not
  header("Location: ../index.php");
  exit;
}
if(isset($_GET['experience_id'])){
  $experience = fetchWhere("*","ExpID","experiences",$_GET['experience_id']);
  if(!empty($experience)){
    foreach ($experience as $key => $row) {
      $position_title = $row->PositionTitle;
      $company_name = $row->EmployerName;
      $date_from = $row->Date_From;
      $date_until = $row->Date_Until;
      $position_level = $row->Position;
      $salary = $row->Salary;
      $other_desc = $row->OtherDesc;
    }
  }else{
    header("location: error.php");
  }
}
if(isset($_POST['save_button'])){
  $position_title = filter($_POST['position_title']);
  $company_name = filter($_POST['company_name']);
  $date_from = filter($_POST['date_from']);
  $date_until = filter($_POST['date_until']);
  $position_level = filter($_POST['position_level']);
  $salary = filter($_POST['salary']);
  $other_desc = filter($_POST['other_desc']);

  $arr_where = array("ExpID"=>filter($_GET['experience_id']));//update where
  $arr_set = array("PositionTitle" => $position_title, "EmployerName" => $company_name, "Date_From" => $date_from, "Date_Until" => $date_until, "Position" => $position_level, "UserID" => $_SESSION['UserID'], "Salary" => $salary, "OtherDesc" => $other_desc);
  $tbl_name = "experiences";
  $update = UpdateQuery($dbcon,$tbl_name,$arr_set,$arr_where);// UPDATE SQL
  header("location: experience.php");
}
?>
<?php include'../dist/assets/applicant_header.php';?>
<main role="main" style="background:#f4f4f5;">
      <div class="container marketing">
       <hr class="featurette-divider">
        <div class="row featurette" style="margin-top:100px;">
          <div class="col-md-3">
            <?php include'../dist/assets/applicant_sidebar.php';?>
            <!-- /.card -->
          </div>
        </div>
          <!-- /.col -->
          <div class="col-md-9">
            <div class="card">
              <div class="card-body">
                <div class="row" >
                  <div class="col-md-11">
                    <h5><i class="fa fa-pencil"></i> Update Information</h5>
                  </div>
                  <div class="col-md-1">
                    
                  </div>
                </div>
                <hr>
<form method="post" enctype="multipart/form-data">
              <div class="row">
                <div class="col-md-12">
                  Position:<br>
                  <input type="text" name="position_title" class="form-control" placeholder="Position" required value="<?php echo $position_title?>">
                </div>
                <br>
                <div class="col-md-12">
                  Company:<br>
                  <input type="text" name="company_name" class="form-control" placeholder="Company Name" required value="<?php echo $company_name?>">
                </div>
                <br>  
                <div class="col-md-12">
                  From:<br>
                  <input type="date" name="date_from" class="form-control" required value="<?php echo $date_from?>">
                </div><br>
                <div class="col-md-12">
                  Until:<br>
                  <input type="date" name="date_until" class="form-control" required value="<?php echo $date_until?>">
                </div>
                <div class="col-md-12">
                  Position Level:<br>
                  <select name="position_level" class="form-control">
                    <option value="Fresh Graduate" <?php if(isset($_GET['experience_id'])){
                      if($position_level == "Fresh Graduate"){echo 'selected';}}
                        elseif(isset($_POST['save_button'])){echo $_POST['position_level'];}?>>Fresh Graduate</option>
                    <option value="1 to 2 years Working experience" <?php if(isset($_GET['experience_id'])){
                      if($position_level == "1 to 2 years Working experience"){echo 'selected';}}
                        elseif(isset($_POST['save_button'])){echo $_POST['position_level'];}?>>1 to 2 years Working experience</option>
                    <option value="Managerial Position" <?php if(isset($_GET['experience_id'])){
                      if($position_level == "Managerial Position"){echo 'selected';}}
                        elseif(isset($_POST['save_button'])){echo $_POST['position_level'];}?>>Managerial Position</option>
                    <option value="CEO" <?php if(isset($_GET['experience_id'])){
                      if($position_level == "CEO"){echo 'selected';}}
                        elseif(isset($_POST['save_button'])){echo $_POST['position_level'];}?>>CEO</option>
                  </select>
                </div>

                <div class="col-md-12">
                  Salary:<br>
                  <input type="number" name="salary" class="form-control" placeholder="Basic Salary" required value="<?php echo $salary?>">
                </div>
                <div class="col-md-12">
                  Additional Information:<br>
                  <textarea class="form-control" name="other_desc" placeholder="Enter Additinoal info(Optional)"><?php echo $other_desc?></textarea>
                </div>
                <br><br>
                <div class="col-md-12">
                  <br>
                  <button name="save_button" class="btn btn-primary">
                    <i class="fa fa-save"></i> Save Data
                  </button> 
                </div>

              </div>
            </form>      
              </div><!-- /.card-body -->
            </div>
            <!-- /.nav-tabs-custom -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
        </div>

</main>
      </div>
    
     <footer  style="background:#3f51b5; color:white; padding:20px;">
        <div class="container">
        <p>© JAD+GTC Manpower Supply & Services Inc. 2018 </p>
      </div>
      </footer>
<?php include'../dist/assets/pagemodals.php';?>
<?php include'../dist/assets/applicant_footer.php';?>
</body></html>