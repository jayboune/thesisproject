<?php 
include'../config/db.php';
include'../config/functions.php';
include'../config/myfunction.php';
if(empty($_SESSION['login_applicant'])){ 
//This function is to check weather the account has been login or not
  header("Location: ../index.php");
  exit;
}
$list = fetchAll("*","jobpost");
if(isset($_GET['job_id'])){
  $job_id = filter($_GET['job_id']);
  $selected_job = fetchWhere("*","job_id","jobpost",$job_id);
  if(!empty($selected_job)){
    
  }else{
    header("location: error.php");
  }
}
?>
<?php include'../dist/assets/applicant_header.php';?>
<main role="main" style="background:#f4f4f5;">
      <div class="container marketing">
       <hr class="featurette-divider">
        <div class="row featurette" style="margin-top:100px;">

          <!-- /.col -->
          <div class="col-md-12">
            <div class="card">
              <div class="card-body">

             <div class="alert alert-danger alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <h5><i class="icon fa fa-ban"></i> Alert!</h5>
                 You are trying to access invalid data to our site.
                 <a href="index.php">Return</a>
                </div> 
        

            <!-- /.nav-tabs-custom -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
        </div>
</div>
</main>
      </div>
    
     <footer  style="background:#3f51b5; color:white; padding:20px;">
        <div class="container">
        <p>© JAD+GTC Manpower Supply & Services Inc. 2018 </p>
      </div>
      </footer>
<?php include'../dist/assets/pagemodals.php';?>
<?php include'../dist/assets/applicant_footer.php';?>

</body></html>