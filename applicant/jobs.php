<?php 
include'../config/db.php';
include'../config/functions.php';
include'../config/myfunction.php';
if(empty($_SESSION['login_applicant'])){ 
//This function is to check weather the account has been login or not
  header("Location: ../index.php");
  exit;
}
$list = fetchWhere("*","ArchiveID","jobposts","0"); 
if(isset($_POST['save_job'])){
  $job_id = filter($_POST['job_id']);
  $interview_schedule = filter($_POST['interview_schedule']);

  $allowedExts = array("docx", "pdf", "doc");
  $temp = explode(".", $_FILES["ApplicantCV"]["name"]);
  $ApplicantCV =$_FILES['ApplicantCV'] ["name"];
  $extension = end($temp);

  $dateNOW = strtotime(date("Y-m-d"));
  $applieddate = strtotime(date("Y-m-d",$interview_schedule)); 

  $diff = $dateNOW - $applieddate; 

  if($diff < 0 ){
    //$msg = 'previous ito';
    echo '<script>alert("You are not allowed to enter previous dates");</script>';
    //echo '<h1>EROOR</h1>';
  }
  else{
     $selected_job = fetchWhere("*","JobID","jobposts",$job_id);
  if(!empty($selected_job)){
    foreach ($selected_job as $key => $row) {
      $job_id = $row->JobID;
      $company_id = $row->ClientID;
    }

    
    $insertArray = array("JobID" => $job_id , "ClientID" => $company_id , "ApplicantID" => $_SESSION['UserID'], "ApplicationStatus" => "Waiting for Response", "InterviewSchedule" => $interview_schedule);
    SaveData("jobapplications",$insertArray);
    
    $job = getSingleRow("*","JobID","jobposts",$job_id);
    $difference = $job['Slots'] - 1;
    $arr_where = array("JobID"=>$job_id);//update where
    $arr_set = array("Slots" => $difference);
    $tbl_name = "jobposts";
    $update = UpdateQuery($dbcon,$tbl_name,$arr_set,$arr_where);// UPDATE

    //Applicant CV updating of resume
    $where = array("UserID"=>$_SESSION['UserID']);//update where
    $set = array("ApplicantCV" => $ApplicantCV);
    $tbl_name2 = "applicants";
    $update1 = UpdateQuery($dbcon,$tbl_name2,$set,$where);// UPDATE
    move_uploaded_file($_FILES["ApplicantCV"]["tmp_name"],"../img/". $_FILES["ApplicantCV"]["name"]);

     //$msg =  'success';
    echo '<script>alert("You have successfully apply for a job. Please wait for the confirmation of interviewer.");window.location= "jobs.php";</script>';
  

 
  }else{
    header("location: error.php");
  }
}
}
?>
<?php include'../dist/assets/applicant_header.php';?>
<main role="main" style="background:#f4f4f5;">
      <div class="container marketing">
       <hr class="featurette-divider">
        <div class="row featurette" style="margin-top:100px;">

          <!-- /.col -->
          <div class="col-md-12">
            <div class="card">
              <div class="card-body">

              <h3 class="card-title"><i class="fa fa-search"></i> Job List</h3>
<hr>
        <form method="get">
  <div class="form-row">
    <div class="col-md-5">
      <input type="text" name="search_field" placeholder="Search Job" class="form-control">
    </div>
    <div class="col-md-4">
<select class="form-control" required = 'required' name = "cat_id" id="functionList" onchange="changeCat(this.value)">
  <option value="all">All</option>
  <?php
    $catCounter = 0;
    $field = $dbcon->query("SELECT * FROM jobcategories") or die(mysqli_error());
    while($fields = $field->fetch_assoc()){
      if($catCounter == 0){
        $catCounter += 1;
        $functionOne = $fields['JCategoryID'];
      }
  ?>
    <option value="<?php echo $fields['JCategoryID']?>"><?php echo $fields['Category']?></option>
  <?php   
  }
  ?>
</select>
    </div>
    <div class="col-md-3">
      <select class="form-control" required = 'required' name="sub_id" id="compList" onchange="me(this.value)" >
        <option value="all">All</option>
        <?php
          $funcCounter = 0;
          $function = $dbcon->query("SELECT * FROM specializations WHERE JCategoryID = ".$functionOne) or die(mysqli_error());
          if(mysqli_num_rows($function) == 0){
            echo '<option>Select Data</option>';
          }else{
          while($functions = $function->fetch_assoc()){
            if($funcCounter == 0){
              $funcCounter += 1;
              $compOne = $functions['SpecializationID'];
          }
        ?>
        <option value="<?php echo $functions['SpecializationID']?>"><?php echo $functions['Specialization']?></option>
                       
        <?php }
          }
        ?>
      <select>
    </div>
    <br><br>
    <div class="col-md-4">
    <button class="btn btn-primary" name="search_button"><i class="fa fa-search"></i> Search Job</button>
    </div>
  </div>
</form>
<br>
<?php if(isset($msg)): echo $msg; endif;?>
<?php if(isset($_GET['search_button']) AND isset($_POST['sub_id']) == 'all' AND isset($_POST['cat_id']) == 'all'):?>
<?php $jobs = searchJobAll();?>
  <?php if(!empty($jobs)):?>
  <?php foreach ($jobs as $key => $value):?>
    <div class="callout callout-default">
      <div class="row">
        <div class="col-md-11">
          <h4>
        <a href="" style="color:black;" data-toggle="modal" data-target="#view-job<?php echo $value->JobID?>">
        <?php echo $value->JobTitle?> 
        <?php if($value->isUrgent == 'No'):?>
        <?php else:?>
          -  <small style="color:red;">Urgent!</small>
        <?php endif;?>
      </a>
      </h4>
        </div>
        <div class="col-md-1">
           <a href="" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#set-interview<?php echo $value->JobID?>"><i class="fa fa-file"></i> Apply </a>
        </div>
      </div>
      
      <hr>
      <div class="row">
        <div class="col-md-4"><i class="fa fa-home"></i> Company:<?php echo $value->CompanyName?></div>
        <div class="col-md-4"><i class="fa fa-map"></i> Location: <?php echo $value->Location?></div>
        <div class="col-md-4"><i class="fa fa-file"></i> Available Slots: <?php echo $value->Slots?></div>
      </div>
      <hr>
      <?php echo substr($value->JobDescription, 0,300)?>
      <div class="row">
        <div class="col-12">
          <h4>
              <small class="float-right" style="font-size:15px;color:#999;">Date Created: <?php echo date("Y-m-d",strtotime($value->DateCreated))?>
              
              </small>
          </h4>
        </div>
        
      </div>
    </div>
<!-- View Full Job-->
<div class="modal fade" id="view-job<?php echo $value->JobID?>">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title">Job Details</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                
              </div>
              <div class="modal-body">
              <?php 
              $viewjob = getSingleRow("*","JobID","jobposts",$value->JobID);
              $company = getSingleRow("*","UserID","companyclients",$value->ClientID);
              ?>
              <div class="row">
                <div class="col-md-4">Company Name:</div>
                <div class="col-md-8"><?php echo $value->CompanyName?></div>
                <div class="col-md-4">Job Title:</div>
                <div class="col-md-8"><?php echo $value->JobTitle?></div>
                <div class="col-md-4">Job Status:</div>
                <div class="col-md-8"><?php echo $value->JobStatus?></div>
                <div class="col-md-4">Available Slots:</div>
                <div class="col-md-8"><?php echo $value->Slots?></div>
                <div class="col-md-4">Expected Salary:</div>
                <div class="col-md-8"><?php echo $value->ExpectedSalary?></div>
              </div>
              <hr>
              <strong>Job Description:</strong>
              <?php echo $value->JobDescription?>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
</div>
<!-- end -->
    <!-- Applying for a job-->
<div class="modal fade" id="set-interview<?php echo $value->JobID?>">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title">Set Interview Schedule</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                
              </div>
              <div class="modal-body">
                <form method="post" enctype="multipart/form-data">
<?php 
$viewuser = getSingleRow("*","UserID","applicants",$_SESSION['UserID']);
$account = getSingleRow("*","UserID","accounts",$viewuser['UserID']);
?>
  <div class="row">
    <div class="col-md-4">Full Name:</div>
    <div class="col-md-8"><?php echo $account['FirstName']?> <?php echo $account['LastName']?></div>
    <div class="col-md-4">Address:</div>
    <div class="col-md-8"><?php echo $viewuser['Address']?></div>
    <div class="col-md-4">Email Address:</div>
    <div class="col-md-8"><?php echo $viewuser['ApplicantEmail']?></div>
    <div class="col-md-4">Contact Number:</div>
    <div class="col-md-8"><?php echo $viewuser['ContactNumber']?></div>
    <div class="col-md-4">Gender:</div>
    <div class="col-md-8"><?php echo $viewuser['Gender']?></div>
        <div class="col-md-4">Birthdate:</div>
    <div class="col-md-8"><?php echo $viewuser['BirthDate']?></div>
  </div>
  <hr>
  <?php if(empty($viewuser['ApplicantCV'])):?>
    <input type="file" name="ApplicantCV">
  <?php else:?>
  <?php endif;?>
  <br>
  <strong>Schedule:</strong><br>
    <input type="hidden" name="job_id" required="required" value="<?php echo $value->job_id?>"><br>
  <input type="date" name="interview_schedule" required="required" class="form-control"><br>
              </div>
              <div class="modal-footer">
                
                <button type="submit" name="save_job" class="btn btn-primary">Save</button>
              </div>
            </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
</div>
<!-- end -->
  <?php endforeach;?>
  <?php else:?>
    <div class="alert alert-danger">There are no records on the database. <a href="jobs.php">Return</a></div>
  <?php endif;?>
<?php elseif(isset($_GET['search_button']) AND isset($_POST['sub_id']) != 'all' AND isset($_POST['cat_id']) != 'all'):?>
  <?php $job = searchJob();?>
  <?php if(!empty($job)):?>
  <?php foreach ($job as $key => $value):?>
    <div class="callout callout-default">
      <div class="row">
        <div class="col-md-11">
          <h4>
        <a href="" style="color:black;" data-toggle="modal" data-target="#view-job<?php echo $value->JobID?>">
        <?php echo $value->JobTitle?> 
        <?php if($value->isUrgent == 'No'):?>
        <?php else:?>
          -  <small style="color:red;">Urgent!</small>
        <?php endif;?>
      </a>
      </h4>
        </div>
        <div class="col-md-1">
           <a href="" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#set-interview<?php echo $value->JobID?>"><i class="fa fa-file"></i> Apply </a>
        </div>
      </div>
      
      <hr>
      <div class="row">
        <div class="col-md-4"><i class="fa fa-home"></i> Company:<?php echo $value->CompanyName?></div>
        <div class="col-md-4"><i class="fa fa-map"></i> Location: <?php echo $value->Location?></div>
        <div class="col-md-4"><i class="fa fa-file"></i> Available Slots: <?php echo $value->Slots?></div>
      </div>
      <hr>
      <?php echo substr($value->JobDescription, 0,300)?>
      <div class="row">
        <div class="col-12">
          <h4>
              <small class="float-right" style="font-size:15px;color:#999;">Date Created: <?php echo date("Y-m-d",strtotime($value->DateCreated))?>
              
              </small>
          </h4>
        </div>
        
      </div>
    </div>
<!-- View Full Job-->
<div class="modal fade" id="view-job<?php echo $value->JobID?>">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title">Job Details</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                
              </div>
              <div class="modal-body">
              <?php 
              $viewjob = getSingleRow("*","JobID","jobposts",$value->JobID);
              $company = getSingleRow("*","UserID","companyclients",$value->ClientID);
              ?>
              <div class="row">
                <div class="col-md-4">Company Name:</div>
                <div class="col-md-8"><?php echo $value->CompanyName?></div>
                <div class="col-md-4">Job Title:</div>
                <div class="col-md-8"><?php echo $value->JobTitle?></div>
                <div class="col-md-4">Job Status:</div>
                <div class="col-md-8"><?php echo $value->JobStatus?></div>
                <div class="col-md-4">Available Slots:</div>
                <div class="col-md-8"><?php echo $value->Slots?></div>
                <div class="col-md-4">Expected Salary:</div>
                <div class="col-md-8"><?php echo $value->ExpectedSalary?></div>
              </div>
              <hr>
              <strong>Job Description:</strong>
              <?php echo $value->JobDescription?>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
</div>
<!-- end -->
    <!-- Applying for a job-->
<div class="modal fade" id="set-interview<?php echo $value->JobID?>">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title">Set Interview Schedule</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                
              </div>
              <div class="modal-body">
                <form method="post" enctype="multipart/form-data">
<?php 
$viewuser = getSingleRow("*","UserID","applicants",$_SESSION['UserID']);
$account = getSingleRow("*","UserID","accounts",$viewuser['UserID']);
?>
  <div class="row">
    <div class="col-md-4">Full Name:</div>
    <div class="col-md-8"><?php echo $account['FirstName']?> <?php echo $account['LastName']?></div>
    <div class="col-md-4">Address:</div>
    <div class="col-md-8"><?php echo $viewuser['Address']?></div>
    <div class="col-md-4">Email Address:</div>
    <div class="col-md-8"><?php echo $viewuser['ApplicantEmail']?></div>
    <div class="col-md-4">Contact Number:</div>
    <div class="col-md-8"><?php echo $viewuser['ContactNumber']?></div>
    <div class="col-md-4">Gender:</div>
    <div class="col-md-8"><?php echo $viewuser['Gender']?></div>
        <div class="col-md-4">Birthdate:</div>
    <div class="col-md-8"><?php echo $viewuser['BirthDate']?></div>
  </div>
  <hr>
  <?php if(empty($viewuser['ApplicantCV'])):?>
    <input type="file" name="ApplicantCV">
  <?php else:?>
  <?php endif;?>
  <br>
  <strong>Schedule:</strong><br>
    <input type="hidden" name="job_id" required="required" value="<?php echo $value->job_id?>"><br>
  <input type="date" name="interview_schedule" required="required" class="form-control"><br>
              </div>
              <div class="modal-footer">
                
                <button type="submit" name="save_job" class="btn btn-primary">Save</button>
              </div>
            </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
</div>
<!-- end -->
  <?php endforeach;?>
  <?php else:?>
    <div class="alert alert-danger">There are no records on the database. <a href="jobs.php">Return</a></div>
  <?php endif;?>

<?php else:?>
<?php  if(!empty($list)):?>
  <table id="example2" class="table">
    <thead>
      <tr>
        <th>Job Description</th>
      </tr>
    </thead>
    <tbody>
  <?php foreach ($list as $key => $value):?>
    <tr>
      <td>
         <div class="callout callout-default">
      <div class="row">
        <div class="col-md-11">
          <h4>
        <a href="" style="color:black;" data-toggle="modal" data-target="#view-job<?php echo $value->JobID?>">
        <?php echo $value->JobTitle?> 
        <?php if($value->isUrgent == 'No'):?>
        <?php else:?>
          -  <small style="color:red;">Urgent!</small>
        <?php endif;?>
      </a>
      </h4>
        </div>
        <div class="col-md-1">
           <a href="" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#set-interview<?php echo $value->JobID?>"><i class="fa fa-file"></i> Apply </a>
        </div>
      </div>
      
      <hr>
      <?php 
      $company = getSingleRow("*","UserID","companyclients",$value->ClientID);
      $location = getSingleRow("*","LocationID","locations",$value->LocationID);
      ?>
      <div class="row">
        <div class="col-md-4"><i class="fa fa-home"></i> Company:<?php echo $company['CompanyName']?></div>
        <div class="col-md-4"><i class="fa fa-map"></i> Location: <?php echo $location['Location']?></div>
        <div class="col-md-4"><i class="fa fa-file"></i> Available Slots: <?php echo $value->Slots?></div>
      </div>
      <hr>
      <?php echo substr($value->JobDescription, 0,300)?>
      <div class="row">
        <div class="col-12">
          <h4>
              <small class="float-right" style="font-size:15px;color:#999;">Date Created: <?php echo date("Y-m-d",strtotime($value->DateCreated))?>
              
              </small>
          </h4>
        </div>
        
      </div>
    </div>
       
  </td>
</tr>
<!-- Applying for a job-->
<div class="modal fade" id="set-interview<?php echo $value->JobID?>">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title">Profile</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                
              </div>
              <div class="modal-body">
                <form method="post" enctype="multipart/form-data">
<?php 
$viewuser = getSingleRow("*","UserID","applicants",$_SESSION['UserID']);
$account = getSingleRow("*","UserID","accounts",$viewuser['UserID']);
?>
  <div class="row">
    <div class="col-md-4">Full Name:</div>
    <div class="col-md-8"><?php echo $account['FirstName']?> <?php echo $account['LastName']?></div>
    <div class="col-md-4">Address:</div>
    <div class="col-md-8"><?php echo $viewuser['Address']?></div>
    <div class="col-md-4">Email Address:</div>
    <div class="col-md-8"><?php echo $viewuser['ApplicantEmail']?></div>
    <div class="col-md-4">Contact Number:</div>
    <div class="col-md-8"><?php echo $viewuser['ContactNumber']?></div>
    <div class="col-md-4">Gender:</div>
    <div class="col-md-8"><?php echo $viewuser['Gender']?></div>
        <div class="col-md-4">Birthdate:</div>
    <div class="col-md-8"><?php echo $viewuser['BirthDate']?></div>
  </div>
  <hr>
  <?php if(empty($viewuser['ApplicantCV'])):?>
    <input type="file" name="ApplicantCV">
  <?php else:?>
  <?php endif;?>
  <br>
  <strong>Schedule:</strong><br>
    <input type="hidden" name="job_id" required="required" value="<?php echo $value->JobID?>"><br>
  <input type="date" name="interview_schedule" required="required" class="form-control"><br>
              </div>
              <div class="modal-footer">
                
                <button type="submit" name="save_job" class="btn btn-primary">Save</button>
              </div>
            </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
</div>
<!-- end -->
<!-- View Full Job-->
<div class="modal fade" id="view-job<?php echo $value->JobID?>">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title">Job Details</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                
              </div>
              <div class="modal-body">
              <?php 
              $viewjob = getSingleRow("*","JobID","jobposts",$value->JobID);
              $company = getSingleRow("*","UserID","companyclients",$value->ClientID);
              ?>
              <div class="row">
                <div class="col-md-4">Company Name:</div>
                <div class="col-md-8"><?php echo $company['CompanyName']?></div>
                <div class="col-md-4">Job Title:</div>
                <div class="col-md-8"><?php echo $value->JobTitle?></div>
                <div class="col-md-4">Job Status:</div>
                <div class="col-md-8"><?php echo $value->JobStatus?></div>
                <div class="col-md-4">Available Slots:</div>
                <div class="col-md-8"><?php echo $value->Slots?></div>
                <div class="col-md-4">Expected Salary:</div>
                <div class="col-md-8"><?php echo $value->ExpectedSalary?></div>
              </div>
              <hr>
              <strong>Job Description:</strong>
              <?php echo $value->JobDescription?>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
</div>
<!-- end -->
<?php endforeach;?>
</table>
<?php else:?>
<?php endif;?> 

<?php endif;?>
            </div>
            <!-- /.nav-tabs-custom -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
        </div>

</main>
      </div>
    
     <footer  style="background:#3f51b5; color:white; padding:20px;">
        <div class="container">
        <p>© JAD+GTC Manpower Supply & Services Inc. 2018 </p>
      </div>
      </footer>
<?php include'../dist/assets/pagemodals.php';?>
<?php include'../dist/assets/applicant_footer.php';?>
<script type="text/javascript">
function changeCat(key){
  if(window.XMLHttpRequest){
  // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp = new XMLHttpRequest();
  }else{
  // code for IE6, IE5
    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange = function(){
    if(xmlhttp.readyState == 4 && xmlhttp.status == 200){
      document.getElementById("compList").innerHTML = xmlhttp.responseText;
      var comp = document.getElementById("compList").value;
      me(comp);
    }
  }
  xmlhttp.open("GET","../admin/changeCat.php?key="+key, true);
  xmlhttp.send(); 
}
</script>

</body></html>