<?php 
include'../config/db.php';
include'../config/functions.php';
include'../config/myfunction.php';
if(empty($_SESSION['login_applicant'])){ 
//This function is to check weather the account has been login or not
  header("Location: ../index.php");
  exit;
}
$language_list = fetchWhere("*","UserID","languages",$_SESSION['UserID']);
if(isset($_POST['save_button'])){
  $language_name = filter($_POST['language_name']);
  $spoken = filter($_POST['spoken']);
  $written = filter($_POST['written']);
 
  $insertArray = array("Language" => $language_name, "Spoken" => $spoken, "UserID" => $_SESSION['UserID'], "Written" => $written);
  SaveData("languages",$insertArray);
  header("location: language.php");
}
if(isset($_GET['delete'])){ // Deleting records on the database.
  $delete = filter($_GET['delete']);
    $ar = array("LanguageID"=>$delete); //WHERE statement
  $tbl_name = "languages"; 
  $del = Delete($dbcon,$tbl_name,$ar);
  if($del){
    header("location: language.php");
  }
}
?>
<?php include'../dist/assets/applicant_header.php';?>
<main role="main" style="background:#f4f4f5;">
      <div class="container marketing">
       <hr class="featurette-divider">
        <div class="row featurette" style="margin-top:100px;">
          <div class="col-md-3">
            <?php include'../dist/assets/applicant_sidebar.php';?>
            <!-- /.card -->
          </div>
        </div>
          <!-- /.col -->
          <div class="col-md-9">
            <div class="card">
              <div class="card-body">
                <div class="row" >
                  <div class="col-md-11">
                    <h5><i class="fa fa-comment"></i> Language</h5>
                  </div>
                  <div class="col-md-1">
                    <a href="" data-toggle="modal" data-target="#add-language"><i class="fa fa-plus"></i> Add</a>
                  </div>
                </div>
                <hr>
                <?php if(!empty($language_list)):?>
                <?php foreach ($language_list as $key => $value):?>
                  <div class="callout callout-success">
                  <h5>Language: <?php echo $value->Language?></h5>
                  <hr>
                  <strong>Spoken:</strong> <?php echo $value->Spoken?><br>
                  <strong>Written:</strong> <?php echo $value->Written?><br>

                  <a href="edit-language.php?language_id=<?php echo $value->LanguageID?>" style="color:black;"><i class="fa fa-pencil"></i> Update</a> 
                  <a href="#" style="color:black;" <?php echo 'onclick=" confirm(\'Are you sure you want to delete?\')?window.location = \'language.php?delete='.$value->LanguageID.'\' : \'\';"'; ?>><i class="fa fa-remove"></i> Remove</a>
                </div>
                <?php endforeach;?>
                <?php else:?>
                  <div class="alert alert-danger">There are no records on the database.</div>
                <?php endif;?>
              </div><!-- /.card-body -->
            </div>
            <!-- /.nav-tabs-custom -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
        </div>

</main>
      </div>
    
     <footer  style="background:#3f51b5; color:white; padding:20px;">
        <div class="container">
        <p>© JAD+GTC Manpower Supply & Services Inc. 2018 </p>
      </div>
      </footer>
<?php include'../dist/assets/pagemodals.php';?>
<?php include'../dist/assets/applicant_footer.php';?>
</body></html>