<?php 
include'../config/db.php';
include'../config/functions.php';
include'../config/myfunction.php';
if(empty($_SESSION['login_applicant'])){ 
//This function is to check weather the account has been login or not
  header("Location: ../index.php");
  exit;
}
if(isset($_GET['language_id'])){
  $language = fetchWhere("*","LanguageID","languages",$_GET['language_id']);
  if(!empty($language)){
    foreach ($language as $key => $row) {
      $language_name = $row->Language;
      $spoken = $row->Spoken;
      $written = $row->Written;
    }
  }else{
    header("location: error.php");
  }
}
if(isset($_POST['save_button'])){
  $language_name = filter($_POST['language_name']);
  $spoken = filter($_POST['spoken']);
  $written = filter($_POST['written']);

  $arr_where = array("LanguageID"=>filter($_GET['language_id']));//update where
  $arr_set = array("Language" => $language_name, "Spoken" => $spoken, "Written" => $written);
  $tbl_name = "languages";
  $update = UpdateQuery($dbcon,$tbl_name,$arr_set,$arr_where);// UPDATE SQL
  header("location: language.php");
}
?>
<?php include'../dist/assets/applicant_header.php';?>
<main role="main" style="background:#f4f4f5;">
      <div class="container marketing">
       <hr class="featurette-divider">
        <div class="row featurette" style="margin-top:100px;">
          <div class="col-md-3">
            <?php include'../dist/assets/applicant_sidebar.php';?>
            <!-- /.card -->
          </div>
        </div>
          <!-- /.col -->
          <div class="col-md-9">
            <div class="card">
              <div class="card-body">
                <div class="row" >
                  <div class="col-md-11">
                    <h5><i class="fa fa-pencil"></i> Update Information</h5>
                  </div>
                  <div class="col-md-1">
                    
                  </div>
                </div>
                <hr>
                <form method="post" enctype="multipart/form-data">
  <div class="row">
  <div class="col-md-12">
    Language:<br>
    <input type="text" name="language_name" class="form-control" placeholder="Language" required value="<?php echo $language_name?>">
  </div>
    <br><br>
    <div class="col-md-12">
      Spoken:<br>
      <select name="spoken" class="form-control">
        <option value="1" <?php if(isset($_GET['language_id'])){
          if($spoken == "1"){echo 'selected';}}
          elseif(isset($_POST['save_button'])){echo $_POST['spoken'];}?>>1</option>
        <option value="2" <?php if(isset($_GET['language_id'])){
          if($spoken == "2"){echo 'selected';}}
          elseif(isset($_POST['save_button'])){echo $_POST['spoken'];}?>>2</option>
        <option value="3" <?php if(isset($_GET['language_id'])){
          if($spoken == "3"){echo 'selected';}}
          elseif(isset($_POST['save_button'])){echo $_POST['spoken'];}?>>3</option>
        <option value="4" <?php if(isset($_GET['language_id'])){
          if($spoken == "4"){echo 'selected';}}
          elseif(isset($_POST['save_button'])){echo $_POST['spoken'];}?>>4</option>
        <option value="5" <?php if(isset($_GET['language_id'])){
          if($spoken == "5"){echo 'selected';}}
          elseif(isset($_POST['save_button'])){echo $_POST['spoken'];}?>>5</option>
      </select>
    </div>
    <br><br>
    <div class="col-md-12">
      Written:<br>
      <select name="written" class="form-control">
        <option value="1" <?php if(isset($_GET['language_id'])){
          if($written == "1"){echo 'selected';}}
          elseif(isset($_POST['save_button'])){echo $_POST['written'];}?>>1</option>
        <option value="2"  <?php if(isset($_GET['language_id'])){
          if($written == "2"){echo 'selected';}}
          elseif(isset($_POST['save_button'])){echo $_POST['written'];}?>>2</option>
        <option value="3"  <?php if(isset($_GET['language_id'])){
          if($written == "3"){echo 'selected';}}
          elseif(isset($_POST['save_button'])){echo $_POST['written'];}?>>3</option>
        <option value="4"  <?php if(isset($_GET['language_id'])){
          if($written == "4"){echo 'selected';}}
          elseif(isset($_POST['save_button'])){echo $_POST['written'];}?>>4</option>
        <option value="5"  <?php if(isset($_GET['language_id'])){
          if($written == "5"){echo 'selected';}}
          elseif(isset($_POST['save_button'])){echo $_POST['written'];}?>>5</option>
      </select>
    </div>
  <div class="col-md-12">
    <br>
    <button name="save_button" class="btn btn-primary"><i class="fa fa-save"></i> Save Data</button> 
  </div>
              </div>
            </form>      
              </div><!-- /.card-body -->
            </div>
            <!-- /.nav-tabs-custom -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
        </div>

</main>
      </div>
    
     <footer  style="background:#3f51b5; color:white; padding:20px;">
        <div class="container">
        <p>© JAD+GTC Manpower Supply & Services Inc. 2018 </p>
      </div>
      </footer>
<?php include'../dist/assets/pagemodals.php';?>
<?php include'../dist/assets/applicant_footer.php';?>
</body></html>