<?php 
include'../config/db.php';
include'../config/functions.php';
include'../config/myfunction.php';
if(empty($_SESSION['login_applicant'])){ 
//This function is to check weather the account has been login or not
  header("Location: ../index.php");
  exit;
}
if(isset($_GET['info_id'])){
  $info = fetchWhere("*","InfoID","additional_infos",$_GET['info_id']);
  if(!empty($info)){
    foreach ($info as $key => $row) {
      $expected_salary = $row->ExpectedSalary;
      $work_location = $row->WorkLocation;
      $other_info = $row->OtherInfo;
    }
  }else{
    header("location: error.php");
  }
}
if(isset($_POST['save_button'])){
  $expected_salary = filter($_POST['expected_salary']);
  $work_location = filter($_POST['work_location']);
  $other_info = filter($_POST['other_info']);
 

  $arr_where = array("InfoID"=>filter($_GET['info_id']));//update where
  $arr_set = array("ExpectedSalary" => $expected_salary, "WorkLocation" => $work_location, "UserID" => $_SESSION['UserID'], "OtherInfo" => $other_info);
  $tbl_name = "additional_infos";
  $update = UpdateQuery($dbcon,$tbl_name,$arr_set,$arr_where);// UPDATE SQL
  header("location: info.php");
}
?>
<?php include'../dist/assets/applicant_header.php';?>
<main role="main" style="background:#f4f4f5;">
      <div class="container marketing">
       <hr class="featurette-divider">
        <div class="row featurette" style="margin-top:100px;">
          <div class="col-md-3">
            <?php include'../dist/assets/applicant_sidebar.php';?>
            <!-- /.card -->
          </div>
        </div>
          <!-- /.col -->
          <div class="col-md-9">
            <div class="card">
              <div class="card-body">
                <div class="row" >
                  <div class="col-md-11">
                    <h5><i class="fa fa-pencil"></i> Update Information</h5>
                  </div>
                  <div class="col-md-1">
                    
                  </div>
                </div>
                <hr>
 <form method="post" enctype="multipart/form-data">
              <div class="row">
                <div class="col-md-12">
                  Expected Salary:<br>
                  <input type="text" name="expected_salary" class="form-control" placeholder="Expected Salary" required value="<?php echo $expected_salary?>">
                </div>
                <br> 
                <div class="col-md-12">
                  Work Location:<br>
                  <input type="text" name="work_location" class="form-control" placeholder="Work Location" required value="<?php echo $work_location?>">
                </div>
                <div class="col-md-12">
                  Additional Information:<br>
                  <textarea class="form-control" name="other_info" placeholder="Enter Additinoal info(Optional)"><?php echo $other_info?></textarea>
                </div>
                <br><br>
                <div class="col-md-12">
                  <br>
                  <button name="save_button" class="btn btn-primary">
                    <i class="fa fa-save"></i> Save Data
                  </button> 
                </div>

              </div>
            </form>       
              </div><!-- /.card-body -->
            </div>
            <!-- /.nav-tabs-custom -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
        </div>

</main>
      </div>
    
     <footer  style="background:#3f51b5; color:white; padding:20px;">
        <div class="container">
        <p>© JAD+GTC Manpower Supply & Services Inc. 2018 </p>
      </div>
      </footer>
<?php include'../dist/assets/pagemodals.php';?>
<?php include'../dist/assets/applicant_footer.php';?>
</body></html>