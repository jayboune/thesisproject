<?php 
include'../config/db.php';
include'../config/functions.php';
include'../config/myfunction.php';

if(empty($_SESSION['login_admin'])){ //This function is to check weather the account has been login or not
  header("Location: ../index.php");
  exit;
}

if(isset($_GET['job_id'])){ 
  $view = fetchWhere("*","JobID","jobposts",filter($_GET['job_id']));
if(!empty($view)){
  foreach ($view as $key => $row) {
    $JobTitle = $row->JobTitle;
    $JobStatus = $row->JobStatus;
    $ExpirationDate = $row->ExpirationDate;
    $Slots = $row->Slots;
    $FilledSlots = $row->FilledSlots;
    $isUrgent =$row->isUrgent;
    $JobDescription = $row->JobDescription;
    $ExpectedSalary = $row->ExpectedSalary;
    $ClientID = $row->ClientID;
    $loc_id = $row->LocationID;
    $SpecializationID = $row->SpecializationID;

  }
}else{
  header("location: error.php");
}
}
if(isset($_POST['save_button']) && isset($_GET['job_id'])){
  $JobTitle = filter($_POST['JobTitle']);
  $JobStatus = filter($_POST['JobStatus']);
  $expiration_date = filter($_POST['expiration_date']);
  $available_slots = filter($_POST['available_slots']);
  $isUrgent = filter($_POST['isUrgent']);
  $job_description = $_POST['job_description'];
  $expected_salary = filter($_POST['expected_salary']);
  $ClientID = filter($_POST['ClientID']);
  $sub_id = filter($_POST['sub_id']);
  $loc_id = filter($_POST['loc_id']);
  
  $arr_where = array("JobID"=>filter($_GET['job_id']));//update where
  $arr_set = array("JobTitle"=>$JobTitle,"JobStatus"=>$JobStatus,"ExpirationDate"=>$expiration_date,"Slots"=>$available_slots,"isUrgent"=>$isUrgent,"JobDescription"=>$job_description,"ExpectedSalary"=>$expected_salary,"ClientID"=>$ClientID,"LocationID" => $loc_id);//set update

  $tbl_name = "jobposts";
  $update = UpdateQuery($dbcon,$tbl_name,$arr_set,$arr_where);// UPDATE SQL
  header("location: job-list.php");
}

?>
<?php include'../dist/assets/dashboard_header.php';?>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
<?php include'../dist/assets/dashboard_nav.php';?>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Info boxes -->
        <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title"><i class="fa fa-plus"></i> Job Details</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">

<?php if(isset($msg)):?><div class="alert alert-danger"><?php echo $msg;?></div><?php endif;?>
            <form method="post" class="table-responsive">
            <table class="table table-bordered">
                <tr>
                  <td>Title:</td>
                  <td>
                    <input type="text" name="JobTitle" class="form-control" placeholder="Job Title" required="required"
                    value="<?php if(isset($_GET['job_id'])): echo $JobTitle; elseif(isset($_POST['save_button'])): echo $_POST['JobTitle']; endif;?>">
                  </td>
                </tr>
                <tr>
                  <td>Company:</td>
                  <td>
            <select name="ClientID" class="form-control">
              <?php $list = selectCompany();?>
              <?php foreach ($list as $key => $value):?>
              <option value="<?php echo $value->UserID?>"
              <?php if(isset($_GET['job_id'])){
                if($ClientID == $value->UserID){
                  echo 'selected';
                }
              }
              elseif(isset($_POST['save_button'])){
                  echo $_POST['ClientID'];
                }
                ?>><?php echo $value->CompanyName?></option>
            <?php endforeach;?>
            </select>
                  </td>
                </tr>
                 <tr>
                  <td>Location:</td>
                  <td>
            <select name="loc_id" class="form-control">
              <?php $list = fetchAll("*","locations");?>
              <?php foreach ($list as $key => $value):?>
              <option value="<?php echo $value->LocationID?>"
              <?php if(isset($_GET['job_id'])){
                if($loc_id == $value->LocationID){
                  echo 'selected';
                }
              }
              elseif(isset($_POST['save_button'])){
                  echo $_POST['loc_id'];
                }
                ?>><?php echo $value->Location?></option>
            <?php endforeach;?>
            </select>
                  </td>
                </tr>
                <tr>
                  <td>Status:</td>
                  <td>
                    <select name="JobStatus" class="form-control">
                    <option value="Available" <?php if(isset($_GET['job_id'])){
                      if($JobStatus == "Available"){
                        echo 'selected';
                      }
                    }
                    elseif(isset($_POST['save_button'])){
                      echo $_POST['JobStatus'];
                    }?>>Available</option>
                    <option value="Occupied" <?php if(isset($_GET['job_id'])){
                      if($JobStatus == "Occupied"){
                        echo 'selected';
                      }
                    }
                    elseif(isset($_POST['save_button'])){
                      echo $_POST['JobStatus'];
                    }?>>Occupied</option> 
                    </select>
                  </td>
                </tr>
                <tr>
                  <td>Expiration Date:</td>
                  <td>
                    <input type="date" name="expiration_date" class="form-control" required="required"
                    value="<?php if(isset($_GET['job_id'])): echo $ExpirationDate; elseif(isset($_POST['save_button'])): echo $_POST['expiration_date']; endif;?>">
                  </td>
                </tr>
                 <tr>
                  <td>Expected Salary:</td>
                  <td>
                    <input type="number" name="expected_salary" class="form-control" required="required"
                    value="<?php if(isset($_GET['job_id'])): echo $ExpectedSalary; elseif(isset($_POST['save_button'])): echo $_POST['expected_salary']; endif;?>">
                  </td>
                </tr>
                <tr>
                  <td>Available Slots:</td>
                  <td>
                    <input type="number" name="available_slots" class="form-control" placeholder="Available Slots" required="required"
                    value="<?php if(isset($_GET['job_id'])): echo $Slots; elseif(isset($_POST['save_button'])): echo $_POST['available_slots']; endif;?>">
                  </td>
                </tr>
                <!--
                <tr>
                  <td>Job Category:</td>
                  <td>
                     
                    <select class="form-control" required = 'required' name = "cat_id" id="functionList" onchange="changeCat(this.value)">
                      <?php
                      $catCounter = 0;
                      $field = $dbcon->query("SELECT * FROM jobcategories") or die(mysqli_error());
                      while($fields = $field->fetch_assoc()){
                        if($catCounter == 0){
                          $catCounter += 1;
                          $functionOne = $fields['JCategoryID'];
                        }
                      ?>
                      <option value="<?php echo $fields['JCategoryID']?>" <?php if(isset($_GET['job_id'])){
                      $getCat = getSingleRow("*","JCategoryID","jobcategories",$fields['JCategoryID']);
                      $s = getSingleRow("*","JCategoryID","specializations",$fields['JCategoryID']);
                      if($getCat['JCategoryID'] == $s['JCategoryID']){
                        echo 'selected';
                      }
                    }
                    elseif(isset($_POST['save_button'])){
                      echo $_POST['cat_id'];
                    }?>><?php echo $fields['Category']?></option>
                      <?php   
                      }
                      ?>
                    </select>
                  </td>
                </tr>
                <tr>
                  <td>Specialization:</td>
                  <td>
                    
                     <select class="form-control" required = 'required' name="sub_id" id="compList" onchange="me(this.value)" >
                        <?php
                        $funcCounter = 0;
                        $function = $dbcon->query("SELECT * FROM specializations WHERE JCategoryID = ".$functionOne) or die(mysqli_error());
                        if(mysqli_num_rows($function) == 0){
                          echo '<option>Select Data</option>';
                        }else{
                        while($functions = $function->fetch_assoc()){
                          if($funcCounter == 0){
                            $funcCounter += 1;
                            $compOne = $functions['SpecializationID'];
                          }
                      ?>
                       <option value="<?php echo $functions['SpecializationID']?>" 
                     
                ><?php echo $functions['Specialization']?></option>
                       
                      <?php }
                      }
                        ?>
                      </select>
                    
                  </td>
                </tr>
              -->
                <tr>
                  <td>Urgent:</td>
                  <td>
                   <select name="isUrgent" class="form-control">
                    <option value="Yes" <?php if(isset($_GET['job_id'])){
                      if($isUrgent == "Yes"){
                        echo 'selected';
                      }
                    }
                    elseif(isset($_POST['save_btn'])){
                      echo $_POST['JobStatus'];
                    }?>>Yes</option>
                    <option value="No" <?php if(isset($_GET['job_id'])){
                      if($isUrgent == "No"){
                        echo 'selected';
                      }
                    }
                    elseif(isset($_POST['save_btn'])){
                      echo $_POST['JobStatus'];
                    }?>>No</option> 
                    </select>
                  </td>
                </tr>
                <tr>
                  <td>Job Description:</td>
                  <td>
                    <textarea id="editor1" name="job_description" style="width: 100%; height:200px;"><?php if(isset($_GET['job_id'])): echo $JobDescription; elseif(isset($_POST['save_button'])): echo $_POST['job_description']; endif;?></textarea>
                  </td>
                </tr>
                <tr>
                  <td></td>
                  <td>
                    <button class="btn btn-primary" name="save_button"><i class="fa fa-save"></i> Save</button>
                    <a href="job-category.php" class="btn btn-default"><i class="fa fa-arrow-left"></i> Back to list</a>
                  </td>
                </tr>
              </table>
              </form>

              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>

      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php include'../dist/assets/dashboard_footer.php';?>
<script type="text/javascript">
function changeCat(key){
  if(window.XMLHttpRequest){
  // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp = new XMLHttpRequest();
  }else{
  // code for IE6, IE5
    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange = function(){
    if(xmlhttp.readyState == 4 && xmlhttp.status == 200){
      document.getElementById("compList").innerHTML = xmlhttp.responseText;
      var comp = document.getElementById("compList").value;
      me(comp);
    }
  }
  xmlhttp.open("GET","changeCat.php?key="+key, true);
  xmlhttp.send(); 
}
</script>
