<?php 
include'../config/db.php';
include'../config/functions.php';
include'../config/myfunction.php';

if(empty($_SESSION['login_admin'])){ //This function is to check weather the account has been login or not
  header("Location: ../index.php");
  exit;
}
if(isset($_POST['save_button']) AND !isset($_GET['loc_id'])){
  $location = filter($_POST['location']);
  $details = filter($_POST['details']);

  $checkName = getSingleRow("*","Location","locations",$location);
  if($checkName['Location'] == $location){
    $msg = 'Location Name: '.$location.' already exist on database.';
  }
  else{
    $insertSQL = array("Location"=>$location,"Details"=>$details);
    SaveData("locations",$insertSQL);
    header("location: location.php");
  }
}
if(isset($_GET['loc_id'])){
  
  $fetchRow = fetchWhere("*","LocationID","locations",filter($_GET['loc_id']));
  if(!empty($fetchRow)){
    foreach ($fetchRow as $key => $row) {
      $location  = $row->Location;
      $details  = $row->Details;
    }
  }else{
    header("location: error.php");
  }
}
if(isset($_POST['save_button']) && isset($_GET['loc_id'])){
  $location = filter($_POST['location']);
  $details = filter($_POST['details']);
  
  $arr_where = array("LocationID"=>filter($_GET['loc_id']));//update where
  $arr_set = array("Location"=>$location,"Details"=>$details);//set update
  $tbl_name = "locations";
  $update = UpdateQuery($dbcon,$tbl_name,$arr_set,$arr_where);// UPDATE SQL
  header("location: location.php");
}
?>
<?php include'../dist/assets/dashboard_header.php';?>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
<?php include'../dist/assets/dashboard_nav.php';?>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Info boxes -->
        <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title"><i class="fa fa-plus"></i> Job Category</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">

                <?php if(isset($msg)):?><div class="alert alert-danger"><?php echo $msg;?></div><?php endif;?>
            <form method="post">
            <table class="table table-bordered">
                <tr>
                  <td>Location:</td>
                  <td>
                    <input type="text" name="location" class="form-control" placeholder="Location" required="required"
                    value="<?php if(isset($_GET['loc_id'])): echo $location; elseif(isset($_POST['save_button'])): echo $_POST['location']; endif;?>">
                  </td>
                </tr>
                <tr>
                  <td>Details:</td>
                  <td>
                    <input type="text" name="details" class="form-control" placeholder="Details" required="required"
                    value="<?php if(isset($_GET['loc_id'])): echo $details; elseif(isset($_POST['save_button'])): echo $_POST['details']; endif;?>">
                  </td>
                </tr>
                <tr>
                  <td></td>
                  <td>
                    <button class="btn btn-primary" name="save_button"><i class="fa fa-save"></i> Save</button>
                    <a href="job-category.php" class="btn btn-default"><i class="fa fa-arrow-left"></i> Back to list</a>
                  </td>
                </tr>
              </table>
              </form>

              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>

      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php include'../dist/assets/dashboard_footer.php';?>