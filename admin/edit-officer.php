<?php 
include'../config/db.php';
include'../config/functions.php';
include'../config/myfunction.php';
if(empty($_SESSION['login_admin'])){ 
//This function is to check weather the account has been login or not
  header("Location: ../index.php");
  exit;
}

if(isset($_POST['update_button'])){
  $FirstName = filter($_POST['FirstName']);
  $LastName = filter($_POST['LastName']);
  $ContactNumber = filter($_POST['ContactNumber']);
  $officer_address = filter($_POST['officer_address']);

  if(!is_numeric($ContactNumber)){
    $msg = 'Please enter valid number.';
  }elseif(is_numeric($FirstName)){
    $msg = 'Please enter letters only';
  }elseif(is_numeric($LastName)){
    $msg = 'Please enter letters only';
  }else{
    $arr_where = array("UserID"=>filter($_GET['user_id']));//update where
    $arr_set = array("FirstName"=>$FirstName,"LastName"=>$LastName);//set update
    $arr_set2 = array("ContactNumber"=>$ContactNumber, "OfficerAddress"=>$officer_address);
    $tbl_name = "accounts";
    $tbl_name2 = "recruitment_officers";

    $update = UpdateQuery($dbcon,$tbl_name,$arr_set,$arr_where);  
    $update2 = UpdateQuery($dbcon, $tbl_name2, $arr_set2,$arr_where);
    header("location: officer.php");
  }
}

if(isset($_GET['user_id'])){
  $user_id = $_GET['user_id'];
  if(checkOfficer()){
  }else{
    $list = fetchOfficer();
    if(!empty($list)){
      foreach ($list as $key => $value) {
        $FirstName = $value->FirstName;
        $LastName = $value->LastName;
        $ContactNumber = $value->ContactNumber;
        $officer_address = $value->OfficerAddress;
      }
    }else{
    }
  }
}
?>
<?php include'../dist/assets/dashboard_header.php';?>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
<?php include'../dist/assets/dashboard_nav.php';?>
</div>
</aside>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <br>
    <section class="content">
      <div class="container-fluid">
        <!-- Info boxes -->
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title"><i class="fa fa-plus"></i> Recruitment Officer Details</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
<?php if(isset($msg)):?>
<div class="alert alert-danger"><?php echo $msg;?></div>
<?php endif;?>
<form method="post">
<table class="table">
  <tr>
    <td>First Name:</td>
    <td>
      <input type="text" class="form-control" name="FirstName" placeholder="First Name" 
      value="<?php if(isset($_GET['user_id'])): echo $FirstName; endif;?>">
    </td>
  </tr>
   <tr>
    <td>Last Name:</td>
    <td>
      <input type="text" class="form-control" name="LastName" placeholder="Last Name" value="<?php if(isset($_GET['user_id'])): echo $LastName; endif;?>">
    </td>
  </tr>
   <tr>
    <td>Contact Number:</td>
    <td>
      <input type="text" class="form-control" name="ContactNumber" placeholder="Contact Number" value="<?php if(isset($_GET['user_id'])): echo $ContactNumber; endif;?>" maxlength="11">
    </td>
  </tr>
   <tr>
    <td>Address:</td>
    <td>
      <input type="text" class="form-control" name="officer_address" placeholder="Address" 
      value="<?php if(isset($_GET['user_id'])): echo $officer_address; endif;?>">
    </td>
  </tr>
   <tr>
     <td></td>
     <td>
       <button class="btn btn-primary" name="update_button">
         <i class="fa fa-save"></i> Save Data
       </button>
       <a href="officer.php" class="btn btn-danger"><i class="fa fa-arrow-left"></i>
        Return
       </a>
     </td>
   </tr>
</table>

</form>
              
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
 
        </div>

      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php include'../dist/assets/dashboard_footer.php';?>