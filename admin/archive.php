<?php 
include'../config/db.php';
include'../config/functions.php';
include'../config/myfunction.php';
if(empty($_SESSION['login_admin'])){ 
//This function is to check weather the account has been login or not
  header("Location: ../index.php");
  exit;
}
$list = fetchWhere("*","ArchiveID","jobposts","1"); 
if(isset($_GET['archive'])){
  $archive = filter($_GET['archive']);

  $arr_where = array("JobID"=>$archive);//update where
  $arr_set = array("ArchiveID"=>"0");//set update
  $tbl_name = "jobposts";
  $update = UpdateQuery($dbcon,$tbl_name,$arr_set,$arr_where);// UPDATE SQL
  header("location: archive.php");
}
?>
<?php include'../dist/assets/dashboard_header.php';?>
<style type="text/css">
  .txtfield{
    width:28%;
    height:38px;
  }
</style>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
<?php include'../dist/assets/dashboard_nav.php';?>
</div>
</aside>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <br>
    <section class="content">
      <div class="container-fluid">
        <!-- Info boxes -->
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title"><i class="fa fa-search"></i> Archive Jobs</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">


<?php  if(!empty($list)):?>
  <table id="example2" class="table">
    <thead>
      <tr>
        <th>Job Description</th>
      </tr>
    </thead>
    <tbody>
  <?php foreach ($list as $key => $value):?>
    <tr>
      <td>
      <div class="callout callout-default">
      <div class="row">
        <div class="col-md-10">
          <h4>
        <a href="" style="color:black;" data-toggle="modal" data-target="#view-job<?php echo $value->JobID?>">
        <?php echo $value->JobTitle?> 
        <?php if($value->isUrgent == 'No'):?>
        <?php else:?>
          -  <small style="color:red;">Urgent!</small>
        <?php endif;?>
      </a>
      </h4>
        </div>
        <div class="col-md-2">
          <a href="#" style="color:black;" <?php echo 'onclick=" confirm(\'Are you sure you want to retrieve this jobpost?\') 
      ?window.location = \'archive.php?archive='.$value->JobID.'\' : \'\';"'; ?>><i class="fa fa-book"></i>Retrieve Job Post</a>
        </div>
      </div>
      
      <hr>
      <?php 
      $company = getSingleRow("*","UserID","companyclients",$value->ClientID);
      $location = getSingleRow("*","LocationID","locations",$value->LocationID);
      ?>
      <div class="row">
        <div class="col-md-4"><i class="fa fa-home"></i> Company:<?php echo $company['CompanyName']?></div>
        <div class="col-md-4"><i class="fa fa-map"></i> Location: <?php echo $location['Location']?></div>
        <div class="col-md-4"><i class="fa fa-file"></i> Available Slots: <?php echo $value->Slots?></div>
      </div>
      <hr>
      <?php echo substr($value->JobDescription, 0,300)?>
      <div class="row">
        <div class="col-12">
          <h4>
              <small class="float-right" style="font-size:15px;color:#999;">Date Created: <?php echo date("Y-m-d",strtotime($value->DateCreated))?>
              
              </small>
          </h4>
        </div>
        
      </div>
    </div>
    <!-- View Full Job-->
<div class="modal fade" id="view-job<?php echo $value->JobID?>">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title">Job Details</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                
              </div>
              <div class="modal-body">
              <?php 
              $viewjob = getSingleRow("*","JobID","jobposts",$value->JobID);
              $company = getSingleRow("*","UserID","companyclients",$value->ClientID);
              ?>
              <div class="row">
                <div class="col-md-4">Company Name:</div>
                <div class="col-md-8"><?php echo $company['CompanyName']?></div>
                <div class="col-md-4">Job Title:</div>
                <div class="col-md-8"><?php echo $value->JobTitle?></div>
                <div class="col-md-4">Job Status:</div>
                <div class="col-md-8"><?php echo $value->JobStatus?></div>
                <div class="col-md-4">Available Slots:</div>
                <div class="col-md-8"><?php echo $value->Slots?></div>
                <div class="col-md-4">Expected Salary:</div>
                <div class="col-md-8"><?php echo $value->ExpectedSalary?></div>
              </div>
              <hr>
              <strong>Job Description:</strong>
              <?php echo $value->JobDescription?>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
</div>
<!-- end -->
                  </td>
                </tr>
<?php endforeach;?>
</table>
<?php else:?>
<?php endif;?> 

</div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
 
        </div>

      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php include'../dist/assets/dashboard_footer.php';?>
<script type="text/javascript">
function changeCat(key){
  if(window.XMLHttpRequest){
  // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp = new XMLHttpRequest();
  }else{
  // code for IE6, IE5
    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange = function(){
    if(xmlhttp.readyState == 4 && xmlhttp.status == 200){
      document.getElementById("compList").innerHTML = xmlhttp.responseText;
      var comp = document.getElementById("compList").value;
      me(comp);
    }
  }
  xmlhttp.open("GET","changeCat.php?key="+key, true);
  xmlhttp.send(); 
}
</script>

