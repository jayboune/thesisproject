<?php 
include'../config/db.php';
include'../config/functions.php';
include'../config/myfunction.php';

if(empty($_SESSION['login_admin'])){ //This function is to check weather the account has been login or not
  header("Location: ../index.php");
  exit;
}
if(isset($_POST['save_button']) AND !isset($_GET['JobID'])){
  $category = filter($_POST['category']);
  $user = filter($_SESSION['email_address']);

  $checkName = getSingleRow("*","Category","jobcategories",$category);
  if($checkName['Category'] == $category){
    $msg = 'Category Name: '.$category.' already exist on database.';
  }
  else{
    $insertSQL = array("Category"=>$category);
    SaveData("jobcategories",$insertSQL);
    header("location: job-category.php");
  }
}
if(isset($_GET['JobID'])){
  
  $fetchRow = fetchWhere("*","JCategoryID","jobcategories",filter($_GET['JobID']));
  if(!empty($fetchRow)){
    foreach ($fetchRow as $key => $row) {
      $category  = $row->Category;
    }
  }else{
    header("location: error.php");
  }
}
if(isset($_POST['save_button']) && isset($_GET['JobID'])){
  $category =filter($_POST['category']);
  $user = $_SESSION['email_address'];
  $date = date("Y-m-d h:i:s"); 
  
  $arr_where = array("JCategoryID"=>filter($_GET['JobID']));//update where
  $arr_set = array("Category"=>$category);//set update
  $tbl_name = "jobcategories";
  $update = UpdateQuery($dbcon,$tbl_name,$arr_set,$arr_where);// UPDATE SQL
  header("location: job-category.php");
}
?>
<?php include'../dist/assets/dashboard_header.php';?>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
<?php include'../dist/assets/dashboard_nav.php';?>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Info boxes -->
        <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title"><i class="fa fa-plus"></i> Job Category</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">

                <?php if(isset($msg)):?><div class="alert alert-danger"><?php echo $msg;?></div><?php endif;?>
            <form method="post">
            <table class="table table-bordered">
                <tr>
                  <td>Category Name:</td>
                  <td>
                    <input type="text" name="category" class="form-control" placeholder="Category Name" required="required"
                    value="<?php if(isset($_GET['JobID'])): echo $category; elseif(isset($_POST['save_button'])): echo $_POST['category']; endif;?>">
                  </td>
                </tr>
                <tr>
                  <td></td>
                  <td>
                    <button class="btn btn-primary" name="save_button"><i class="fa fa-save"></i> Save</button>
                    <a href="job-category.php" class="btn btn-default"><i class="fa fa-arrow-left"></i> Back to list</a>
                  </td>
                </tr>
              </table>
              </form>

              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>

      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php include'../dist/assets/dashboard_footer.php';?>