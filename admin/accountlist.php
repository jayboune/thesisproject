<?php 
include'../config/db.php';
include'../config/functions.php';
include'../config/myfunction.php';
if(empty($_SESSION['login_admin'])){ 
//This function is to check weather the account has been login or not
  header("Location: ../index.php");
  exit;
}
$applicant = fetchAll("*","applicants"); 
// SELECT all data from the accounts table where usertype =1
$officer = getOfficer(); // SELECT all data from the accounts and recruitment officer table
$company = fetchAll("*","companyclients"); 
// SELECT all data from the accounts table where usertype =1
?>
<?php include'../dist/assets/dashboard_header.php';?>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
<?php include'../dist/assets/dashboard_nav.php';?>
</div>
</aside>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <br>
    <section class="content">
      <div class="container-fluid">
        <!-- Info boxes -->
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title"><i class="fa fa-users"></i> Account List</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
<form method="post">
  <select name="accounttype" class="form-control">
    <option value="1">Applicant</option>
    <option value="2">Company Client</option>
    <option value="3">Recruitment Officer</option>
  </select>
  <br>
  <button name="search_btn"><i class="fa fa-search"></i> Search</button>
</form>
<?php if(isset($_POST['search_btn']) AND $_POST['accounttype'] == '1'): ?>
              <?php  if(!empty($applicant)):?>
                <table id="example1" class="table table-bordered table-striped" style="font-size:12px;">
                <thead>
                <tr>
                  <th>Full Name</th>
                  <th>Email</th>
                  <th>Contact Number</th>
                </tr>
                </thead>
                <tbody>
              <?php foreach ($applicant as $key => $value):?>
                <tr>
                  <td>
                    <?php $result = getSingleRow("*","UserID","accounts",$value->UserID);?>
                    <?php echo $result['FirstName']?> <?php echo $result['LastName']?>
                    </td>
                  <td><?php echo $value->BirthDate?></td>
                  <td><?php echo $value->ContactNumber?></td>
                  <td><?php echo $value->Address?></td>
                  <td>
                    <?php if(empty($value->ApplicantCV)):?>No CV uploaded<?php else:?><a href="../img/<?php echo $value->ApplicantCV?>">Download CV</a><?php endif;?>
                  </td>
                </tr>
              <?php endforeach;?>
              </table>
              <?php else:?>
                <div class="alert alert-danger">There are no records on the database.</div>
              <?php endif;?>
              </div>
<?php elseif(isset($_POST['search_btn']) AND $_POST['accounttype'] == '2'):?>
  <?php  if(!empty($company)):?>
                <table id="example1" class="table table-bordered table-striped" style="font-size:12px;">
                <thead>
                <tr>
                  <th>Company Name</th>
                  <th>Email Address</th>
                  <th>Company Address</th>
                  <th>Contact Number</th>
                  <th>Status</th>
                </tr>
                </thead>
                <tbody>
              <?php foreach ($company as $key => $value):?>
                <tr>
                  <td><?php echo $value->CompanyName?></td>
                  <td><?php echo $value->CompanyEmail?></td>
                  <td><?php echo $value->CompanyAddress?></td>
                  <td><?php echo $value->ContactNumber?></td>
                  <td>
                    <?php $status = getSingleRow("*","UserID","accounts",$value->UserID);?>
                    <?php if($status['UserStatus'] == '0'):?>Unverfied<?php else:?>Verfied<?php endif;?>
                  </td>
                  
                </tr>
              <?php endforeach;?>
              </table>
              <?php else:?>
                <div class="alert alert-danger">There are no records on the database</div>
              <?php endif;?>
<?php elseif(isset($_POST['search_btn']) AND $_POST['accounttype'] == '3'):?>
  <?php  if(!empty($officer)):?>
                <table id="example1" class="table table-bordered table-striped" style="font-size:12px;">
                <thead>
                <tr>
                  <th>Full Name</th>
                  
                  <th>Contact Number</th>
                  <th>Address</th>
                  <th>Email Address</th>
                </tr>
                </thead>
                <tbody>
              <?php foreach ($officer as $key => $value):?>
                <tr>
                  <td>
                    <?php $result = getSingleRow("*","UserID","accounts",$value->UserID);?>
                    <?php echo $result['FirstName']?> <?php echo $result['LastName']?>
                  </td>
                  <td><?php echo $value->ContactNumber?></td>
                  <td><?php echo $value->OfficerAddress?></td>
                  <td><?php echo $result['EmailAddress']?></td>
                  
                </tr>
              <?php endforeach;?>
              </table>
              <?php else:?>
                <div class="alert alert-danger">There are no records on the database.</div>
              <?php endif;?>
<?php endif;?>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
 
        </div>

      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php include'../dist/assets/dashboard_footer.php';?>