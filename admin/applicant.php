<?php 
include'../config/db.php';
include'../config/functions.php';
include'../config/myfunction.php';
if(empty($_SESSION['login_admin'])){ 
//This function is to check weather the account has been login or not
  header("Location: ../index.php");
  exit;
}
$applicant = fetchAll("*","applicants"); 
// SELECT all data from the accounts table where usertype =1
?>
<?php include'../dist/assets/dashboard_header.php';?>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
<?php include'../dist/assets/dashboard_nav.php';?>
</div>
</aside>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <br>
    <section class="content">
      <div class="container-fluid">
        <!-- Info boxes -->
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title"><i class="fa fa-users"></i> Applicant List</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
              <?php  if(!empty($applicant)):?>
                <table id="example1" class="table table-bordered table-striped" style="font-size:12px;">
                <thead>
                <tr>
                  <th>Full Name</th>
                  <th>Birthdate</th>
                  <th>Contact Number</th>
                  <th>Address</th>
                  <th>Applicant CV</th>
                </tr>
                </thead>
                <tbody>
              <?php foreach ($applicant as $key => $value):?>
                <tr>
                  <td>
                    <?php $result = getSingleRow("*","UserID","accounts",$value->UserID);?>
                    <?php echo $result['FirstName']?> <?php echo $result['LastName']?>
                    </td>
                  <td><?php echo $value->BirthDate?></td>
                  <td><?php echo $value->ContactNumber?></td>
                  <td><?php echo $value->Address?></td>
                  <td>
                    <?php if(empty($value->ApplicantCV)):?>No CV uploaded<?php else:?><a href="../img/<?php echo $value->ApplicantCV?>">Download CV</a><?php endif;?>
                  </td>
                </tr>
              <?php endforeach;?>
              </table>
              <?php else:?>
                <div class="alert alert-danger">There are no records on the database.</div>
              <?php endif;?>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
 
        </div>

      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php include'../dist/assets/dashboard_footer.php';?>