<?php 
include'../config/db.php';
include'../config/functions.php';
include'../config/myfunction.php';

if(empty($_SESSION['login_admin'])){ //This function is to check weather the account has been login or not
  header("Location: ../index.php");
  exit;
}
$specialization = fetchAll("*","specializations");
if(isset($_GET['delete'])){ // Deleting records on the database.
  $delete = filter($_GET['delete']);
  $ar = array("SpecializationID"=>$delete); //WHERE statement
  $tbl_name = "specializations"; 
  $del = Delete($dbcon,$tbl_name,$ar);
  if($del){
    header("location: specialization.php");
  }
}
?>
<?php include'../dist/assets/dashboard_header.php';?>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
<?php include'../dist/assets/dashboard_nav.php';?>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Info boxes -->
        <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title"><i class="fa fa-plus"></i> Job Category</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
<?php if(!empty($specialization)):?>
  <table id="example1" class="table table-bordered table-striped" style="font-size:12px;">
    <thead>
      <tr>
        <th>Specialization</th>
        <th>Category Name</th>
        <th>Option</th>
      </tr>
    </thead>
  <tbody>
    <?php foreach ($specialization as $key => $value):?>
      <tr>
        <th><?php echo $value->Specialization?></th>
        <th>
          <?php 
          $cat = getSingleRow("Category","JCategoryID","jobcategories",$value->JCategoryID);
          echo $cat['Category'];
          ?>
        </th>
        <th>
          <a href="add-specialization.php?sID=<?php echo $value->SpecializationID?>" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i> Update</a>
          <a href="#" <?php echo 'onclick=" confirm(\'Are you sure you want to Delete?\') 
      ?window.location = \'specialization.php?delete='.$value->SpecializationID.'\' : \'\';"'; ?> class="btn btn-danger btn-sm"><i class="fa fa-remove"></i> Delete</a>
        </th>
      </tr>
    <?php endforeach;?>
  </table>
<?php else:?>
  <div class="alert alert-danger">There are no records on the database.</div>
<?php endif;?>

              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>

      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php include'../dist/assets/dashboard_footer.php';?>