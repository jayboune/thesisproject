<?php 
include'../config/db.php';
include'../config/functions.php';
include'../config/myfunction.php';
if(empty($_SESSION['login_admin'])){ 
//This function is to check weather the account has been login or not
  header("Location: ../index.php");
  exit;
}
if(isset($_POST['save_button'])){
  $FirstName = filter($_POST['FirstName']);
  $LastName = filter($_POST['LastName']);
  $ContactNumber = filter($_POST['ContactNumber']);
  $email_address = filter($_POST['email_address']);
  $officer_address = filter($_POST['officer_address']);
  $user_pass = md5('P@ssword1');

  $insertAccounts = array("FirstName"=>$FirstName,"LastName"=>$LastName,
    "EmailAddress"=>$email_address,"Password"=>$user_pass,"UserType"=>"3","UserStatus"=> "1");
  SaveData("accounts",$insertAccounts); // Inserting data in accounts table
  $id = mysqli_insert_id($dbcon);

  $insertOfficer = array("ContactNumber"=>$ContactNumber,"UserID"=>$id,"OfficerAddress"=>$officer_address);
  SaveData("recruitment_officers",$insertOfficer);
  header("location: officer.php");
}

?>
<?php include'../dist/assets/dashboard_header.php';?>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
<?php include'../dist/assets/dashboard_nav.php';?>
</div>
</aside>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <br>
    <section class="content">
      <div class="container-fluid">
        <!-- Info boxes -->
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title"><i class="fa fa-plus"></i> Recruitment Officer Details</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
<form method="post">
<table class="table">
  <tr>
    <td>First Name:</td>
    <td>
      <input type="text" class="form-control" name="FirstName" placeholder="First Name" 
      value="<?php if(isset($_POST['save_button'])): echo $FirstName; endif;?>">
    </td>
  </tr>
   <tr>
    <td>Last Name:</td>
    <td>
      <input type="text" class="form-control" name="LastName" placeholder="Last Name" value="<?php if(isset($_POST['save_button'])): echo $LastName; endif;?>">
    </td>
  </tr>
   <tr>
    <td>Contact Number:</td>
    <td>
      <input type="text" class="form-control" name="ContactNumber" placeholder="Contact Number" value="<?php if(isset($_POST['save_button'])): echo $ContactNumber; endif;?>">
    </td>
  </tr>
   <tr>
    <td>Address:</td>
    <td>
      <input type="text" class="form-control" name="officer_address" placeholder="Address" 
      value="<?php if(isset($_POST['save_button'])): echo $officer_address; endif;?>">
    </td>
  </tr>
   <tr>
    <td>Email Address:</td>
    <td>
      <input type="email" class="form-control" name="email_address" placeholder="Email Address" value="<?php if(isset($_POST['save_button'])): echo $email_address; endif;?>">
    </td>
  </tr>
   <tr>
     <td></td>
     <td>
       <button class="btn btn-primary" name="save_button">
         <i class="fa fa-save"></i> Save Data
       </button>
       <a href="officer.php" class="btn btn-danger"><i class="fa fa-arrow-left"></i>
        Return
       </a>
     </td>
   </tr>
</table>

</form>
              
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
 
        </div>

      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php include'../dist/assets/dashboard_footer.php';?>