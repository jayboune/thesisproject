<?php 
include'../config/db.php';
include'../config/functions.php';
include'../config/myfunction.php';
if(empty($_SESSION['login_admin'])){ 
//This function is to check weather the account has been login or not
  header("Location: ../index.php");
  exit;
}

if(isset($_POST['save'])){
  $FirstName = filter($_POST['FirstName']);
  $LastName = filter($_POST['LastName']);
  $ContactNumber = filter($_POST['ContactNumber']);
  $email_address = filter($_POST['email_address']);
  $officer_address = filter($_POST['officer_address']);
  $user_pass = filter($_POST['user_pass']);

  $insertAccounts = array("FirstName"=>$FirstName,"LastName"=>$LastName,
    "EmailAddress"=>$email_address,"Password"=>md5($user_pass),"UserType"=>"3","UserStatus"=> "1");
  SaveData("accounts",$insertAccounts); // Inserting data in accounts table
  $id = mysqli_insert_id($dbcon);

  $insertOfficer = array("ContactNumber"=>$ContactNumber,"UserID"=>$id,"OfficerAddress"=>$officer_address);
  SaveData("recruitment_officers",$insertOfficer);
  header("location: officer.php");
}

elseif(isset($_POST['save_button'])){
  $FirstName = filter($_POST['FirstName']);
  $LastName = filter($_POST['LastName']);
  $CompanyName = filter($_POST['CompanyName']);
  $user_pass = filter($_POST['user_pass']);
  $ContactNumber = filter($_POST['ContactNumber']);
  $email_address = filter($_POST['email_address']);
  $officer_address = filter($_POST['officer_address']);

  $checkemail = getSingleRow("EmailAddress","EmailAddress","accounts",$email_address);
  $checkCompany = getSingleRow("CompanyName","CompanyName","companyclients",$CompanyName);

  if(checkname($FirstName,$LastName)){
    $msg = 'Name: '.$FirstName.' '.$LastName.' already exist. ';
  }
  
  elseif($checkemail['EmailAddress'] == $email_address){
    $msg = 'Email Address: '.$email_address.' already exist';
  }elseif($checkCompany['CompanyName'] == $CompanyName){
    $msg = 'Company Name: '.$CompanyName.' already exist';
  }
  
  else{
    $accountArray = array("FirstName"=>$FirstName, "LastName"=>$LastName,"EmailAddress"=>$email_address,"Password"=>md5($user_pass),"UserType"=>"2", "UserStatus" => "1");
    SaveData("accounts",$accountArray);

    $last_id = mysqli_insert_id($dbcon);

    $employerArray = array("UserID"=>$last_id, "CompanyEmail"=>$email_address,"CompanyName"=>$CompanyName, "ContactNumber" =>$ContactNumber, "CompanyAddress" => $CompanyAddress, "CompanyAddress" =>$officer_address);
    SaveData("companyclients",$employerArray);
    header("location: company.php");
}
}
?>
<?php include'../dist/assets/dashboard_header.php';?>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
<?php include'../dist/assets/dashboard_nav.php';?>
</div>
</aside>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <br>
    <section class="content">
      <div class="container-fluid">
        <!-- Info boxes -->
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title"><i class="fa fa-plus"></i> Create Account</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
<?php if(isset($msg)): echo $msg; endif;?>
<form method="post">
  <div class="row">
    <div class="col-md-8">
    <select class="form-control" name="account_type">
      <option value="1" <?php if(isset($_POST['search_btn'])){
                if($_POST['account_type'] == "1"){
                  echo 'selected';
                }
              }
             
                ?>>Company</option>
      <option value="2" <?php if(isset($_POST['search_btn'])){
                if($_POST['account_type'] == "2"){
                  echo 'selected';
                }
              }
              
                ?>>Recruitment Officer</option>
    </select>
    </div>
    <div class="col-md-4">
      <button class="btn btn-primary" name="search_btn">Select Type</button>
    </div>
  </div>
  <br> 
</form>
<?php if(isset($_POST['search_btn']) AND $_POST['account_type'] == '1'):?>
<form method="post">
<table class="table">
  <tr>
    <td>Company Name:</td>
    <td>
      <input type="text" name="CompanyName" class="form-control" placeholder="Company Name" required="required" value="<?php if(isset($_POST['save_button'])): echo $_POST['CompanyName']; endif;?>">
    </td>
  </tr>
  <tr>
    <td>First Name:</td>
    <td>
       <input type="text" name="FirstName" class="form-control" placeholder="First Name" required="required" value="<?php if(isset($_POST['save_button'])): echo $_POST['FirstName']; endif;?>">
    </td>
  </tr>
  <tr>
    <td>Last Name:</td>
    <td>
       <input type="text" name="LastName" class="form-control" placeholder="Last Name" required="required" value="<?php if(isset($_POST['save_button'])): echo $_POST['LastName']; endif;?>">
    </td>
  </tr>
  <tr>
    <td>Contact Number:</td>
    <td>
      <input type="text" class="form-control" name="ContactNumber" placeholder="Contact Number" value="<?php if(isset($_POST['save_button'])): echo $ContactNumber; endif;?>">
    </td>
  </tr>
  <tr>
    <td>Email Address:</td>
    <td>
      <input type="email" class="form-control" name="email_address" placeholder="Email Address" value="<?php if(isset($_POST['save_button'])): echo $email_address; endif;?>">
    </td>
  </tr>
  <tr>
    <td>Address:</td>
    <td>
      <input type="text" class="form-control" name="officer_address" placeholder="Address" 
      value="<?php if(isset($_POST['save'])): echo $officer_address; endif;?>">
    </td>
  </tr>
  <tr>
    <td>Password:</td>
    <td>
      <input type="password" name="user_pass" class="form-control" placeholder="Password" required="required">
    </td>
  </tr>
  <tr>
    <td></td>
    <td>
      <button class="btn btn-primary" name="save_button"><i class="fa fa-save"></i> Signup</button>
      <a href="index.php" class="btn btn-danger"><i class="fa fa-arrow-left"></i> Return</a>
    </td>
  </tr>
</table>

              </form>
<?php elseif(isset($_POST['search_btn']) AND $_POST['account_type'] == '2'):?>
  <form method="post">
<table class="table">
  <tr>
    <td>First Name:</td>
    <td>
      <input type="text" class="form-control" name="FirstName" placeholder="First Name" 
      value="<?php if(isset($_POST['save'])): echo $FirstName; endif;?>">
    </td>
  </tr>
   <tr>
    <td>Last Name:</td>
    <td>
      <input type="text" class="form-control" name="LastName" placeholder="Last Name" value="<?php if(isset($_POST['save'])): echo $LastName; endif;?>">
    </td>
  </tr>
   <tr>
    <td>Contact Number:</td>
    <td>
      <input type="text" class="form-control" name="ContactNumber" placeholder="Contact Number" value="<?php if(isset($_POST['save'])): echo $ContactNumber; endif;?>">
    </td>
  </tr>
   <tr>
    <td>Address:</td>
    <td>
      <input type="text" class="form-control" name="officer_address" placeholder="Address" 
      value="<?php if(isset($_POST['save'])): echo $officer_address; endif;?>">
    </td>
  </tr>
   <tr>
    <td>Email Address:</td>
    <td>
      <input type="email" class="form-control" name="email_address" placeholder="Email Address" value="<?php if(isset($_POST['save'])): echo $email_address; endif;?>">
    </td>
  </tr>
  <tr>
    <td>Password:</td>
    <td>
      <input type="password" name="user_pass" class="form-control" placeholder="Password" required="required">
    </td>
  </tr>
   <tr>
     <td></td>
     <td>
       <button class="btn btn-primary" name="save">
         <i class="fa fa-save"></i> Save Data
       </button>
       <a href="officer.php" class="btn btn-danger"><i class="fa fa-arrow-left"></i>
        Return
       </a>
     </td>
   </tr>
</table>

</form>
<?php endif;?>
              
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
 
        </div>

      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php include'../dist/assets/dashboard_footer.php';?>