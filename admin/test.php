<form action="test.php" method="post">
    <select name="Particulars">
        <option value="Username">Username</option>
        <option value="Email">Email</option>
        <option value="Address">Address</option>
    </select>
    <!--UserName TextField-->
    <input name="username" type="text" style="display: none">
    <input type="submit" name="submit" value="Get Selected Values" />
</form>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js"></script>

<script type="text/javascript">
//To show if default option Username is selected
$(document).ready(function(){
    if($('select[name=Particulars]').val() == "Username"){
        $('input[name=username]').show();
    }
});

//With changing dropdownlist, if Username is selected then text field will appear.
$('select[name=Particulars]').on('change', function(){
    if($(this).val() == "Username"){
        $('input[name=username]').show();
    }else {
        $('input[name=username]').hide().val('');
    }
});
</script>