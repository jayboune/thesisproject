<?php 
include'../config/db.php';
include'../config/functions.php';
include'../config/myfunction.php';

if(empty($_SESSION['login_admin'])){ //This function is to check weather the account has been login or not
  header("Location: ../index.php");
  exit;
}
if(isset($_POST['save_button']) AND !isset($_GET['sID'])){
  $SpecializationName = filter($_POST['SpecializationName']);
  $JobID = filter($_POST['JobID']);
  

  $checkName = getSingleRow("*","Specialization","specializations",$SpecializationName);
  if($checkName['Specialization'] == $SpecializationName){
    $msg = 'Name: '.$SpecializationName.' already exist on database.';
  }
  else{
    $insertSQL = array("Specialization"=>$SpecializationName,"JCategoryID"=>$JobID);
    SaveData("specializations",$insertSQL);
    header("location: specialization.php");
  }
}
if(isset($_GET['sID'])){
  
  $fetchRow = fetchWhere("*","SpecializationID","specializations",filter($_GET['sID']));
  if(!empty($fetchRow)){
    foreach ($fetchRow as $key => $row) {
      $CatID  = $row->JCategoryID;
      $SpecializationName = $row->Specialization;
    }
  }else{
    header("location: error.php");
  }
}
if(isset($_POST['save_button']) && isset($_GET['sID'])){
  $SpecializationName = filter($_POST['SpecializationName']);
  $JobID = filter($_POST['JobID']);
  //$user = filter($_SESSION['email_address']);
  //$date = date("Y-m-d h:i:s"); 
  
  $arr_where = array("SpecializationID"=>filter($_GET['sID']));//update where
  $arr_set = array("Specialization"=>$SpecializationName,"JCategoryID"=>$JobID);//set update
  $tbl_name = "specializations";
  $update = UpdateQuery($dbcon,$tbl_name,$arr_set,$arr_where);// UPDATE SQL
  header("location: specialization.php");
}
?>
<?php include'../dist/assets/dashboard_header.php';?>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
<?php include'../dist/assets/dashboard_nav.php';?>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Info boxes -->
        <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title"><i class="fa fa-plus"></i> Specialization</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">

                <?php if(isset($msg)):?><div class="alert alert-danger"><?php echo $msg;?></div><?php endif;?>
            <form method="post">
            <table class="table table-bordered">
                <tr>
                  <td>Name:</td>
                  <td>
                    <input type="text" name="SpecializationName" class="form-control" placeholder="Name" required="required"
                    value="<?php if(isset($_GET['sID'])): echo $SpecializationName; elseif(isset($_POST['save_button'])): echo $_POST['SpecializationName']; endif;?>">
                  </td>
                </tr>
                <tr>
                  <td>Category:</td>
                  <td>
            <select name="JobID" class="form-control">
              <?php $list = fetchAll("*","jobcategories");?>
              <?php foreach ($list as $key => $value):?>
              <option value="<?php echo $value->JCategoryID?>"
              <?php if(isset($_GET['sID'])){
                if($CatID == $value->JCategoryID){
                  echo 'selected';
                }
              }
              elseif(isset($_POST['save_btn'])){
                  echo $_POST['JobID'];
                }
                ?>><?php echo $value->Category?></option>
            <?php endforeach;?>
            </select>
                  </td>
                </tr>
                <tr>
                  <td></td>
                  <td>
                    <button class="btn btn-primary" name="save_button"><i class="fa fa-save"></i> Save</button>
                    <a href="job-category.php" class="btn btn-default"><i class="fa fa-arrow-left"></i> Back to list</a>
                  </td>
                </tr>
              </table>
              </form>

              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>

      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php include'../dist/assets/dashboard_footer.php';?>