<?php 
include'../config/db.php';
include'../config/functions.php';
include'../config/myfunction.php';

if(empty($_SESSION['login_admin'])){ //This function is to check weather the account has been login or not
  header("Location: ../index.php");
  exit;
}
$location = fetchAll("*","locations");
if(isset($_GET['delete'])){ // Deleting records on the database.
  $delete = filter($_GET['delete']);
  $ar = array("LocationID"=>$delete); //WHERE statement
  $tbl_name = "locations"; 
  $del = Delete($dbcon,$tbl_name,$ar);
  if($del){
    header("location: location.php");
  }
}
?>
<?php include'../dist/assets/dashboard_header.php';?>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
<?php include'../dist/assets/dashboard_nav.php';?>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Info boxes -->
        <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title"><i class="fa fa-globe"></i> Location</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
<a href="add-location.php" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Add location</a>
<br><br>
<?php if(!empty($location)):?>
  <table id="example1" class="table table-bordered table-striped" style="font-size:12px;">
    <thead>
      <tr>
        <th>Location</th>
        <th>Details</th>
        <th>Option</th>
      </tr>
    </thead>
  <tbody>
    <?php foreach ($location as $key => $value):?>
      <tr>
        <th><?php echo $value->Location?></th>
        <th><?php echo $value->Details?></th>
        <th>
          <a href="add-location.php?loc_id=<?php echo $value->LocationID?>" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i> Update</a>
          <a href="#" <?php echo 'onclick=" confirm(\'Are you sure you want to Delete?\') 
      ?window.location = \'location.php?delete='.$value->LocationID.'\' : \'\';"'; ?> class="btn btn-danger btn-sm"><i class="fa fa-remove"></i> Delete</a>
        </th>
      </tr>
    <?php endforeach;?>
  </table>
<?php else:?>
  <div class="alert alert-danger">There are no records on the database.</div>
<?php endif;?>

              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>

      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php include'../dist/assets/dashboard_footer.php';?>