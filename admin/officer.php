<?php 
include'../config/db.php';
include'../config/functions.php';
include'../config/myfunction.php';
if(empty($_SESSION['login_admin'])){ 
//This function is to check weather the account has been login or not
  header("Location: ../index.php");
  exit;
}
$officer = getOfficer(); // SELECT all data from the accounts and recruitment officer table

if(isset($_GET['delete'])){ // Deleting records on the database.
  $delete = filter($_GET['delete']);
  $ar = array("UserID"=>$delete); //WHERE statement
  $tbl_name = "accounts"; //table name
  $del = delete($dbcon,$tbl_name,$ar);
  if($del){
    $ar2 = array("UserID"=>$delete); //WHERE statement
    $tbl_name2 = "recruitment_officers"; //table name
    $del = delete($dbcon,$tbl_name2,$ar2
    );
    header("location: officer.php");
  }
}
?>
<?php include'../dist/assets/dashboard_header.php';?>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
<?php include'../dist/assets/dashboard_nav.php';?>
</div>
</aside>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <br>
    <section class="content">
      <div class="container-fluid">
        <!-- Info boxes -->
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title"><i class="fa fa-users"></i> Recruitement Officers</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
              <a href="add-officer.php" class="btn btn-primary"><i class="fa fa-plus"></i> Add Account</a>
              <br><br>
              <?php  if(!empty($officer)):?>
                <table id="example1" class="table table-bordered table-striped" style="font-size:12px;">
                <thead>
                <tr>
                  <th>Full Name</th>
                  
                  <th>Contact Number</th>
                  <th>Address</th>
                  <th>Email Address</th>
                  <th>Option</th>
                </tr>
                </thead>
                <tbody>
              <?php foreach ($officer as $key => $value):?>
                <tr>
                  <td>
                    <?php $result = getSingleRow("*","UserID","accounts",$value->UserID);?>
                    <?php echo $result['FirstName']?> <?php echo $result['LastName']?>
                  </td>
                  <td><?php echo $value->ContactNumber?></td>
                  <td><?php echo $value->OfficerAddress?></td>
                  <td><?php echo $result['EmailAddress']?></td>
                  <td>
                   <div class="btn-group">
                    <button type="button" class="btn btn-info">View</button>
                    <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                      <span class="caret"></span>
                      <span class="sr-only">Toggle Dropdown</span>
                    </button>
<ul class="dropdown-menu" role="menu">
  <li style="margin:5px;"><a href="edit-officer.php?user_id=<?php echo $value->UserID?>">Edit</a></li>
  <li style="margin:5px;"><a href="#" <?php echo 'onclick=" confirm(\'Are you sure you want to delete?\') 
      ?window.location = \'officer.php?delete='.$value->UserID.'\' : \'\';"'; ?>>Delete</a></li>
</ul>
        </div>
                  </td>
                </tr>
              <?php endforeach;?>
              </table>
              <?php else:?>
                <div class="alert alert-danger">There are no records on the database.</div>
              <?php endif;?>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
 
        </div>

      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php include'../dist/assets/dashboard_footer.php';?>