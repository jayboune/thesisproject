<?php 
include'config/db.php';
include'config/functions.php';
include'config/myfunction.php';

$list =fetchWhere("*","ArchiveID","jobposts","0");
if(isset($_SESSION['login_admin']) == 'login_admin')
{
    header("location: admin/");
}

if(isset($_SESSION['login_applicant']) == 'login_applicant')
{
    header("location: applicant/");
}

if(isset($_SESSION['login_company']) == 'login_company')
{
    header("location: company/");
}
if(isset($_SESSION['login_recruiter']) == 'login_recruiter')
{
    header("location: recruiter/");
}
   
?>
<?php include'dist/assets/header.php';?>
    <main role="main" style="">
      <div class="container marketing" style="margin-top:10%;">
<h3 class="card-title"><i class="fa fa-search"></i> Job List</h3>
<hr>
        <form method="get">
  <div class="form-row">
    <div class="col-md-4">
      <input type="text" name="search_field" placeholder="Search Job" class="form-control">
    </div>
    <div class="col-md-3">
<select class="form-control" required = 'required' name = "cat_id" id="functionList" onchange="changeCat(this.value)">
  <option value="all">All</option>
  <?php
    $catCounter = 0;
    $field = $dbcon->query("SELECT * FROM jobcategories") or die(mysqli_error());
    while($fields = $field->fetch_assoc()){
      if($catCounter == 0){
        $catCounter += 1;
        $functionOne = $fields['JCategoryID'];
      }
  ?>
    <option value="<?php echo $fields['JCategoryID']?>"><?php echo $fields['Category']?></option>
  <?php   
  }
  ?>
</select>
    </div>
    <div class="col-md-3">
      <select class="form-control" required = 'required' name="sub_id" id="compList" onchange="me(this.value)" >
        <option value="all">All</option>
        <?php
          $funcCounter = 0;
          $function = $dbcon->query("SELECT * FROM specializations WHERE JCategoryID= ".$functionOne) or die(mysqli_error());
          if(mysqli_num_rows($function) == 0){
            echo '<option>Select Data</option>';
          }else{
          while($functions = $function->fetch_assoc()){
            if($funcCounter == 0){
              $funcCounter += 1;
              $compOne = $functions['SpecializationID'];
          }
        ?>
        <option value="<?php echo $functions['SpecializationID']?>"><?php echo $functions['Specialization']?></option>
                       
        <?php }
          }
        ?>
      <select>
    </div>
    <div class="col-md-2">
    <button class="btn btn-primary" name="search_button"><i class="fa fa-search"></i> Search Job</button>
    </div>
  </div>
</form>
<br>
<?php if(isset($_GET['search_button']) AND isset($_POST['sub_id']) == 'all' AND isset($_POST['cat_id']) == 'all'):?>
<?php $job = searchJobAll();?>
  <?php if(!empty($job)):?>
  <?php foreach ($job as $key => $value):?>
        <div class="callout callout-default">
      <div class="row">
        <div class="col-md-10">
          <h4>
        <a href="" style="color:black;" data-toggle="modal" data-target="#view-job<?php echo $value->JobID?>">
        <?php echo $value->JobTitle?> 
        <?php if($value->isUrgent == 'No'):?>
        <?php else:?>
          -  <small style="color:red;">Urgent!</small>
        <?php endif;?>
      </a>
      </h4>
        </div>
        <div class="col-md-2">
          
        </div>
      </div>
      
      <hr>
      <div class="row">
        <div class="col-md-4"><i class="fa fa-home"></i> Company:<?php echo $value->CompanyName?></div>
        <div class="col-md-4"><i class="fa fa-map"></i> Location: <?php echo $value->Location?></div>
        <div class="col-md-4"><i class="fa fa-file"></i> Available Slots: <?php echo $value->Slots?></div>
      </div>
      <hr>
      <?php echo substr($value->JobDescription, 0,300)?>
            <br>
        <?php if(!isset($_SESSION['login_applicant']) == 'login_applicant'):?>
        <a href="login.php"  class="btn btn-primary"><i class="fa fa-file"></i> Apply</a>
      <?php else:?>
        <a href="applicant/"  class="btn btn-primary"><i class="fa fa-file"></i> Apply</a>
      <?php endif;?>
      <a href="login.php" class="btn btn-primary"><i class="fa fa-file"></i> Apply</a>
      <div class="row">
        <div class="col-12">
          <h4>
              <small class="float-right" style="font-size:15px;color:#999;">Date Created: <?php echo date("Y-m-d",strtotime($value->DateCreated))?>
              
              </small>
          </h4>
        </div>
        
      </div>
    </div>
        <!-- View Full Job-->
<div class="modal fade" id="view-job<?php echo $value->JobID?>">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title">Job Details</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                
              </div>
              <div class="modal-body">
              <?php 
              $viewjob = getSingleRow("*","JobID","jobposts",$value->JobID);
              $company = getSingleRow("*","UserID","companyclients",$value->ClientID);
              ?>
              <div class="row">
                <div class="col-md-4">Company Name:</div>
                <div class="col-md-8"><?php echo $value->CompanyName?></div>
                <div class="col-md-4">Job Title:</div>
                <div class="col-md-8"><?php echo $value->JobTitle?></div>
                <div class="col-md-4">Job Status:</div>
           
  <?php endforeach;?>
  <?php else:?>
    <div class="alert alert-danger">There are no records on the database. <a href="jobs.php">Return</a></div>
  <?php endif;?>
<?php elseif(isset($_GET['search_button']) AND isset($_POST['sub_id']) != 'all' AND isset($_POST['cat_id']) != 'all'):?>
  <?php $job = searchJob();?>
  <?php if(!empty($job)):?>
  <?php foreach ($job as $key => $value):?>
        <div class="callout callout-default">
      <div class="row">
        <div class="col-md-10">
          <h4>
        <a href="" style="color:black;" data-toggle="modal" data-target="#view-job<?php echo $value->JobID?>">
        <?php echo $value->JobTitle?> 
        <?php if($value->isUrgent == 'No'):?>
        <?php else:?>
          -  <small style="color:red;">Urgent!</small>
        <?php endif;?>
      </a>
      </h4>
        </div>
        <div class="col-md-2">
          
        </div>
      </div>
      
      <hr>
      <div class="row">
        <div class="col-md-4"><i class="fa fa-home"></i> Company:<?php echo $value->CompanyName?></div>
        <div class="col-md-4"><i class="fa fa-map"></i> Location: <?php echo $value->Location?></div>
        <div class="col-md-4"><i class="fa fa-file"></i> Available Slots: <?php echo $value->Slots?></div>
      </div>
      <hr>
      <?php echo substr($value->JobDescription, 0,300)?>
            <br>
        <?php if(!isset($_SESSION['login_applicant']) == 'login_applicant'):?>
        <a href="login.php"  class="btn btn-primary"><i class="fa fa-file"></i> Apply</a>
      <?php else:?>
        <a href="applicant/"  class="btn btn-primary"><i class="fa fa-file"></i> Apply</a>
      <?php endif;?>
      <a href="login.php" class="btn btn-primary"><i class="fa fa-file"></i> Apply</a>
      <div class="row">
        <div class="col-12">
          <h4>
              <small class="float-right" style="font-size:15px;color:#999;">Date Created: <?php echo date("Y-m-d",strtotime($value->DateCreated))?>
              
              </small>
          </h4>
        </div>
        
      </div>
    </div>
        <!-- View Full Job-->
<div class="modal fade" id="view-job<?php echo $value->JobID?>">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title">Job Details</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                
              </div>
              <div class="modal-body">
              <?php 
              $viewjob = getSingleRow("*","JobID","jobposts",$value->JobID);
              $company = getSingleRow("*","UserID","companyclients",$value->ClientID);
              ?>
              <div class="row">
                <div class="col-md-4">Company Name:</div>
                <div class="col-md-8"><?php echo $value->CompanyName?></div>
                <div class="col-md-4">Job Title:</div>
                <div class="col-md-8"><?php echo $value->JobTitle?></div>
                <div class="col-md-4">Job Status:</div>
           
  <?php endforeach;?>
  <?php else:?>
    <div class="alert alert-danger">There are no records on the database. <a href="jobs.php">Return</a></div>
  <?php endif;?>
<?php else:?>
<?php  if(!empty($list)):?>
  <table id="example2" class="table">
    <thead>
      <tr>
        <th>Job Description</th>
      </tr>
    </thead>
    <tbody>
  <?php foreach ($list as $key => $value):?>
    <tr>
      <td>
        <div class="callout callout-default">
      <div class="row">
        <div class="col-md-10">
          <h4>
        <a href="" style="color:black;" data-toggle="modal" data-target="#view-job<?php echo $value->JobID?>">
        <?php echo $value->JobTitle?> 
        <?php if($value->isUrgent == 'No'):?>
        <?php else:?>
          -  <small style="color:red;">Urgent!</small>
        <?php endif;?>
      </a>
      </h4>
        </div>
        <div class="col-md-2">
         
        </div>
      </div>
      
      <hr>
      <?php 
      $company = getSingleRow("*","UserID","companyclients",$value->ClientID);
      $location = getSingleRow("*","LocationID","locations",$value->LocationID);
      ?>
      <div class="row">
        <div class="col-md-4"><i class="fa fa-home"></i> Company:<?php echo $company['CompanyName']?></div>
        <div class="col-md-4"><i class="fa fa-map"></i> Location: <?php echo $location['Location']?></div>
        <div class="col-md-4"><i class="fa fa-file"></i> Available Slots: <?php echo $value->Slots?></div>
      </div>
      <hr>
      <?php echo substr($value->JobDescription, 0,300)?>
      <br>
      <?php if(!isset($_SESSION['login_applicant']) == 'login_applicant'):?>
        <a href="login.php"  class="btn btn-primary"><i class="fa fa-file"></i> Apply</a>
      <?php else:?>
        <a href="applicant/"  class="btn btn-primary"><i class="fa fa-file"></i> Apply</a>
      <?php endif;?>
      <div class="row">
        <div class="col-12">
          <h4>
              <small class="float-right" style="font-size:15px;color:#999;">Date Created: <?php echo date("Y-m-d",strtotime($value->DateCreated))?>
              
              </small>
          </h4>
        </div>
        
      </div>
    </div>
      </td>
</tr>
<!-- View Full Job-->
<div class="modal fade" id="view-job<?php echo $value->JobID?>">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title">Job Details</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                
              </div>
              <div class="modal-body">
              <?php 
              $viewjob = getSingleRow("*","JobID","jobposts",$value->JobID);
              $company = getSingleRow("*","UserID","companyclients",$value->ClientID);
              ?>
              <div class="row">
                <div class="col-md-4">Company Name:</div>
                <div class="col-md-8"><?php echo $company['CompanyName']?></div>
                <div class="col-md-4">Job Title:</div>
                <div class="col-md-8"><?php echo $value->JobTitle?></div>
                <div class="col-md-4">Job Status:</div>
                <div class="col-md-8"><?php echo $value->JobStatus?></div>
                <div class="col-md-4">Available Slots:</div>
                <div class="col-md-8"><?php echo $value->Slots?></div>
                <div class="col-md-4">Expected Salary:</div>
                <div class="col-md-8"><?php echo $value->ExpectedSalary?></div>
              </div>
              <hr>
              <strong>Job Description:</strong>
              <?php echo $value->JobDescription?>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
</div>
<!-- end -->
<?php endforeach;?>
</table>
<?php else:?>
<?php endif;?> 
<?php endif;?>
</main>

</div>
<?php include'dist/assets/footer.php';?>
<script type="text/javascript">
function changeCat(key){
  if(window.XMLHttpRequest){
  // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp = new XMLHttpRequest();
  }else{
  // code for IE6, IE5
    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange = function(){
    if(xmlhttp.readyState == 4 && xmlhttp.status == 200){
      document.getElementById("compList").innerHTML = xmlhttp.responseText;
      var comp = document.getElementById("compList").value;
      me(comp);
    }
  }
  xmlhttp.open("GET","admin/changeCat.php?key="+key, true);
  xmlhttp.send(); 
}
</script>
