<?php 
include'config/db.php';
include'config/functions.php';
include'config/myfunction.php';

if(isset($_POST['save_button'])){
  $FirstName = filter($_POST['FirstName']);
  $LastName = filter($_POST['LastName']);
  $email_address = filter($_POST['email_address']);
  $CompanyName = filter($_POST['CompanyName']);
  $user_pass = filter($_POST['user_pass']);

  $checkemail = getSingleRow("EmailAddress","EmailAddress","accounts",$email_address);
  $checkCompany = getSingleRow("CompanyName","CompanyName","companyclients",$CompanyName);

  if(checkname($FirstName,$LastName)){
    $msg = 'Name: '.$FirstName.' '.$LastName.' already exist. ';
  }
  
  elseif($checkemail['EmailAddress'] == $email_address){
    $msg = 'Email Address: '.$email_address.' already exist';
  }elseif($checkCompany['CompanyName'] == $CompanyName){
    $msg = 'Company Name: '.$CompanyName.' already exist';
  }
  
  else{
    $accountArray = array("FirstName"=>$FirstName, "LastName"=>$LastName,"EmailAddress"=>$email_address,"Password"=>md5($user_pass),"UserType"=>"2");
    SaveData("accounts",$accountArray);

    $last_id = mysqli_insert_id($dbcon);

    $employerArray = array("UserID"=>$last_id, "CompanyEmail"=>$email_address,"CompanyName"=>$CompanyName);
    SaveData("companyclients",$employerArray);
    $success = 'You have successfully registered to website.';
  
  }
}
?>
<?php include'dist/assets/header.php';?>
    <main role="main" style="">
      <div class="container marketing" style="margin-top:10%;">

        <!-- Three columns of text below the carousel -->
        
        <div >
          <div class="col-md-12" >
            <center><h1><i class="fa fa-pencil"></i> Company Registration</h1>
            <hr>
            <div class="container">
            <?php if(!empty($msg)):?><div class="alert alert-danger"><?php echo $msg;?></div><?php endif;?>
            <?php if(!empty($success)):?>
              <div class="alert alert-success"><?php echo $success;?></div>
              <META HTTP-EQUIV="refresh" CONTENT="1; URL=login.php">
            <?php endif;?>
            <form method="post">
              <div class="col-md-6 mx-sm-3">
                <input type="text" name="CompanyName" class="form-control" placeholder="Company Name" required="required" value="<?php if(isset($_POST['save_button'])): echo $_POST['CompanyName']; endif;?>">
              </div>
              <p></p>
              <div class="col-md-6 mx-sm-3">
                <input type="text" name="FirstName" class="form-control" placeholder="First Name" required="required" value="<?php if(isset($_POST['save_button'])): echo $_POST['FirstName']; endif;?>">
              </div>
              <p></p>
              <div class="col-md-6 mx-sm-3">
                <input type="text" name="LastName" class="form-control" placeholder="Last Name" required="required" value="<?php if(isset($_POST['save_button'])): echo $_POST['LastName']; endif;?>">
              </div>
              <p></p>
              <div class="col-md-6 mx-sm-3">
                <input type="text" name="email_address" class="form-control" placeholder="Email Address" required="required" value="<?php if(isset($_POST['save_button'])): echo $_POST['email_address']; endif;?>">
              </div>
              <p></p>
              <div class="col-md-6 mx-sm-3">
                <input type="password" name="user_pass" class="form-control" placeholder="Password" required="required">
              </div>
              <p></p>
               <div class="col-md-6">
                <button class="btn btn-danger" name="save_button"><i class="fa fa-save"></i> Signup</button>
              </div>
              </form>
              <br>
              <div class="col-md-6">
                Already a member?<a href="login.php"> Login now</a>
              </div>
              <p></p>
            </div>
            </center>
        </div>
</main>

</div>

<?php include'dist/assets/footer.php';?>