<?php 
include'../config/db.php';
include'../config/functions.php';
include'../config/myfunction.php';


$applicant = initialInterview();
?>
<?php include'../dist/assets/dashboard_header.php';?>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
<?php include'../dist/assets/dashboard_nav.php';?>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="col-md-12">

            <div class="card">

              <div class="card-header">
                <h3 class="card-title"><i class="fa fa-calendar"></i> Generate Reports</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <form method="get" target="_blank" action="print-report.php">
                  <div class="row">
                    <div class="col-md-5">
                      <input type="date" name="from" class="form-control">
                    </div>
                    <div class="col-md-5">
                      <input type="date" name="until" class="form-control">
                    </div>
                    <div class="col-md-2">
                      <button class="btn btn-primary" name="search_button"><i class="fa fa-search"></i> Generate Reports</button>
                    </div>
                  </div>
                  
                </form>
              </div>
                 
              
              </div>
            </div>

    </div>
  <!-- /.content-wrapper -->
<?php include'../dist/assets/dashboard_footer.php';?>