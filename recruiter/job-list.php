<?php 
include'../config/db.php';
include'../config/functions.php';
include'../config/myfunction.php';

if(empty($_SESSION['login_recruiter'])){ 
//This function is to check weather the account has been login or not
  header("Location: ../index.php");
  exit;
}
$job_list = fetchWhere("*","JobStatus","jobposts","Available");

?>
<?php include'../dist/assets/dashboard_header.php';?>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
<?php include'../dist/assets/dashboard_nav.php';?>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title"><i class="fa fa-briefcase"></i> Job Lists</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <?php  if(!empty($job_list)):?>
                <table id="example1" class="table table-bordered table-striped" style="font-size:12px;">
                <thead>
                <tr>
                  <th>Company Name</th>
                  <th>Job Title</th>
                  <th>Applicant</th>
                  <th>Available Slots</th>
                  <th>Expected Salary</th>
                  <th>Option</th>
                </tr>
                </thead>
                <tbody>
              <?php foreach ($job_list as $key => $value):?>
                <tr>
                  <td>
                    <?php 
                    $name = getSingleRow("*","UserID","companyclients",$value->ClientID);
                    echo $name['CompanyName'];
                    ?>
                  </td>
                  <td><?php echo $value->JobTitle?></td>
                  <td>
                  <?php 
                  //$total = getSingleRow("COUNT(*) as total", "jobID","jobapplication", $value->job_id);
                  $job_id = $value->JobID; 
                  $total = countApplicant($job_id);
                  ?>
                    <a href="view-applicant.php?jobID=<?php echo $value->JobID;?>">View Applicant (<?php echo $total['total'];?>)</a>
                  </td>
                  <td><?php echo $value->Slots?></td>
                  <td><?php echo $value->ExpectedSalary?></td>
                  <td><a href="" data-toggle="modal" data-target="#view-job<?php echo $value->JobID?>">View Job Details</a></td>
                </tr>
                <!-- View Details-->
<div class="modal fade" id="view-job<?php echo $value->JobID?>">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">Job Details</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                
              </div>
              <div class="modal-body">
                <?php $list = fetchWhere("*","JobID","jobposts",$value->JobID);?>
                <?php if(!empty($list)):?>
                  <?php foreach ($list as $key => $result):?>
                    <h5>Job Title: <?php echo $result->JobTitle?> <?php if($result->isUrgent == 'No'):?>
                  <?php else:?>
                  - <div class="btn btn-danger btn-sm">Urgent!</div>
                  <?php endif;?></h5>
                  <?php 
                    $company = getSingleRow("*","UserID","companyclients",$result->ClientID);
                    $location = getSingleRow("*","LocationID","locations",$result->LocationID);
                  ?>
                  <hr>
                    <div class="row">
                      <div class="col-md-4">
                        Company:
                      </div>
                      <div class="col-md-8">
                        <?php echo $company['CompanyName']?>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-4">
                        Location:
                      </div>
                      <div class="col-md-8">
                       <?php echo $location['Location']?>
                      </div>
                    </div>
                     <div class="row">
                      <div class="col-md-4">
                        Expected Salary:
                      </div>
                      <div class="col-md-8">
                       <?php echo $result->ExpectedSalary?>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-4">
                        Expiration Date:
                      </div>
                      <div class="col-md-8">
                       <?php echo $result->ExpirationDate?>
                      </div>
                    </div>
                     <div class="row">
                      <div class="col-md-4">
                        Available Slots:
                      </div>
                      <div class="col-md-8">
                       <?php echo $result->Slots?>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-4">
                        Job Description:
                      </div>
                      <div class="col-md-8">
                       
                      </div>
                    </div>
                    <?php echo $result->JobDescription?>
                  <?php endforeach;?>
                <?php else:?>
                  <div class="alert alert-danger">There are no records on the database.</div>
                <?php endif;?>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
</div>
<!-- end -->
              <?php endforeach;?>
              </table>
              <?php else:?>
                <div class="alert alert-danger">There are no records on the database.</div>
              <?php endif;?>
              
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
    </div>

  </div>
  <!-- /.content-wrapper -->
<?php include'../dist/assets/dashboard_footer.php';?>