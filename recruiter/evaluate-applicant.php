<?php 
include'../config/db.php';
include'../config/functions.php';
include'../config/myfunction.php';

if(empty($_SESSION['login_recruiter'])){ 
//This function is to check weather the account has been login or not
  header("Location: ../index.php");
  exit;
}

$view = viewCandidate();
if(!empty($view)){
  foreach ($view as $key => $row) {
    $JobTitle = $row->JobTitle;
    $JobStatus = $row->JobStatus;
    $CompanyName = $row->CompanyName;
    $FirstName = $row->FirstName;
    $LastName = $row->LastName;
    $expected_salary = $row->ExpectedSalary;
    $location = $row->Location;
    $applicant_photo = $row->ApplicantPhoto;
    $ContactNumber = $row->ContactNumber;
    $email_address = $row->ApplicantEmail;
    $user_address = $row->Address;
    $birthdate = $row->BirthDate;
    $UserID = $row->UserID;    
  }
}
if(isset($_POST['save_button'])){
  $MedicalExam = filter($_POST['MedicalExam']);
  $VisaApplicationForm = filter($_POST['VisaApplicationForm']);
  $VisaStamping = filter($_POST['VisaStamping']);
  $POEA = filter($_POST['POEA']);
  $PDOS = filter($_POST['PDOS']);

  $dbcheck = $dbcon->query("SELECT * FROM requirements WHERE JobID = '".$_GET['jobID']."' AND ApplicantID = '".$_GET['application_id']."'") or die(mysqli_error());

  if(mysqli_num_rows($dbcheck) > 0){
    $arr_where = array("JobID" => filter($_GET['jobID']), "ApplicantID" => $_GET['application_id']);//update where
    $arr_set = array("MedicalExam" =>$MedicalExam, "VisaApplicationForm" =>$VisaApplicationForm, "VisaStamping" =>$VisaStamping, "POEA" =>$POEA, "PDOS" => $PDOS, "JobID" => filter($_GET['jobID']), "ApplicantID" => $_GET['application_id']);
    $tbl_name = "requirements";
    $update = UpdateQuery($dbcon,$tbl_name,$arr_set,$arr_where);// UPDATE SQL
    header("location: predeployment.php");
  }
  else{
    $insertSQL = array("MedicalExam" =>$MedicalExam, "VisaApplicationForm" =>$VisaApplicationForm, "VisaStamping" =>$VisaStamping, "POEA" =>$POEA, "PDOS" => $PDOS, "JobID" => filter($_GET['jobID']), "ApplicantID" => $_GET['application_id']);
    SaveData("requirements",$insertSQL);
    header("location: predeployment.php");
  }
  }
  
$sql = $dbcon->query("SELECT * FROM requirements WHERE JobID = '".$_GET['jobID']."' AND ApplicantID = '".$_GET['application_id']."'") or die(mysqli_error());
$check = mysqli_num_rows($sql);

$checkrequirements = viewRequirements();
  if(!empty($checkrequirements)){
    foreach ($checkrequirements as $key => $row) {
      $JobID  = $row->JobID;
      $ApplicantID = $row->ApplicantID;
      $VisaApplicationForm = $row->VisaApplicationForm;
      $VisaStamping = $row->VisaStamping;
      $POEA = $row->POEA;
      $PDOS = $row->PDOS;
      $med = $row->MedicalExam;
    }
  }
?>
<?php include'../dist/assets/dashboard_header.php';?>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
<?php include'../dist/assets/dashboard_nav.php';?>
     
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
    
            <div class="card">

              <div class="card-header">
                <h3 class="card-title"><i class="fa fa-user"></i> Applicant Information</h3>
                <hr>
                                <div class="row">
                <div class="col-md-2">
                  <img class="profile-user-img img-fluid"
                       src="../img/<?php echo $applicant_photo; ?>"
                       alt="User profile picture">
                </div>
                 <div class="col-md-7">
                  <h3 class="profile-username">
                  <?php echo $FirstName;?> <?php echo $LastName;?> 
                </h3>
                <p class="text-muted">
                <i class="fa fa-calendar"></i> <?php echo $birthdate?> /  <i class="fa fa-mobile"></i> <?php echo $ContactNumber;?> / <i class="fa fa-envelope"></i> <?php echo $email_address;?> / <i class="fa fa-home"></i> <?php echo $user_address?></p>
                <center>

                </div>
              </div>

              
              </div>
              <!-- /.card-body -->
            </div>
            <div class="card">
              <div class="card-header">
                <h3 class="card-title"><i class="fa fa-briefcase"></i> Job Details</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div class="row">
                  <div class="col-md-2">
                    Job Title:
                  </div>
                  <div class="col-md-4">
                    <div class="text-muted"><?php echo $JobTitle;?></div>
                  </div>
                  <div class="col-md-2">
                    Job Status:
                  </div>
                  <div class="col-md-4">
                    <div class="text-muted"><?php echo $JobStatus;?></div>
                  </div>
                </div><p></p>
                <div class="row">
                  <div class="col-md-2">
                    Company Name:
                  </div>
                  <div class="col-md-4">
                    <div class="text-muted"><?php echo $CompanyName;?></div>
                  </div>
                  <div class="col-md-2">
                    Location:
                  </div>
                  <div class="col-md-4">
                    <div class="text-muted"><?php echo $location;?></div>
                  </div>
                </div><p></p>
                <div class="row">
                  <div class="col-md-2">
                    Expected Salary:
                  </div>
                  <div class="col-md-4">
                    <div class="text-muted"><?php echo $expected_salary;?></div>
                  </div>
                  <div class="col-md-2">
                    
                  </div>
                  <div class="col-md-4">
                    
                  </div>
                </div>
                
                </div>
              </div>
              <div class="card">
              
              <div class="card-header">
                <h3 class="card-title"><i class="fa fa-file"></i> Requirements</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <form method="post">
               <div class="row">
                <div class="col-md-4">
                  <input type="checkbox" value="1" name="MedicalExam" <?php if($check == 0):?><?php else: if($med == '1'):?>
checked="checked"
<?php endif;?> <?php endif;?>> Medical Exam 
                </div>
                
               </div>
               <div class="row">
                <div class="col-md-4">
                  <input type="checkbox" value="1"  name="VisaApplicationForm" <?php if($check == 0):?><?php else: if($VisaApplicationForm == '1'):?>
checked="checked"
<?php endif;?> <?php endif;?>> Visa Application
                </div>
                
               </div>
               <div class="row">
                <div class="col-md-4">
                  <input type="checkbox" value="1" name="VisaStamping" <?php if($check == 0):?><?php else: if($VisaStamping == '1'):?>
checked="checked"
<?php endif;?> <?php endif;?>>  Visa Stamping
                </div>
               
               </div>
               <div class="row">
                <div class="col-md-4">
                  <input type="checkbox" value="1" name="POEA" <?php if($check == 0):?><?php else: if($POEA == '1'):?>
checked="checked"
<?php endif;?> <?php endif;?>>  POEA
                  
                </div>
                
               </div>
                <div class="row">
                <div class="col-md-4">
                  <input type="checkbox" value="1" name="PDOS" <?php if($check == 0):?><?php else: if($PDOS == '1'):?>
checked="checked"
<?php endif;?> <?php endif;?>> PDOS
                </div>
                
               </div>
               <button class="btn btn-primary btn-sm" name="save_button"><i class="fa fa-save"></i> Save Requirement</button>
               </form>
                </div>
                
                </div>
              </div>
          
              
                
                </div>
              </div>
              </div>
              <!-- /.card-header -->
             
              
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
    </div>

  </div>
  <!-- /.content-wrapper -->
<?php include'../dist/assets/dashboard_footer.php';?>