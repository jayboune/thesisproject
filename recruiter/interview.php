<?php 
include'../config/db.php';
include'../config/functions.php';
include'../config/myfunction.php';

if(empty($_SESSION['login_recruiter'])){ 
//This function is to check weather the account has been login or not
  header("Location: ../index.php");
  exit;
}
$view = viewUserInterview();
$result = fetchWhere("*","ApplicantID","jobapplications",$_GET['application_id']);
$account = getSingleRow("*","UserID","accounts",$_SESSION['UserID']);

if(!empty($view)){
  foreach ($view as $key => $row) {
    $JobTitle = $row->JobTitle;
    $JobStatus = $row->JobStatus;
    $CompanyName = $row->CompanyName;
    $FirstName = $row->FirstName;
    $LastName = $row->LastName;
    $expected_salary = $row->ExpectedSalary;
    $location = $row->Location;
    $applicant_photo = $row->ApplicantPhoto;
    $ContactNumber = $row->ContactNumber;
    $email_address = $row->ApplicantEmail;
    $user_address = $row->Address;
    $birthdate = $row->BirthDate;    
  }
}else{
  header("location: error.php");
}
if(!empty($result)){
  foreach ($result as $key => $value) {
    $applicant_status = $row->ApplicationStatus;
    $interview_schedule = $row->InterviewSchedule;
    $interviewer = $row->Interviewer;
    $remarks = $row->Remarks;
  }
}else{
  header("location: error.php");
}
if(isset($_POST['save_button'])){
  $applicant_status = $_POST['applicant_status'];
  $interview_schedule = $_POST['interview_schedule'];
  $interviewer = $_POST['interviewer'];
  $remarks = $_POST['remarks'];
  $application_id = $_GET['application_id'];

  if($applicant_status == 'Failed'){
    $job = getSingleRow("*","JobID","jobpost",$_GET['jobID']);
    $total = $job['Slots'] + 1;
    $arr_where = array("JobID"=>filter($_GET['jobID']));//update where
    $arr_set = array("Slots" => $total);
    $tbl_name = "jobposts";
    $update = UpdateQuery($dbcon,$tbl_name,$arr_set,$arr_where);// UPDATE SQL
    header("location: index.php");
  }else{
  $arr_where = array("ApplicantID"=>filter($_GET['application_id']));//update where
  $arr_set = array("ApplicationStatus" => $applicant_status, "InterviewSchedule" => $interview_schedule, "Interviewer" => $interviewer, "Remarks" => $remarks);
  $tbl_name = "jobapplications";
  $update = UpdateQuery($dbcon,$tbl_name,$arr_set,$arr_where);// UPDATE SQL
  header("location: index.php");
  }

  
}
?>
<?php include'../dist/assets/dashboard_header.php';?>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
<?php include'../dist/assets/dashboard_nav.php';?>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title"><i class="fa fa-user"></i> View Profile </h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div class="row">
                <div class="col-md-2">
                  <img class="profile-user-img img-fluid"
                       src="../img/<?php echo $applicant_photo; ?>"
                       alt="User profile picture">
                </div>
                <div class="col-md-7">
                  <h3 class="profile-username">
                  <?php echo $FirstName;?> <?php echo $LastName;?>
                </h3>
                <p class="text-muted">
                <i class="fa fa-calendar"></i> <?php echo $birthdate?> /  <i class="fa fa-mobile"></i> <?php echo $ContactNumber;?> / <i class="fa fa-envelope"></i> <?php echo $email_address;?> / <i class="fa fa-home"></i> <?php echo $user_address?></p>
                <center>

                </div>
              </div>

              
              </div>
              <!-- /.card-body -->
            </div>
            <div class="card">
              <div class="card-header">
                <h3 class="card-title"><i class="fa fa-briefcase"></i> Job Details</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div class="row">
                  <div class="col-md-2">
                    Job Title:
                  </div>
                  <div class="col-md-4">
                    <div class="text-muted"><?php echo $JobTitle;?></div>
                  </div>
                  <div class="col-md-2">
                    Job Status:
                  </div>
                  <div class="col-md-4">
                    <div class="text-muted"><?php echo $JobStatus;?></div>
                  </div>
                </div><p></p>
                <div class="row">
                  <div class="col-md-2">
                    Company Name:
                  </div>
                  <div class="col-md-4">
                    <div class="text-muted"><?php echo $CompanyName;?></div>
                  </div>
                  <div class="col-md-2">
                    Location:
                  </div>
                  <div class="col-md-4">
                    <div class="text-muted"><?php echo $location;?></div>
                  </div>
                </div><p></p>
                <div class="row">
                  <div class="col-md-2">
                    Expected Salary:
                  </div>
                  <div class="col-md-4">
                    <div class="text-muted"><?php echo $expected_salary;?></div>
                  </div>
                  <div class="col-md-2">
                    
                  </div>
                  <div class="col-md-4">
                    
                  </div>
                </div>
                
                </div>
              </div>
              <div class="card">
              <div class="card-header">
                <h3 class="card-title"><i class="fa fa-th"></i> Change Status</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
              <form method="post">
                <div class="row">
                  <div class="col-md-2">Applicant Status:</div>
                  <div class="col-md-4">
                    <select class="form-control" name="applicant_status">
                      <option value="Waiting for Response" <?php if(isset($_GET['application_id'])){
                      if($applicant_status == "Waiting for Response"){echo 'selected';}}
                        elseif(isset($_POST['save_button'])){echo $_POST['applicant_status'];}?>>Waiting for Response</option>
                      <option value="Failed" <?php if(isset($_GET['application_id'])){
                      if($applicant_status == "Failed"){echo 'selected';}}
                        elseif(isset($_POST['save_button'])){echo $_POST['applicant_status'];}?>>Failed</option>
                      <option value="For Initial Interview" <?php if(isset($_GET['application_id'])){
                      if($applicant_status == "For Initial Interview"){echo 'selected';}}
                        elseif(isset($_POST['save_button'])){echo $_POST['applicant_status'];}?>>For Initial Interview</option>
                      <option value="For Evaluation" <?php if(isset($_GET['application_id'])){
                      if($applicant_status == "For Evaluation"){echo 'selected';}}
                        elseif(isset($_POST['save_button'])){echo $_POST['applicant_status'];}?>>For Evaluation</option>
                    </select>
                  </div>
                  <br>
                  <div class="col-md-2">Interview Date:</div>
                  <div class="col-md-4">
                    <input type="date" name="interview_schedule" class="form-control" value="<?php echo $interview_schedule?>">
                  </div>
                </div><p></p>
                <div class="row">
                  <div class="col-md-2">Interviewer:</div>
                  <div class="col-md-4">
                    <input type="text" name="interviewer" class="form-control" value="<?php echo $account['FirstName']?> <?php echo $account['LastName']?>" readonly>
                  </div>
                  <div class="col-md-2">Remarks:</div>
                  <div class="col-md-4">
                    <textarea class="form-control" name="remarks"><?php echo $remarks?></textarea>
                  </div>
                </div>
                <center>
                  <button class="btn btn-primary btn-sm" name="save_button">
                    <i class="fa fa-save"></i> Save
                  </button>
                  <a href="view-applicant.php?jobID=<?php echo $_GET['jobID']?>" class="btn btn-danger btn-sm"><i class="fa fa-arrow-left"></i> Return</a>
                </center>
              </form>  
              </div>
                
                </div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
    </div>

  </div>
  <!-- /.content-wrapper -->
<?php include'../dist/assets/dashboard_footer.php';?>