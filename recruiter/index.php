<?php 
include'../config/db.php';
include'../config/functions.php';
include'../config/myfunction.php';

if(empty($_SESSION['login_recruiter'])){ 
//This function is to check weather the account has been login or not
  header("Location: ../index.php");
  exit;
}
$applicant = initialInterview();
?>
<?php include'../dist/assets/dashboard_header.php';?>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
<?php include'../dist/assets/dashboard_nav.php';?>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="col-md-12">
        <div class="row">
          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box">
              <span class="info-box-icon bg-info elevation-1"><i class="fa fa-users"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Initial Interview</span>
                <span class="info-box-number">
                  10
                  
                </span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
              <span class="info-box-icon bg-danger elevation-1"><i class="fa fa-home"></i></span>

              <div class="info-box-content">
                <span class="info-box-text"> Pre-Depolyed Applicant </span>
                <span class="info-box-number">41,410</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->

          <!-- fix for small devices only -->
          <div class="clearfix hidden-md-up"></div>

          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
              <span class="info-box-icon bg-success elevation-1"><i class="fa fa-user"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Hired Applicants</span>
                <span class="info-box-number">760</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
              <span class="info-box-icon bg-warning elevation-1"><i class="fa fa-warning"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Failed Interview</span>
                <span class="info-box-number">2,000</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
        </div>
            <div class="card">

              <div class="card-header">
                <h3 class="card-title"><i class="fa fa-briefcase"></i> Applicants for Initial Interview</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <?php  if(!empty($applicant)):?>
                <table id="example1" class="table table-bordered table-striped" style="font-size:12px;">
                <thead>
                <tr>
                  <th>Applicant</th>
                  <th>Company Name</th>
                  <th>Position Applying for</th>
                  <th>Interview Schedule</th>
                  <th>Interviewer</th>
                  <th>Status</th>
                  <th>Option</th>
                </tr>
                </thead>
                <tbody>
                
              <?php foreach ($applicant as $key => $value):?>
                <tr>
                  <td><?php echo $value->FirstName?> <?php echo $value->LastName?></td>
                  <td><?php echo $value->CompanyName?></td>
                  <td>
                  <?php echo $value->JobTitle?>
                  </td>
                  <td><?php echo $value->ApplicationStatus?></td>
                  <td><?php echo $value->InterviewSchedule?></td>
                  <td><?php echo $value->Interviewer?> ID: <?php echo $value->ID?> CID: <?php echo $value->ClientID?> Job: <?php echo $value->JobID?></td>
                  <td>
                    <div class="btn-group">
                    <button type="button" class="btn btn-info">View</button>
                    <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                      <span class="caret"></span>
                      <span class="sr-only">Toggle Dropdown</span>
                    </button>
<ul class="dropdown-menu" role="menu">
  <li style="margin:5px;"><a href="" data-toggle="modal" data-target="#view-profile<?php echo $value->ID?>">View Profile</a></li>
  <li style="margin:5px;"><a href="interview.php?application_id=<?php echo $value->ID?>&company_id=<?php echo $value->ClientID?>&jobID=<?php echo $value->JobID?>">Option</a></li>
  <!-- option.php?application_id=<?php echo $value->ApplicationID?>&jobID=<?php echo $_GET['jobID']?>&company_id=<?php echo $value->ClientID?> -->
</ul>
        </div>
                  </td>
                </tr>
                 <!-- View Details-->
<div class="modal fade" id="view-profile<?php echo $value->ID?>">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">Applicant Details</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                
              </div>
              <div class="modal-body">
                <?php
                $user = getSingleRow("*","UserID","applicants",filter($value->ID));
                $accounts = getSingleRow("*","UserID","accounts",filter($value->ID));
                //Getting the Applicant personal information
                $education_list = fetchWhere("*","UserID","educations",$value->ID);
                $skill_list = fetchWhere("*","UserID","skills",$value->ID);
                $language_list = fetchWhere("*","UserID","languages",$value->ID);
                $info_list = fetchWhere("*","UserID","additional_infos",$value->ID);
                $experience_list = fetchWhere("*","UserID","experiences",$value->ID);
                ?>
                <div class="row">

                <div class="col-md-12">
                  <center>
                  <img class="profile-user-img img-fluid"
                       src="../img/<?php echo $user['ApplicantPhoto']?>"
                       alt="User profile picture">
                  <h3 class="profile-username">
                  <?php echo $accounts['FirstName']?> <?php echo $accounts['LastName']?>
                </h3>
                <p class="text-muted">
                <i class="fa fa-calendar"></i> <?php echo $user['BirthDate']?> /  <i class="fa fa-mobile"></i> <?php echo $user['ContactNumber']?> / <i class="fa fa-envelope"></i> <?php echo $user['ApplicantEmail']?> / <i class="fa fa-home"></i> <?php echo $user['Address']?></p>
                <center>
                </center>
                </div>
              </div>
              <hr>                  
              <h5><i class="fa fa-graduation-cap"></i> Educational Background</h5><hr>
              <?php if(!empty($education_list)):?>
                <?php foreach ($education_list as $key => $value):?>
                  <div class="callout callout-success">
                  <h5 class="text-muted"><strong><?php echo $value->University?> - <?php echo $value->GradDate?></strong></h5>
                  <hr>

                  <p class="text-muted"><?php echo $value->Qualification?> - <?php echo $value->Course?></p>
                  <strong class="text-muted">Additional Information:</strong>
                  <p class="text-muted"><?php echo $value->AddInfo?></p>
                  
                </div>
                <?php endforeach;?>
                <?php else:?>
                  <div class="alert alert-danger">There are no records on the database.</div>
                <?php endif;?>
            <h5><i class="fa fa-briefcase"></i> Work Experience</h5><hr>
              <?php if(!empty($experience_list)):?>
                <?php foreach ($experience_list as $key => $value):?>
                  <div class="callout callout-success">
                  <h5 class="text-muted"><?php echo $value->EmployerName?> - <?php echo $value->PositionTitle?></h5>
                  <hr>

                  <p class="text-muted">From: <?php echo $value->Date_From?> - Until: <?php echo $value->Date_Until?></p>
                  <strong class="text-muted">Position Level: </strong> <?php echo $value->Position?><br>
                  <strong class="text-muted">Salary: </strong> <?php echo $value->Salary?><br>
                  <strong class="text-muted">Additional Information:</strong>
                  <p class="text-muted"><?php echo $value->OtherDesc?></p>
                  
                </div>
                <?php endforeach;?>
                <?php else:?>
                  <div class="alert alert-danger">There are no records on the database.</div>
                <?php endif;?>
              <h5><i class="fa fa-book"></i> Skills</h5><hr>
              <?php if(!empty($skill_list)):?>
                <?php foreach ($skill_list as $key => $value):?>

                  <div class="callout callout-success">
                  <div class="row">
                    <div class="col-md-9 text-muted"><strong>Skill:</strong> <?php echo $value->Skill?></div>
                    <div class="col-md-3 text-muted"><strong>Profeciency:</strong> <?php echo $value->Proficiency?></div>
                  </div>
                </div>
                <?php endforeach;?>
                <?php else:?>
                  <div class="alert alert-danger">There are no records on the database.</div>
                <?php endif;?>
              <h5><i class="fa fa-comment"></i> Language</h5><hr>
              <?php if(!empty($language_list)):?>
                <?php foreach ($language_list as $key => $value):?>

                <div class="callout callout-success">
                  <div class="row">
                    <div class="col-md-6 text-muted"><strong>Language:</strong> <?php echo $value->Language?></div>
                    <div class="col-md-3 text-muted"><strong>Spoken:</strong> <?php echo $value->Spoken?></div>
                    <div class="col-md-3 text-muted"><strong>Written:</strong> <?php echo $value->Written?></div>
                  </div>
                </div>

                <?php endforeach;?>
                <?php else:?>
                  <div class="alert alert-danger">There are no records on the database.</div>
                <?php endif;?>
              <h5><i class="fa fa-plus"></i> Additional Information</h5><hr>
              <?php if(!empty($info_list)):?>
                <?php foreach ($info_list as $key => $value):?>
                  <div class="callout callout-success">
                  
                  <strong class="text-muted">Expected Salary:</strong> <?php echo $value->ExpectedSalary?><br>
                  <strong class="text-muted">Preferred Work Location:</strong> <?php echo $value->WorkLocation?><br>
                  <strong class="text-muted">Other Information</strong><br>
                  <p><?php echo $value->OtherInfo?></p>
                 
                </div>
                <?php endforeach;?>
                <?php else:?>
                  <div class="alert alert-danger">There are no records on the database.</div>
                <?php endif;?>
                  
                 
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
</div>
<!-- end -->
              <?php endforeach;?>
              </table>
              <?php else:?>
                <div class="alert alert-danger">There are no records on the database.</div>
              <?php endif;?>
              
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
    </div>

  </div>
  <!-- /.content-wrapper -->
<?php include'../dist/assets/dashboard_footer.php';?>