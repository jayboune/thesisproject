<?php 
include'config/db.php';
include'config/functions.php';
include'config/myfunction.php';

if(isset($_SESSION['login_admin']) == 'login_admin')
{
    header("location: admin/");
}

if(isset($_SESSION['login_applicant']) == 'login_applicant')
{
    header("location: applicant/");
}

if(isset($_SESSION['login_company']) == 'login_company')
{
    header("location: company/");
}
if(isset($_SESSION['login_recruiter']) == 'login_recruiter')
{
    header("location: recruiter/");
}

if(isset($_POST['login_button'])){
  
  $email_address = filter($_POST['email_address']);
  $user_pass = md5($_POST['user_pass']);

  $loginAccount = login();
  if(!empty($loginAccount)){
    foreach ($loginAccount as $key => $value) {
      if($value->UserType == '0'){
          $_SESSION['UserID'] = $value->UserID;
          $_SESSION['EmailAddress'] = $value->EmailAddress; 
          $_SESSION['FirstName'] = $value->FirstName;
          $_SESSION['LastName'] = $value->LastName;
          $_SESSION['UserType'] = $value->UserType;
          $_SESSION['login_admin'] = 'login_admin';

          header("location: admin/");
      }
      elseif($value->UserType == '1'){
          $_SESSION['UserID'] = $value->UserID;
          $_SESSION['EmailAddress'] = $value->EmailAddress; 
          $_SESSION['FirstName'] = $value->FirstName;
          $_SESSION['LastName'] = $value->LastName;
          $_SESSION['UserType'] = $value->UserType;
         $_SESSION['login_applicant'] = 'login_applicant';

          header("location: applicant/");
      }
      elseif($value->UserType == '2'){
          $_SESSION['UserID'] = $value->UserID;
          $_SESSION['EmailAddress'] = $value->EmailAddress; 
          $_SESSION['FirstName'] = $value->FirstName;
          $_SESSION['LastName'] = $value->LastName;
          $_SESSION['UserType'] = $value->UserType;
          $_SESSION['login_company'] = 'login_company';

          header("location: company/");
      }
      elseif($value->UserType == '3'){
          $_SESSION['UserID'] = $value->UserID;
          $_SESSION['EmailAddress'] = $value->EmailAddress; 
          $_SESSION['FirstName'] = $value->FirstName;
          $_SESSION['LastName'] = $value->LastName;
          $_SESSION['UserType'] = $value->UserType;
          $_SESSION['login_recruiter'] = 'login_recruiter';

        header("location: recruiter/");
      }
    }
  }else{
    $msg = 'Wrong username/password or Your account is not yet verfied';
  }
 
}

 
?>
<?php include'dist/assets/header.php';?>
    <main role="main" style="">
      <div class="container marketing" style="margin-top:10%;">

        <!-- Three columns of text below the carousel -->
        
        <div >
          <div class="col-md-12" >
            <center><h1><i class="fa fa-lock"></i> Login Account</h1>
            <hr>
            <a href="" class="btn btn-primary"><i class="fa fa-facebook"></i> Login with Facebook</a>
            <br><br>
            <div class="container">
            <?php if(isset($msg)):?><div class="alert alert-danger"><?php echo $msg;?></div><?php endif;?>
           
            <form method="post">
              <div class="col-md-6 mx-sm-3">
                <input type="email" name="email_address" class="form-control" placeholder="Email Address" required="required" value="<?php if(isset($_POST['login_button'])): echo $_POST['email_address']; endif;?>">
              </div>
              <p></p>
              <div class="col-md-6 mx-sm-3">
                <input type="password" name="user_pass" class="form-control" placeholder="Password" required="required">
              </div>
              <p></p>

               <div class="col-md-6">
                <button class="btn btn-danger" name="login_button"><i class="fa fa-lock"></i> Login</button> <a href="">Forgot Password</a>
              </div>
              </form>
              <div class="col-md-6">
                New User? <a href="signup.php">Signup for free</a>
              </div>
              <p></p>
            </div>
            </center>
        </div>
</main>
      </div>

<?php include'dist/assets/footer.php';?>