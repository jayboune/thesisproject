<?php 
include'config/db.php';
include'config/functions.php';
include'config/myfunction.php';

if(isset($_SESSION['login_admin']) == 'login_admin')
{
    header("location: admin/");
}

if(isset($_SESSION['login_applicant']) == 'login_applicant')
{
    header("location: applicant/");
}

if(isset($_SESSION['login_company']) == 'login_company')
{
    header("location: company/");
}
if(isset($_SESSION['login_recruiter']) == 'login_recruiter')
{
    header("location: recruiter/");
}
if(isset($_POST['save_button'])){
  $FirstName = filter($_POST['FirstName']);
  $LastName = filter($_POST['LastName']);
  $email_address = filter($_POST['email_address']);
  $user_pass = filter($_POST['user_pass']);

  $allowedExts = array("docx", "pdf", "doc");
  $temp = explode(".", $_FILES["applicantCV"]["name"]);
  $applicantCV =$_FILES['applicantCV'] ["name"];
  $extension = end($temp);

  $Address = filter($_POST['Address']);
  $BirthDate = filter($_POST['BirthDate']);
  $ContactNumber = filter($_POST['ContactNumber']);
  $Gender  = filter($_POST['Gender']);

  $target_dir = "img/";
  $target_file = $target_dir . basename($_FILES["applicantCV"]["name"]);
  $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

  $checkemail = getSingleRow("EmailAddress","EmailAddress","accounts",$email_address);

  if(checkname($FirstName,$LastName)){
    $msg = 'Name: '.$FirstName.' '.$LastName.' already exist. ';
  }elseif($checkemail['EmailAddress'] == $email_address){
    $msg = 'Email Address: '.$email_address.' already exist';
  }elseif($user_pass != $confirm_pass){
    $msg = "Password Do not match.";
  }elseif(!is_numeric($ContactNumber)){
    $msg = 'Please enter a valid number only';
  }
  else{
    if(in_array($extension, $allowedExts)){
    $accountArray = array("FirstName"=>$FirstName, "LastName"=>$LastName,"EmailAddress"=>$email_address,"Password"=>md5($user_pass),"UserType"=>"1","UserStatus"=>"1");
    SaveData("accounts",$accountArray);

    $last_id = mysqli_insert_id($dbcon);
    
    $applicantArray = array("UserID"=>$last_id, "ApplicantEmail"=>$email_address,"ApplicantPhoto"=>"default.png",
      "ApplicantCV"=>$applicantCV, "BirthDate" => $BirthDate, "ContactNumber" => $ContactNumber, "Address" =>$Address, "Gender"=>$Gender);
    SaveData("applicants",$applicantArray);
    move_uploaded_file($_FILES["applicantCV"]["tmp_name"],"../img/". $_FILES["applicantCV"]["name"]);
    $success = 'You have successfully registered to website.';
    }
    else{
      $msg = '<script>alert("Please upload .docx, .pdf, or .doc file"); </script>';
    }
  
  }
}
?>
<?php include'dist/assets/header.php';?>
    <main role="main" style="">
      <div class="container marketing" style="margin-top:10%;">

        <!-- Three columns of text below the carousel -->
        
        <div >
          <div class="col-md-12" >
            <center><h1><i class="fa fa-pencil"></i> Register for FREE</h1>
            <hr>
            <div class="container">
            <?php if(!empty($msg)):?><div class="alert alert-danger"><?php echo $msg;?></div><?php endif;?>
            <?php if(!empty($success)):?>
              <div class="alert alert-success"><?php echo $success;?></div>
              <META HTTP-EQUIV="refresh" CONTENT="1; URL=login.php">
            <?php endif;?>
            <form method="post" enctype="multipart/form-data">
              <div class="col-md-6 mx-sm-3">
                <input type="text" name="FirstName" class="form-control" placeholder="First Name" required="required" value="<?php if(isset($_POST['save_button'])): echo $_POST['FirstName']; endif;?>">
              </div>
              <p></p>
              <div class="col-md-6 mx-sm-3">
                <input type="text" name="LastName" class="form-control" placeholder="Last Name" required="required" value="<?php if(isset($_POST['save_button'])): echo $_POST['LastName']; endif;?>">
              </div>
              <p></p>
               <div class="col-md-6 mx-sm-3">
                <input type="date" name="BirthDate" class="form-control" required="required" value="<?php if(isset($_POST['save_button'])): echo $_POST['BirthDate']; endif;?>">
              </div>
              <p></p>
               <div class="col-md-6 mx-sm-3">
                <input type="text" maxlength="11" name="ContactNumber" class="form-control" placeholder="Contact Number" required="required" value="<?php if(isset($_POST['save_button'])): echo $_POST['ContactNumber']; endif;?>">
              </div>
              <p></p>
              <div class="col-md-6 mx-sm-3">
                <input type="text" name="Address" class="form-control" placeholder="Address" required="required" value="<?php if(isset($_POST['save_button'])): echo $_POST['Address']; endif;?>">
              </div>
              <p></p>
              <div class="col-md-6 mx-sm-3">
                <input type="text" name="email_address" class="form-control" placeholder="Email Address" required="required" value="<?php if(isset($_POST['save_button'])): echo $_POST['email_address']; endif;?>">
              </div>
              <p></p>
              <div class="col-md-6 mx-sm-3">
                 <select class="form-control" name="Gender">
                      <option value="Male">Male</option>
                      <option value="Female">Female</option>
                      
                    </select>
              </div>
              <p></p>
              <div class="col-md-6 mx-sm-3">
                <input type="password" name="user_pass" class="form-control" placeholder="Password" required="required">
              </div>
              <p></p>
              <div class="col-md-6 mx-sm-3">
                <input type="password" name="confirm_pass" class="form-control" placeholder="Confirm Password" required="required">
              </div>
              <p></p>
                <div class="col-md-6 mx-sm-3">
              <strong>Upload CV</strong>
                <input type="file" name="applicantCV" class="form-control" >
              </div>
              <br>
               <div class="col-md-6">
                <button class="btn btn-danger" name="save_button"><i class="fa fa-save"></i> Signup</button>
              </div>
              </form>
              <br>
              <div class="col-md-6">
                Already a member?<a href="login.php"> Login now</a>
              </div>
              <p></p>
            </div>
            </center>
        </div>
</main>

</div>

<?php include'dist/assets/footer.php';?>