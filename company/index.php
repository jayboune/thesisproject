<?php 
include'../config/db.php';
include'../config/functions.php';
include'../config/myfunction.php';

if(empty($_SESSION['login_company'])){ 
//This function is to check weather the account has been login or not
  header("Location: ../index.php");
  exit;
}
$applicant = forEvaluation();
?>
<?php include'../dist/assets/dashboard_header.php';?>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
<?php include'../dist/assets/dashboard_nav.php';?>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="col-md-12">
         <div class="row">

          <!-- /.col -->
          <div class="col-12 col-sm-6 col-md-6">
            <div class="info-box mb-3">
              <span class="info-box-icon bg-danger elevation-1"><i class="fa fa-users"></i></span>

              <div class="info-box-content">
                <span class="info-box-text"> For Evaluation </span>
                <span class="info-box-number">41,410</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->

          <!-- fix for small devices only -->
          <div class="clearfix hidden-md-up"></div>

          <div class="col-12 col-sm-6 col-md-6">
            <div class="info-box mb-3">
              <span class="info-box-icon bg-success elevation-1"><i class="fa fa-user"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Hired Applicants</span>
                <span class="info-box-number">760</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
        </div>
            <div class="card">
              <div class="card-header">
                <h3 class="card-title"><i class="fa fa-users"></i> Applicants for Evaluation</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <?php  if(!empty($applicant)):?>
                <table id="example1" class="table table-bordered table-striped" style="font-size:12px;">
                <thead>
                <tr>
                  <th>Applicant</th>
                  <th>Company Name</th>
                  <th>Position Applying for</th>
                  <th>Interview Schedule</th>
                  <th>Interviewer</th>
                  <th>Status</th>
                  <th>Option</th>
                </tr>
                </thead>
                <tbody>
              <?php foreach ($applicant as $key => $value):?>
                <tr>
                  <td><?php echo $value->FirstName?> <?php echo $value->LastName?></td>
                  <td><?php echo $value->CompanyName?></td>
                  <td>
                  <?php echo $value->JobTitle?>
                  </td>
                  <td><?php echo $value->InterviewSchedule?> </td>
                  <td><?php echo $value->ApplicationStatus?></td>
                  <td><?php echo $value->Interviewer?></td>
                  <td>
                    <a href="option.php?application_id=<?php echo $value->ApplicationID?>&jobID=<?php echo $value->JobID?>&company_id=<?php echo $value->ClientID?>">View Details</a>
                  </td>
                </tr>
              <?php endforeach;?>
              </table>
              <?php else:?>
                <div class="alert alert-danger">There are no records on the database.</div>
              <?php endif;?>
              
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
    </div>

  </div>
  <!-- /.content-wrapper -->
<?php include'../dist/assets/dashboard_footer.php';?>