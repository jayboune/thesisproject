<?php 
include'../config/db.php';
include'../config/functions.php';
include'../config/myfunction.php';

if(empty($_SESSION['login_company'])){ 
//This function is to check weather the account has been login or not
  header("Location: ../index.php");
  exit;
}
$hired = getReportHired(); 
?>
<!DOCTYPE html>
<html>
<head>
  <title>Print Report</title>
</head>
<link rel="stylesheet" href="../dist/css/adminlte.min.css">

<style>
  
  @media print {
  #hide {
    display: none;
  }
}

    @media screen{
      thead{
        display:none;
      }
    }
    @media print{
      thead{
        display:table-header-group; margin-bottom:2px;
      }
    }
    @page{
      margin-top:1cm;margin-left:1cm;margin-right:1cm;margin-bottom:1.5cm;
      }
    }
</style>
<body>
<table class="table table-bordered" style="font-size:10px;">
    <thead>
        <tr>
            <th colspan="10">
              <center><img src="../img/jadgtc_logo.png" align="center"></center>
            </th>
        </tr>
       
    </thead>
    <tbody>
  <tr>
    <td>Status: Hired</td>
    <td>From: <?php echo $_GET['from']?></td>
    <td>Until: <?php echo $_GET['until']?></td>
    <td></td>
  </tr>
  <tr>
    <td>Full Name</td>
    <td>Email Address</td>
    <td>Contact</td>
    <td>Address</td>
  </tr>
<?php if(!empty($hired)):?>
<?php foreach ($hired as $key => $value):?>
  <tr>
    <td><?php echo $value->FirstName?> <?php echo $value->LastName?></td>
    <td><?php echo $value->EmailAddress?></td>
    <td><?php echo $value->ContactNumber?></td>
    <td><?php echo $value->Address?></td>
  </tr>
<?php endforeach;?>
<?php else:?>
<?php endif;?>
</table>
<center><a href="" class="btn btn-primary" id="hide" onclick="print()">Print Now</a></center>
</body>
</html>