-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 29, 2018 at 05:59 AM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `jad_backup`
--

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE `accounts` (
  `UserID` int(11) NOT NULL,
  `EmailAddress` varchar(255) NOT NULL,
  `Password` varchar(255) NOT NULL,
  `FirstName` varchar(255) NOT NULL,
  `LastName` varchar(255) NOT NULL,
  `UserType` int(11) NOT NULL,
  `UserStatus` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`UserID`, `EmailAddress`, `Password`, `FirstName`, `LastName`, `UserType`, `UserStatus`) VALUES
(1, 'jadgtc@gmail.com', '21232f297a57a5a743894a0e4a801fc3', 'Juan', 'Dela Cruz', 0, 1),
(2, 'kristel@gmail.com', '95d2431088ab63ddf3dc48ee0dea97ae', 'Kristel Marie', 'Tandoc', 1, 1),
(8, 'manny@pldt.com', '95d2431088ab63ddf3dc48ee0dea97ae', 'Kristine', 'Pangilinan', 2, 0),
(9, 'naskdnsa@gmail.com', '95d2431088ab63ddf3dc48ee0dea97ae', 'kasjndjkas', 'njkasnd', 2, 0),
(10, 'mlsdf@gmail.com', '95d2431088ab63ddf3dc48ee0dea97ae', 'naksjn', 'jksdnfsdf', 2, 0),
(11, 'meralcoadmin@meralco.com.ph', '95d2431088ab63ddf3dc48ee0dea97ae', 'Manny', 'Sarmiento', 2, 1),
(12, 'marks@gmail.com', 'd106b29303767527fc11214f1b325fb6', 'mark', 'Samaniego', 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `additional_infos`
--

CREATE TABLE `additional_infos` (
  `InfoID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `ExpectedSalary` decimal(10,2) DEFAULT NULL,
  `WorkLocation` text,
  `OtherInfo` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `additional_infos`
--

INSERT INTO `additional_infos` (`InfoID`, `UserID`, `ExpectedSalary`, `WorkLocation`, `OtherInfo`) VALUES
(1, 2, '15000.00', 'makati City', 'no ifno');

-- --------------------------------------------------------

--
-- Table structure for table `applicants`
--

CREATE TABLE `applicants` (
  `ApplicantID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `ApplicantEmail` varchar(255) NOT NULL,
  `Gender` varchar(50) NOT NULL,
  `BirthDate` date NOT NULL,
  `ContactNumber` varchar(50) NOT NULL,
  `Address` text NOT NULL,
  `ApplicantCV` varchar(255) DEFAULT NULL,
  `ApplicantPhoto` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `applicants`
--

INSERT INTO `applicants` (`ApplicantID`, `UserID`, `ApplicantEmail`, `Gender`, `BirthDate`, `ContactNumber`, `Address`, `ApplicantCV`, `ApplicantPhoto`) VALUES
(1, 2, 'kristel@gmail.com', '', '1991-07-15', '09999637980', 'pasig city', 'bitbucket.txt', 'Koala.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `audittrails`
--

CREATE TABLE `audittrails` (
  `AuditID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `Action` varchar(255) NOT NULL,
  `TimeStamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `companyclients`
--

CREATE TABLE `companyclients` (
  `ClientID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `CompanyName` varchar(255) NOT NULL,
  `CompanyEmail` varchar(255) NOT NULL,
  `CompanyAddress` varchar(255) NOT NULL,
  `ContactNumber` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `companyclients`
--

INSERT INTO `companyclients` (`ClientID`, `UserID`, `CompanyName`, `CompanyEmail`, `CompanyAddress`, `ContactNumber`) VALUES
(1, 11, 'Meralco', 'meralcoadmin@meralco.com.ph', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `educations`
--

CREATE TABLE `educations` (
  `EduID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `University` varchar(255) NOT NULL,
  `GradDate` varchar(255) NOT NULL,
  `Qualification` varchar(255) NOT NULL,
  `Course` varchar(255) NOT NULL,
  `AddInfo` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `educations`
--

INSERT INTO `educations` (`EduID`, `UserID`, `University`, `GradDate`, `Qualification`, `Course`, `AddInfo`) VALUES
(1, 2, 'University of the Cordilleras (Formerly Baguio Colleges Foundation)', '2011-06-02', 'Bachelors Degree /College Degree', 'Bachelor Of Science in Information Technology major in Network Security', 'No info');

-- --------------------------------------------------------

--
-- Table structure for table `experiences`
--

CREATE TABLE `experiences` (
  `ExpID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `PreviousCompany` text NOT NULL,
  `PositionTitle` varchar(255) DEFAULT NULL,
  `Position` varchar(255) DEFAULT NULL,
  `EmployerName` varchar(255) DEFAULT NULL,
  `Date_From` date DEFAULT NULL,
  `Date_Until` date DEFAULT NULL,
  `Salary` decimal(10,2) DEFAULT NULL,
  `OtherDesc` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `experiences`
--

INSERT INTO `experiences` (`ExpID`, `UserID`, `PreviousCompany`, `PositionTitle`, `Position`, `EmployerName`, `Date_From`, `Date_Until`, `Salary`, `OtherDesc`) VALUES
(1, 2, '', 'IT Helpdesk (Edited)', 'Fresh Graduate', 'Coca-cola Far East', '2011-06-14', '2012-06-30', '11000.00', 'none');

-- --------------------------------------------------------

--
-- Table structure for table `jobapplications`
--

CREATE TABLE `jobapplications` (
  `ApplicationID` int(11) NOT NULL,
  `JobID` int(11) NOT NULL,
  `ApplicantID` int(11) NOT NULL,
  `ClientID` int(11) NOT NULL,
  `ApplicationStatus` varchar(255) NOT NULL,
  `InterviewSchedule` datetime NOT NULL,
  `Interviewer` varchar(255) NOT NULL,
  `Remarks` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jobcategories`
--

CREATE TABLE `jobcategories` (
  `JCategoryID` int(11) NOT NULL,
  `Category` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jobcategories`
--

INSERT INTO `jobcategories` (`JCategoryID`, `Category`) VALUES
(1, 'Information Technology'),
(2, 'Health and Wellness'),
(3, 'Engineering');

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `LanguageID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `Language` varchar(255) DEFAULT NULL,
  `Spoken` int(11) DEFAULT NULL,
  `Written` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`LanguageID`, `UserID`, `Language`, `Spoken`, `Written`) VALUES
(1, 2, 'English', 3, 5),
(2, 2, 'Kapampangan', 4, 4);

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

CREATE TABLE `locations` (
  `LocationID` int(11) NOT NULL,
  `Location` varchar(255) NOT NULL,
  `Details` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `locations`
--

INSERT INTO `locations` (`LocationID`, `Location`, `Details`) VALUES
(1, 'United States', 'Alabama US');

-- --------------------------------------------------------

--
-- Table structure for table `recruitment_officers`
--

CREATE TABLE `recruitment_officers` (
  `RofficerID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `OfficerAddress` varchar(255) NOT NULL,
  `ContactNumber` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `recruitment_officers`
--

INSERT INTO `recruitment_officers` (`RofficerID`, `UserID`, `OfficerAddress`, `ContactNumber`) VALUES
(1, 12, 'asdkansdkans', '09090400500');

-- --------------------------------------------------------

--
-- Table structure for table `requirements`
--

CREATE TABLE `requirements` (
  `RequirementID` int(11) NOT NULL,
  `JobID` int(11) NOT NULL,
  `ApplicantID` int(11) NOT NULL,
  `MedicalExam` varchar(255) DEFAULT NULL,
  `VisaApplicationForm` varchar(255) DEFAULT NULL,
  `VisaStamping` varchar(255) DEFAULT NULL,
  `POEA` varchar(255) DEFAULT NULL,
  `PDOS` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `skills`
--

CREATE TABLE `skills` (
  `SkillID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `Skill` varchar(255) DEFAULT NULL,
  `Proficiency` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `skills`
--

INSERT INTO `skills` (`SkillID`, `UserID`, `Skill`, `Proficiency`) VALUES
(1, 2, 'PHP, MYSQLI, CSS (edited)', 'Advanced'),
(2, 2, 'Bootstrap', 'Advanced');

-- --------------------------------------------------------

--
-- Table structure for table `specializations`
--

CREATE TABLE `specializations` (
  `SpecializationID` int(11) NOT NULL,
  `Specialization` varchar(255) NOT NULL,
  `JCategoryID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `specializations`
--

INSERT INTO `specializations` (`SpecializationID`, `Specialization`, `JCategoryID`) VALUES
(1, 'Software Development', 1),
(4, 'Hardware(Tech Support)', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`UserID`);

--
-- Indexes for table `additional_infos`
--
ALTER TABLE `additional_infos`
  ADD PRIMARY KEY (`InfoID`),
  ADD KEY `UserID` (`UserID`);

--
-- Indexes for table `applicants`
--
ALTER TABLE `applicants`
  ADD PRIMARY KEY (`ApplicantID`),
  ADD KEY `UserID` (`UserID`);

--
-- Indexes for table `audittrails`
--
ALTER TABLE `audittrails`
  ADD PRIMARY KEY (`AuditID`),
  ADD KEY `UserID` (`UserID`);

--
-- Indexes for table `companyclients`
--
ALTER TABLE `companyclients`
  ADD PRIMARY KEY (`ClientID`),
  ADD KEY `UserID` (`UserID`);

--
-- Indexes for table `educations`
--
ALTER TABLE `educations`
  ADD PRIMARY KEY (`EduID`),
  ADD KEY `UserID` (`UserID`);

--
-- Indexes for table `experiences`
--
ALTER TABLE `experiences`
  ADD PRIMARY KEY (`ExpID`),
  ADD KEY `UserID` (`UserID`);

--
-- Indexes for table `jobapplications`
--
ALTER TABLE `jobapplications`
  ADD PRIMARY KEY (`ApplicationID`),
  ADD KEY `JobID` (`JobID`),
  ADD KEY `ApplicantID` (`ApplicantID`),
  ADD KEY `ClientID` (`ClientID`);

--
-- Indexes for table `jobcategories`
--
ALTER TABLE `jobcategories`
  ADD PRIMARY KEY (`JCategoryID`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`LanguageID`),
  ADD KEY `UserID` (`UserID`);

--
-- Indexes for table `locations`
--
ALTER TABLE `locations`
  ADD PRIMARY KEY (`LocationID`);

--
-- Indexes for table `recruitment_officers`
--
ALTER TABLE `recruitment_officers`
  ADD PRIMARY KEY (`RofficerID`),
  ADD KEY `UserID` (`UserID`);

--
-- Indexes for table `requirements`
--
ALTER TABLE `requirements`
  ADD PRIMARY KEY (`RequirementID`),
  ADD KEY `JobID` (`JobID`),
  ADD KEY `ApplicantID` (`ApplicantID`);

--
-- Indexes for table `skills`
--
ALTER TABLE `skills`
  ADD PRIMARY KEY (`SkillID`),
  ADD KEY `UserID` (`UserID`);

--
-- Indexes for table `specializations`
--
ALTER TABLE `specializations`
  ADD PRIMARY KEY (`SpecializationID`),
  ADD KEY `JCategoryID` (`JCategoryID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accounts`
--
ALTER TABLE `accounts`
  MODIFY `UserID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `additional_infos`
--
ALTER TABLE `additional_infos`
  MODIFY `InfoID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `applicants`
--
ALTER TABLE `applicants`
  MODIFY `ApplicantID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `audittrails`
--
ALTER TABLE `audittrails`
  MODIFY `AuditID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `companyclients`
--
ALTER TABLE `companyclients`
  MODIFY `ClientID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `educations`
--
ALTER TABLE `educations`
  MODIFY `EduID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `experiences`
--
ALTER TABLE `experiences`
  MODIFY `ExpID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `jobapplications`
--
ALTER TABLE `jobapplications`
  MODIFY `ApplicationID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jobcategories`
--
ALTER TABLE `jobcategories`
  MODIFY `JCategoryID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `LanguageID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `locations`
--
ALTER TABLE `locations`
  MODIFY `LocationID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `recruitment_officers`
--
ALTER TABLE `recruitment_officers`
  MODIFY `RofficerID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `requirements`
--
ALTER TABLE `requirements`
  MODIFY `RequirementID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `skills`
--
ALTER TABLE `skills`
  MODIFY `SkillID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `specializations`
--
ALTER TABLE `specializations`
  MODIFY `SpecializationID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `additional_infos`
--
ALTER TABLE `additional_infos`
  ADD CONSTRAINT `additional_infos_ibfk_1` FOREIGN KEY (`UserID`) REFERENCES `accounts` (`UserID`);

--
-- Constraints for table `applicants`
--
ALTER TABLE `applicants`
  ADD CONSTRAINT `applicants_ibfk_1` FOREIGN KEY (`UserID`) REFERENCES `accounts` (`UserID`);

--
-- Constraints for table `audittrails`
--
ALTER TABLE `audittrails`
  ADD CONSTRAINT `audittrails_ibfk_1` FOREIGN KEY (`UserID`) REFERENCES `accounts` (`UserID`);

--
-- Constraints for table `companyclients`
--
ALTER TABLE `companyclients`
  ADD CONSTRAINT `companyclients_ibfk_1` FOREIGN KEY (`UserID`) REFERENCES `accounts` (`UserID`);

--
-- Constraints for table `educations`
--
ALTER TABLE `educations`
  ADD CONSTRAINT `educations_ibfk_1` FOREIGN KEY (`UserID`) REFERENCES `accounts` (`UserID`);

--
-- Constraints for table `experiences`
--
ALTER TABLE `experiences`
  ADD CONSTRAINT `experiences_ibfk_1` FOREIGN KEY (`UserID`) REFERENCES `accounts` (`UserID`);

--
-- Constraints for table `jobapplications`
--
ALTER TABLE `jobapplications`
  ADD CONSTRAINT `jobapplications_ibfk_1` FOREIGN KEY (`JobID`) REFERENCES `jobposts` (`JobID`),
  ADD CONSTRAINT `jobapplications_ibfk_2` FOREIGN KEY (`ApplicantID`) REFERENCES `accounts` (`UserID`),
  ADD CONSTRAINT `jobapplications_ibfk_3` FOREIGN KEY (`ClientID`) REFERENCES `accounts` (`UserID`);

--
-- Constraints for table `languages`
--
ALTER TABLE `languages`
  ADD CONSTRAINT `languages_ibfk_1` FOREIGN KEY (`UserID`) REFERENCES `accounts` (`UserID`);

--
-- Constraints for table `recruitment_officers`
--
ALTER TABLE `recruitment_officers`
  ADD CONSTRAINT `recruitment_officers_ibfk_1` FOREIGN KEY (`UserID`) REFERENCES `accounts` (`UserID`);

--
-- Constraints for table `requirements`
--
ALTER TABLE `requirements`
  ADD CONSTRAINT `requirements_ibfk_1` FOREIGN KEY (`JobID`) REFERENCES `jobposts` (`JobID`),
  ADD CONSTRAINT `requirements_ibfk_2` FOREIGN KEY (`ApplicantID`) REFERENCES `accounts` (`UserID`);

--
-- Constraints for table `skills`
--
ALTER TABLE `skills`
  ADD CONSTRAINT `skills_ibfk_1` FOREIGN KEY (`UserID`) REFERENCES `accounts` (`UserID`);

--
-- Constraints for table `specializations`
--
ALTER TABLE `specializations`
  ADD CONSTRAINT `specializations_ibfk_1` FOREIGN KEY (`JCategoryID`) REFERENCES `jobcategories` (`JCategoryID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
