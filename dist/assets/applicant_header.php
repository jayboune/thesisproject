<!DOCTYPE html>
<!-- saved from url=(0052)https://getbootstrap.com/docs/4.1/examples/carousel/ -->
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="https://getbootstrap.com/favicon.ico">

    <title>JAD + GTC Manpower Supply & Services</title>

    <!-- Bootstrap core CSS -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700" rel="stylesheet">
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/carousel.css" rel="stylesheet">
    <link rel="stylesheet" href="../plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="../plugins/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="../dist/css/adminlte.min.css">
  <link rel="stylesheet" href="../plugins/datatables/dataTables.bootstrap4.css">
  <link rel="stylesheet" href="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <link rel="stylesheet" href="../plugins/fullcalendar/fullcalendar.min.css">
  <link rel="stylesheet" href="../plugins/fullcalendar/fullcalendar.print.min.css" media="print">
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  </head>
  <body>

    <header>
      <nav class="navbar navbar-expand-md navbar-dark fixed-top" style="background:white; border-bottom:2px solid #adb5bd;">
        <a class="navbar-brand" href=""><a class="nav-link" href=""><img src="../img/jadgtc_logo.png" height="100%" width="100%"></a></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation" style="background:black;">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item">
            </li>
          </ul>
          <ul class="navbar-nav form-inline mt-2 mt-md-0">
            <li class="nav-item">
              <a href="index.php" class="nav-link" style="color:black; padding:15px;"><i class="fa fa-home"></i> Home</a>
            </li>
            
            <li class="nav-item">
              <a href="myprofile.php" class="nav-link" style="color:black; padding:15px;"><i class="fa fa-user"></i> My Profile</a>
            </li>
            <!--
            <li class="nav-item">
              <a href="calendar.php" class="nav-link" style="color:black; padding:15px;"><i class="fa fa-calendar"></i> My Schedule</a>
            </li>
          -->
          <li class="nav-item">
              <a href="jobs.php" class="nav-link" style="color:black; padding:15px;"><i class="fa fa-briefcase"></i> Job Posting</a>
            </li>
            <!--
            <li class="nav-item">
              <a href="change.php" class="nav-link" style="color:black; padding:15px;"><i class="fa fa-wrench"></i> Change Password</a>
            </li>
          -->
            <li class="nav-item">
              <a href="logout.php" class="nav-link" style="color:black; padding:15px;"><i class="fa fa-edit"></i> Logout</a>
            </li>
            
          </ul>
        </div>
      </nav>
    </header>