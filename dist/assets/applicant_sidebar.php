<div class="card card-success">
              <div class="card-header">
                <h3 class="card-title"><i class="fa fa-user"></i> About Me</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
               
                <ul style="list-style: none; padding:5px;line-height: 30px;">
                  <li><a href="education.php"><i class="fa fa-graduation-cap"></i> Education</a></li>
                  <li><a href="skills.php"><i class="fa fa-book"></i> Skills</a></li>
                  <li><a href="language.php"><i class="fa fa-comment"></i> Language</a></li>
                  <li><a href="info.php"><i class="fa fa-plus"></i> Additional Information</a></li>
                  <li><a href="experience.php"><i class="fa fa-file"></i> Work Experience</a></li>
                </ul>
              </div>
              <!-- /.card-body -->
            </div>

            <div class="card card-success">
              <div class="card-header">
                <h3 class="card-title"><i class="fa fa-download"></i> Resume</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
               
                <?php if(!empty($user['ApplicantCV'])):?>
                <a href="../img/<?php echo $user['ApplicantCV']?>">Download Resume</a>
                <br>
              <?php endif;?>
                <a href="" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#upload-resume"><i class="fa fa-upload"></i>
                <?php if(!empty($user['ApplicantCV'])): echo 'Update Resume';else: echo 'Upload Resume'; endif;?>
                </a>
              </div>
              <!-- /.card-body -->
            </div>


            <div class="card card-success">
              <div class="card-header">
                <h3 class="card-title"><i class="fa fa-briefcase"></i> Jobs</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
               
                <ul style="list-style: none; padding:5px;line-height: 30px;">
                  <li><a href="viewstatus.php"><i class="fa fa-th"></i> View Status</a></li>
                </ul>
              </div>


