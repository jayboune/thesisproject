<!-- Uploading of Resume-->
<div class="modal fade" id="upload-resume">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title">Upload Resume</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                
              </div>
              <div class="modal-body">
                <form method="post" enctype="multipart/form-data">
  <strong>Resume:</strong><br>
  <input type="file" name="photo" required="required"><br>
              </div>
              <div class="modal-footer">
                
                <button type="submit" name="upload_resume" class="btn btn-primary">Save</button>
              </div>
            </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
</div>
<!-- end -->

<!-- Add Skill-->
<div class="modal fade" id="add-skill">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title">Add Skills</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                
              </div>
              <div class="modal-body">
  <form method="post" enctype="multipart/form-data">
  <div class="row">
  <div class="col-md-12">
    Skills:<br>
    <input type="text" name="skill_name" class="form-control" placeholder="Skill" required>
  </div>
    <br><br>
    <div class="col-md-12">
      Qualification:<br>
      <select name="profeciency" class="form-control">
        <option>Beginner</option>
        <option>Advanced</option>
        <option>Intermediate</option>
      </select>
    </div>
  <div class="col-md-12">
    <br>
    <button name="save_button" class="btn btn-primary"><i class="fa fa-save"></i> Save Data</button> 
  </div>
              </div>
            </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
</div>
<!-- end -->
<!-- Add Education-->
<div class="modal fade" id="add-educ">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title">Education Information</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                
              </div>
              <div class="modal-body">
            <form method="post" enctype="multipart/form-data">
              <div class="row">
                <div class="col-md-12">
                  University:<br>
                  <input type="text" name="university" class="form-control" placeholder="University" required>
                </div>
                <br> 
                <div class="col-md-12">
                  Graduation Date:<br>
                  <input type="date" name="grad_date" class="form-control" placeholder="University" required>
                </div>
                <div class="col-md-12">
                  Qualification:<br>
                  <select name="qualification" class="form-control">
                    <option>HighSchool Diploma</option>
                    <option>Vocational / Short Course Certificate</option>
                    <option>Bachelors Degree /College Degree</option>
                    <option>Post Graduate Diploma / Masters Degree</option>
                    <option>Professional License (Pass Board/ Bar/ Professional License Exam)</option>
                  </select>
                </div>
                <div class="col-md-12">
                  Course:<br>
                  <input type="text" name="course" class="form-control" placeholder="Course" required>
                </div>
                <div class="col-md-12">
                  Additional Information:<br>
                  <textarea class="form-control" name="add_info" placeholder="Enter Additinoal info(Optional)"></textarea>
                </div>
                <br><br>
                <div class="col-md-12">
                  <br>
                  <button name="save_button" class="btn btn-primary">
                    <i class="fa fa-save"></i> Save Data
                  </button> 
                </div>

              </div>
            </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
</div>
<!-- end -->
<!-- Add Language-->
<div class="modal fade" id="add-language">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title">Add Language</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                
              </div>
              <div class="modal-body">
  <form method="post" enctype="multipart/form-data">
  <div class="row">
  <div class="col-md-12">
    Language:<br>
    <input type="text" name="language_name" class="form-control" placeholder="Language" required>
  </div>
    <br><br>
    <div class="col-md-12">
      Spoken:<br>
      <select name="spoken" class="form-control">
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
      </select>
    </div>
    <br><br>
    <div class="col-md-12">
      Written:<br>
      <select name="written" class="form-control">
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
      </select>
    </div>
  <div class="col-md-12">
    <br>
    <button name="save_button" class="btn btn-primary"><i class="fa fa-save"></i> Save Data</button> 
  </div>
              </div>
            </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
</div>
<!-- end -->
<!-- Add Other Information-->
<div class="modal fade" id="add-info">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title">Additional Information</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                
              </div>
              <div class="modal-body">
            <form method="post" enctype="multipart/form-data">
              <div class="row">
                <div class="col-md-12">
                  Expected Salary:<br>
                  <input type="text" name="expected_salary" class="form-control" placeholder="Expected Salary" required>
                </div>
                <br> 
                <div class="col-md-12">
                  Work Location:<br>
                  <input type="text" name="work_location" class="form-control" placeholder="Work Location" required>
                </div>
                <div class="col-md-12">
                  Additional Information:<br>
                  <textarea class="form-control" name="other_info" placeholder="Enter Additinoal info(Optional)"></textarea>
                </div>
                <br><br>
                <div class="col-md-12">
                  <br>
                  <button name="save_button" class="btn btn-primary">
                    <i class="fa fa-save"></i> Save Data
                  </button> 
                </div>

              </div>
            </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
</div>
<!-- end -->
<!-- Add Job Experience-->
<div class="modal fade" id="add-experience">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title">Working Experience</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                
              </div>
              <div class="modal-body">
            <form method="post" enctype="multipart/form-data">
              <div class="row">
                <div class="col-md-12">
                  Position:<br>
                  <input type="text" name="position_title" class="form-control" placeholder="Position" required>
                </div>
                <br>
                <div class="col-md-12">
                  Company:<br>
                  <input type="text" name="company_name" class="form-control" placeholder="Company Name" required>
                </div>
                <br>  
                <div class="col-md-12">
                  From:<br>
                  <input type="date" name="date_from" class="form-control" required>
                </div><br>
                <div class="col-md-12">
                  Until:<br>
                  <input type="date" name="date_until" class="form-control" required>
                </div>
                <div class="col-md-12">
                  Position Level:<br>
                  <select name="position_level" class="form-control">
                    <option>Fresh Graduate</option>
                    <option>1 to 2 years Working experience</option>
                    <option>Managerial Position</option>
                    <option>CEO</option>
                  </select>
                </div>

                <div class="col-md-12">
                  Salary:<br>
                  <input type="number" name="salary" class="form-control" placeholder="Basic Salary" required>
                </div>
                <div class="col-md-12">
                  Additional Information:<br>
                  <textarea class="form-control" name="other_desc" placeholder="Enter Additinoal info(Optional)"></textarea>
                </div>
                <br><br>
                <div class="col-md-12">
                  <br>
                  <button name="save_button" class="btn btn-primary">
                    <i class="fa fa-save"></i> Save Data
                  </button> 
                </div>

              </div>
            </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
</div>
<!-- end -->